<?php
namespace GSDDB;

require_once plugin_dir_path( __FILE__ ) . 'class-form.php';
require_once plugin_dir_path( __FILE__ ) . 'class-notification.php';
require_once plugin_dir_path( __FILE__ ) . 'class-database.php';

class Pre_Registration
{
  // configuration of the pre registration form
  public $form_config = [];

  public function __construct () {
    $this->form_config = [
      "title" => "Voranmeldung",
      "slug" => "gsd_pre_registration",
      "fields" => [
        [ // Persönliche Daten
          "type" => "section",
          "header" => "Persönliche Daten",
          "fields" => [
            [
              "type" => "text",
              "name" => "firstname", "label" => "Vorname", "mandatory" => true,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "text",
              "name" => "lastname", "label" => "Nachname", "mandatory" => true,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "email",
              "name" => "email", "label" => "e-Mail", "mandatory" => true,
              "sanitize" => "email",
              "length" => 128,
            ],
            [
              "type" => "date",
              "name" => "birthdate", "label" => "Geburtsdatum", "mandatory" => true,
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
          ],
        ],
        [ // Fachhochschule
          "type" => "section",
          "header" => "Fachhochschule",
          "fields" => [
            [
              "type" => "dropdown",
              "name" => "hochschule_id", "label" => "Fachhochschule", "mandatory" => true,
              "options" => Database::get_dropdown_array_hochschule(),
              "validate" => "options"
            ],
            [
              "type" => "radio",
              "name" => "studium", "label" => "Studiengang", "mandatory" => true,
              "options" => Database::get_radio_options_studien(),
              "validate" => "options",
            ],
            [
              "type" => "text",
              "name" => "matrikelnr", "label" => "Matrikelnummer", "mandatory" => true,
              "sanitize" => "title",
              "validate" => "regex", "regex" => '/^[a-z0-9]+$/',
              "length" => 16,
            ],
            [
              "type" => "text",
              "name" => "lektorin", "label" => "Name d* Praxislektor*in", "mandatory" => true,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "text",
              "name" => "lektorin_email", "label" => "e-Mail d* Praxislektor*in", "mandatory" => true,
              "sanitize" => "email",
              "length" => 128,
            ],
          ],
        ],
        [ // Einsatzorganisation
          "type" => "section",
          "header" => "Einsatzorganisation",
          "fields" => [
            [
              "type" => "paragraph",
              "content" => "(In diesem Abschnitt die voraussichtlichen Daten angeben,
              soweit bekannt. Diese können später noch aktualisiert werden.)",
            ],
            [ // TODO: integrate following two fields as alternative
              "type" => "dropdown",
              "name" => "einsatzorga_id", "label" => "Einsatzorganisation", "mandatory" => false,
              "options" => Database::get_dropdown_array_einsatzorga(),
              "validate" => "options",
            ],
            [
              "type" => "text",
              "name" => "einsatzorga_other_name",
              "label" => "Name der Einsatzorganisation (nur falls 'Sonstige')",
              "mandatory" => false,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "text",
              "name" => "einsatzorga_other_website",
              "label" => "Website der Einsatzorganisation (nur falls 'Sonstige')",
              "mandatory" => false,
              "sanitize" => "url",
              "length" => 64,
            ],
            [
              "type" => "text",
              "name" => "einsatzorga_kontakt",
              "label" => "Name der Kontaktperson bei der Einsatzorganisation",
              "mandatory" => false,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "text",
              "name" => "einsatzorga_kontakt_email", "label" => "e-Mail der Kontaktperson",
              "mandatory" => false,
              "sanitize" => "email",
              "length" => 128,
            ],
          ],
        ],
        [ // Praktikumsstelle
          "type" => "section",
          "header" => "Praktikumsstelle",
          "fields" => [
            [
              "type" => "paragraph",
              "content" => "(In diesem Abschnitt die voraussichtlichen Daten angeben,
              soweit bekannt. Diese können später noch aktualisiert werden.)",
            ],
            [
              "type" => "text",
              "name" => "einsatzland", "label" => "Einsatzland", "mandatory" => false,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "text",
              "name" => "praktikumsstelle", "label" => "Praktikumsstelle",
              "mandatory" => false,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "text",
              "name" => "arbeitsbereich", "label" => "Arbeitsbereich im Praktikum",
              "mandatory" => false,
              "sanitize" => "text_field",
              "length" => 64,
            ],
            [
              "type" => "paragraph",
              "content" => "Für die voraussichtliche Ausreise und Rückkehr, sofern
                das genaue Datum noch nicht bekannt ist, aber eine grobe Einschätzung
                (z.B. welcher Monat) schon gegeben werden kann, bitte einfach
                ein geschätztes Datum angeben (z.B. der 1. des Monats)."
            ],
            [
              "type" => "date",
              "name" => "ausreise", "label" => "Voraussichtliche Ausreise",
              "mandatory" => false,
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
            [
              "type" => "date",
              "name" => "rueckkehr", "label" => "Voraussichtliche Rückkehr",
              "mandatory" => false,
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
          ],
        ],
        [ // Sprachkenntnisse
          "type" => "section",
          "header" => "Sprachkenntnisse",
          "fields" => [
            [
              "type" => "text",
              "name" => "sprache", "label" => "Sprache im Einsatzland",
              "mandatory" => false,
              "sanitize" => "text_field",
              "length" => 32,
            ],
            [
              "type" => "radio",
              "name" => "sprachkenntnis",
              "label" => "Aktuelle Sprachkenntnisse laut europ. Referenzrahmen (geschätzt)",
              "mandatory" => false,
              "options" => Database::get_radio_options_sprachkenntnisse(),
              "validate" => "options"
            ],
          ],
        ],
        [ // Bestätigung
          "type" => "section",
          "header" => "Bestätigung",
          "fields" => [
            [
              "type" => "checkbox",
              "name" => "pre_reg_consent",
              "label" => "Ich habe mich bereits bei meiner*m FH-Praxislehrenden
              vorangemeldet.",
              "mandatory" => true,
              "validate" => "checkbox",
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * main static function that initialises the pre registration form and
   * handles output generation
   */
  public static function display()
  {
    $pre_reg = new Pre_Registration();
    $html = "";
    $data = false;

    // check if user is logged in and if they are an admin
    // only display to visitors or, with a notification, to logged in admins
    if ( is_user_logged_in() ) {
      if ( current_user_can("manage_options") ) {
        $html .= Notification::warning([
          "message" => "Du bist als Administrator*in angemeldet."
        ]);
      } else {
        $html .= Notification::warning([
          "message" => "Du bist bereits (vor)angemeldet."
        ]);
        return $html;
      }
    }

    // create initialised form object
    $form = new Form( $pre_reg->form_config );

    // if some data was already posted, process it
    if ( isset( $_POST["gsd_pre_registration"] ) ) {
      // check the nonce first, before we process any other data
      // only with a valid nonce any submitted data should be loaded & processed
      if ( $form->validate_nonce( $_POST ) ) {
        $form->load_data( $_POST );

        // check if there have been any errors with the submitted data
        if ( ! empty( $form->errors ) ) {
          if ( ! empty( $form->errors["missing"] ) ) {
            $html .= Notification::field_issues(
              "Folgende Pflichtfelder sind nicht (oder falsch) ausgefüllt worden:",
              $form->errors["missing"],
            );
          }
          if ( ! empty( $form->errors["validation"] ) ) {
            $html .= Notification::field_issues(
              "Folgende Felder enthalten noch ungültige Zeichen:",
              $form->errors["validation"],
            );
          }
          if ( ! empty( $form->errors["length"] ) ) {
            $html .= Notification::field_issues(
              "Folgende Felder enthalten noch zu viele Zeichen:",
              $form->errors["length"],
            );
          }
        }
      }
      // with an invalid nonce, only return an error notification and quit
      else {
        $html .= Notification::nonce();
        return $html;
      }

      // if the form validation detected no errors, we can process the data
      if ( empty( $form->errors ) ) {
        $error = $pre_reg->process( $form->get_submitted_data() );
        if ( $error ) {
          $html .= Notification::error( $error );
        } else {
          // When the registration was successfull, we render a corresponding
          // message and return, because we don't need the registration form
          $html .= $pre_reg->render_success( $form->get_submitted_data() );
          return $html;
        }
      }
    }

    // render the form and return the full html output
    $html .= $form->render();
    return $html;
  }

  public function process( $data )
  {
    global $wpdb;
    $table = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;

    // check if email already exists
    $query = "SELECT email FROM $table WHERE email = %s";
    $query = $wpdb->prepare( $query, $data["email"] );
    if ( ! empty( $wpdb->get_results( $query ) ) ) {
      return [
        "message" => "Für diese e-Mail Adresse exisitert bereits ein Eintrag!",
      ];
    }

    // check if matrikelnr already exists
    $query = "SELECT matrikelnr FROM $table WHERE matrikelnr = %s";
    $query = $wpdb->prepare( $query, $data["matrikelnr"] );
    if ( ! empty( $wpdb->get_results( $query ) ) ) {
      return [
        "message" => "Für diese Matrikelnummer exisitert bereits ein Eintrag!",
      ];
    }

    // now try to store it to the database
    if ( ! $this->store( $data ) ) {
      error_log('[GSD error]: '.__FILE__.':'. __LINE__.' : ' . json_encode($data));
      return [
        "message" => "Fehler beim Schreiben der Voranmeldungsdaten in die Datenbank!<br>
        Bitte notiere die aktuelle Zeit und kontaktiere mit dieser Information
        umgehend die Projektleitung."
      ];
    }

    // no error to return
    return false;
  }

  private function store( $data )
  {
    global $wpdb;

    // empty array of column names and values to be inserted, when it is filled
    $insert_data = [];

    // those fields can be directly taken from the validated
    // form data, if they are not empty
    $take_if_not_empty = [
      "firstname",
      "lastname",
      "email",
      "birthdate",
      "hochschule_id",
      "studium",
      "matrikelnr",
      "lektorin",
      "lektorin_email",
      "einsatzorga_kontakt",
      "einsatzorga_kontakt_email",
      "einsatzland",
      "praktikumsstelle",
      "arbeitsbereich",
      "ausreise",
      "rueckkehr",
      "sprache",
      "sprachkenntnis",
    ];
    foreach ( $take_if_not_empty as $key ) {
      if ( ! empty( $data[$key] ) ) {
        $insert_data[$key] = $data[$key];
      }
    }

    // explicit checking is needed for the following fields:
    //  - einsatzorga_id
    //  - einsatzorga_other_name
    //  - einsatzorga_other_website
    if (
      ! empty( $data["einsatzorga_id"] ) &&
      $data["einsatzorga_id"] == "OTHER"
    ) {
      $insert_data["einsatzorga_other_name"] = $data["einsatzorga_other_name"];
      $insert_data["einsatzorga_other_website"] = $data["einsatzorga_other_website"];
    } else {
      $insert_data["einsatzorga_id"] = $data["einsatzorga_id"];
    }

    // the kohorte_id depends on which school was chosen
    $insert_data["kohorte_id"] = Database::get_kohorte_by_hochschule( $data["hochschule_id"] );

    // now we can insert the new row and return the number of rows
    // inserted (should be 1) or false in case of an error
    return $wpdb->insert( $wpdb->prefix . GSD_TABLE_TEILNEHMERIN, $insert_data );
  }

  public function render_success( $data )
  {
    global $wpdb;
    $admin_url = get_admin_url();

    // get label name of hochschule notifications
    $table = $wpdb->prefix . GSD_TABLE_HOCHSCHULE;
    $query = $wpdb->prepare( "SELECT name FROM $table WHERE id = %d", $data["hochschule_id"] );
    $result = $wpdb->get_results( $query );
    $fh = ! empty( $result ) ? $result[0]->name : "[Fehler: Keine Hochschule gefunden]";

    // send out email notification to admins
    $msg = "
Hello schnello,

eine neue Voranmeldung ist soeben eingetrudelt, und zwar von
**{$data["firstname"]} {$data["lastname"]}** (Matrikelnummer **{$data["matrikelnr"]}**)
von der **{$fh}**.

Gehe zu $admin_url um diese Anmeldung zu bearbeiten.

liebste grüße,
automagia
    ";
    $admins = get_users([ "role" => "Administrator" ]);
    foreach ( $admins as $admin ) {
      wp_mail($admin->data->user_email, "Neue Voranmeldung zum GSD Praktikum", $msg);
    }

    // send out mail to registrant
    $msg = "
Liebe*r {$data["firstname"]} {$data["lastname"]},

wir haben deine Voranmeldung erhalten und werden uns demnächst mit weiteren
Details bei dir melden.

Liebe Grüße,
die GSD Projektleitung
    ";
    wp_mail($data["email"], "Bestätigung der GSD Voranmeldung", $msg);

    // prepare response message and return it
    $html = Notification::success([
      "message" => "Voranmeldung gespeichert"
    ]) . "
      <p>
        Vielen Dank für das erfolgreiche Ausfüllen des Voranmelde-Formulars. Wir
        haben dir soeben eine Bestätigungsmail geschickt, dass deine Daten bei
        uns angekommen sind. Falls diese Mail nicht innerhalb der nächsten
        Stunde bei dir ankommt, überprüfe bitte zuerst deinen Spam-Folder und
        schreibe sonst an die Projektleitung (siehe Kontakt im Footer).
      </p>
      <p>
        Das GSD-Team wird nun deine Voranmelde-Daten prüfen und sobald dies
        passiert ist, bekommst du eine Nachricht, dass du offiziell für ein
        GSD-Praktikum vorangemeldet bist! Damit bekommst du dann auch die
        Zugangsdaten zu deinem Account auf der GSD-Website.
      </p>
    ";
    return $html;
  }

}
