<?php
namespace GSDDB;

require_once plugin_dir_path( __FILE__ ) . 'class-form.php';
require_once plugin_dir_path( __FILE__ ) . 'class-notification.php';
require_once plugin_dir_path( __FILE__ ) . 'class-database.php';

class Full_Registration
{
  private $participant;
  private $contacts_local;
  private $contacts_remote;
  private $files;
  private $schools;
  private $orgas;
  private $section_person;
  private $section_school;
  private $section_orga;
  private $section_internship;
  private $section_language;
  private $section_contacts_local;
  private $section_contacts_remote;
  private $section_files;
  private $section_subsidy;
  private $section_final;

  public function __construct( $participant_data ) {
    global $wpdb;
    $this->participant = $participant_data;

    // initialise schools
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_HOCHSCHULE);
    foreach ( $r as $row ) {
      $this->schools[ $row->id ] = $row->name;
    }

    // initialise organisations
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_EINSATZORGA);
    foreach ( $r as $row ) {
      $this->orgas[ $row->id ] = [
        "name" => $row->name,
        "website" => $row->website
      ];
    }

    // initialise contacts
    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_NOTFALLKONTAKT .
      " WHERE teilnehmerin_id = %d AND ist_im_einsatzland = 0";
    $q = $wpdb->prepare( $q, $participant_data["id"] );
    $this->contacts_local = $wpdb->get_row( $q, ARRAY_A );

    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_NOTFALLKONTAKT .
      " WHERE teilnehmerin_id = %d AND ist_im_einsatzland = 1";
    $q = $wpdb->prepare( $q, $participant_data["id"] );
    $this->contacts_remote = $wpdb->get_row( $q, ARRAY_A );

    // TODO: initialise files
    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_FILE .
      " WHERE teilnehmerin_id = %d";
    $q = $wpdb->prepare( $q, $participant_data["id"] );
    $this->files = $wpdb->get_results( $q, ARRAY_A );

    // configure the form sections
    $this->section_person = [
      "title" => "Persönliche Daten",
      "slug" => "person",
      "submit_label" => "Zwischenspeichern",
      "fixed_fields" => [
        [
          "name" => "email",
          "label" => "E-Mailadresse", "type" => "email", "sanitize" => "email",
        ],
        [
          "name" => "birthdate",
          "label" => "Geburtsdatum", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
        ],
      ],
      "fields" => [
        [
          "name" => "firstname",
          "label" => "Vorname", "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "lastname",
          "label" => "Nachname", "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "adresse", "label" => "Adressdaten", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 256,
        ],
        [
          "name" => "telefon", "label" => "Telefonnummer", "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "skype", "label" => "Skypekontakt", "type" => "text",
          "sanitize" => "text_field",
          "length" => 64,
        ],
        [
          "name" => "staatsangehoerigkeit", "label" => "Staatsangehoerigkeit",
          "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "passnummer", "label" => "Reisepassnummer", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "ausstellungsdatum", "label" => "Reisepass Ausstellungsdatum",
          "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "ausstellungsort", "label" => "Reisepass Ausstellungsort",
          "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "studienbeihilfe",
          "label" => "Ich beziehe Studienbeihilfe/Selbsterhalterstipendium",
          "type" => "checkbox", "validate" => "checkbox",
        ],
        [
          "name" => "anmerkungen",
          "label" => "Evtl. Erkrankungen, Allergien, spezielle Bedürfnisse",
          "type" => "textarea",
          "sanitize" => "textarea_field",
        ],
      ],
    ];
    $this->section_school = [
      "title" => "FH-bezogene Daten",
      "slug" => "hochschule",
      "submit_label" => "Zwischenspeichern",
      "fixed_fields" => [
        [
          "name" => "hochschule_id",
          "label" => "Fachhochschule", "type" => "school",
          "options" => Database::get_dropdown_array_hochschule(),
        ],
        [
          "name" => "matrikelnr",
          "label" => "Matrikelnummer", "type" => "text",
        ],
      ],
      "fields" => [
        [
          "type" => "radio",
          "name" => "studium", "label" => "Studiengang",
          "options" => Database::get_radio_options_studien(),
          "validate" => "options",
          "mandatory" => true,
        ],
        [
          "name" => "vollzeit",
          "label" => "Vollzeitstudium (nicht ankreuzen für berufsbegleitende Studiengänge):",
          "type" => "checkbox",
          "validate" => "checkbox"
        ],
        [
          "name" => "lektorin",
          "label" => "Name FH-Praxislektor*in", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "lektorin_email",
          "label" => "E-Mailadresse FH-Praxislektor*in", "type" => "email",
          "sanitize" => "email",
          "mandatory" => true,
          "length" => 128,
        ],
        [
          "name" => "lektorin_telefon",
          "label" => "Telefon FH-Praxislektor*in",
          "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "length" => 32,
        ],
      ],
    ];
    $this->section_orga = [
      "title" => "Einsatzorganisation",
      "slug" => "einsatzorganisation",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "name" => "einsatzorga_id", "label" => "Einsatzorganisation",
          "type" => "einsatzorga",
          "options" => Database::get_dropdown_array_einsatzorga(),
          "options_extended" => Database::get_einsatzorgas(),
          "validate" => "options",
          "alternative_fields" => [
            "einsatzorga_other_name" => [ "label" => "Wenn \"Sonstige\", Name der Einsatzorganisation" ],
            "einsatzorga_other_website" => [ "label" => "Wenn \"Sonstige\", Website der Einsatzorganisation" ],
          ],
          "mandatory" => true,
        ],
        [
          "name" => "einsatzorga_kontakt",
          "label" => "Name Kontaktperson der Einsatzorganisation",
          "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "einsatzorga_kontakt_email",
          "label" => "E-Mailadresse Kontaktperson der Einsatzorganisation",
          "type" => "text", "sanitize" => "email",
          "mandatory" => true,
          "length" => 128,
        ],
        [
          "name" => "einsatzorga_kontakt_telefon",
          "label" => "Telefon Kontaktperson der Einsatzorganisation",
          "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "length" => 32,
        ],
      ],
    ];
    $this->section_internship = [
      "title" => "Praktikumsstelle",
      "slug" => "praktikum",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "name" => "einsatzland",
          "label" => "Einsatzland", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "praktikumsstelle",
          "label" => "Name (und ggf. Abkürzung) Praktikumsstelle", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "praktikumsstelle_adresse",
          "label" => "Adresse Praktikumsstelle", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 256,
        ],
        [
          "name" => "praktikumsstelle_telefon",
          "label" => "Telefon Praktikumsstelle",
          "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "length" => 32,
        ],
        [
          "name" => "praktikumsstelle_website",
          "label" => "Website Praktikumsstelle", "type" => "text",
          "sanitize" => "url",
          "length" => 64,
        ],
        [
          "name" => "arbeitsbereich",
          "label" => "Arbeitsbereich", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "praktikumsstelle_kontakt",
          "label" => "Name Praxisanleiter*in vor Ort", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "praktikumsstelle_kontakt_email",
          "label" => "E-Mailadresse Praxisanleiter*in vor Ort", "type" => "email",
          "sanitize" => "email",
          "length" => 128,
        ],
        [
          "name" => "praktikumsstelle_kontakt_telefon",
          "label" => "Telefon Praxisanleiter*in vor Ort", "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "length" => 32,
        ],
        [
          "name" => "ausreise",
          "label" => "Ausreise", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "rueckkehr",
          "label" => "Rückreise", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "praktikum_beginn",
          "label" => "Praktikumsbeginn", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "praktikum_ende",
          "label" => "Praktikumsende", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "praktikum_dauer",
          "label" => "Praktikumsdauer (in Wochen)", "type" => "number",
          "validate" => "regex", "regex" => '/^[0-9]+$/',
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle_beschreibung",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Beschreibung Praktikumsstelle (mind. 5 – höchstens 10 Zeilen)",
          "mandatory" => true,
          "explanation" => "Kurzbeschreibung, Zielsetzungen,
            Zielgruppen, Angebote etc.
            Bitte Quelle angeben (Einsatzorganisation, Schreiben der
            Praktikumsstelle vom …, Internetadresse, Bericht … etc.)",
        ],
        [
          "name" => "arbeitsbereich_sektor",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Arbeitsbereiche/Sektoren, in denen die/der Praktikant*in
            tätig sein wird (max. 2 Zeilen)",
          "mandatory" => true,
          "explanation" => "Beispiel: Arbeit mit Straßenkindern /Kinderarbeiter*innen,
            Gemeinwesenarbeit, Gesundheit, Arbeit mit Mädchen und
            Frauen in der Sexarbeit, ….
            Bitte Quelle angeben (Einsatzorganisation, Schreiben der
            Praktikumsstelle vom …, Internetadresse, Bericht … etc.)",
        ],
        [
          "name" => "arbeitsbereich_anforderung",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Tätigkeitsbereich / Anforderungsprofil Praktikant*in (mind. 5 – höchstens 10 Zeilen)",
          "mandatory" => true,
          "explanation" => "
            Beispiel: Mitgestaltung des Alltags der Kinder (Spiele, Essen etc.),
            Übernehmen von kleineren administrativen Aufgaben (Ansuchen um
            finanzielle Unterstützung etc.), Kontaktaufnahme mit
            Sozialarbeiter*innen der Organisation, ….
            Bitte Quelle angeben (Einsatzorganisation, Schreiben der
            Praktikumsstelle vom …, Internetadresse, Bericht … etc.)",
        ],
        [
          "name" => "erfahrung",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Bisherige Erfahrung",
          "mandatory" => true,
          "explanation" => "(Tätigkeit der/des Studierenden im
            Sozial- und Gesundheitsbereich / Internationaler Zusammenarbeit;
            auch einschlägige Auslandsaufenthalte, Praktika)",
        ],
      ],
    ];
    $this->section_language = [
      "title" => "Sprachkenntnisse",
      "slug" => "sprachkenntnisse",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "name" => "sprache",
          "label" => "Sprache Einsatzland", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "sprachkenntnis_testergebnis",
          "type" => "dropdown",
          "label" => "Aktuelle Sprachkenntnisse (nach europ. Referenzrahmen)
            laut Sprachtest",
          "explanation" => "Bitte geben Sie hier das Testergebnis Ihres
            Sprachtests im europäischen Sprachenportfolio Referenzrahmen
            A1 bis C2 an und laden Sie Ihr Testergebnis weiter unten im
            Bereich „Hochladen der Anmeldeunterlagen“ als Datei hoch",
          "options" => Database::get_dropdown_array_sprechkenntnisse(),
          "mandatory" => true,
        ],
        [
          "name" => "sprachkenntnis_planung",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Sollte dieses Ergebnis unter B2 liegen, führen Sie hier
            bitte genau an, welche Maßnahmen Sie unternehmen werden, um dieses
            Niveau spätestens bis zum Praktikumsbeginn zu
            erreichen und wenn nötig nachträglich nachweisen zu können",
        ],
        [
          "name" => "sprachkurs",
          "label" => "Sprachkurs vor Ort", "type" => "language_course",
          "validate" => "checkbox",
          "fields_if_yes" => [
            "sprachkurs_institut" => [ "label" => "Sprachkurs Institut" ],
            "sprachkurs_institut_website" => [ "label" => "Sprachkurs Instituts-Website" ],
            "sprachkurs_beginn" => [ "label" => "Sprachkurs Beginn", "type" => "date" ],
            "sprachkurs_ende" => [ "label" => "Sprachkurs Ende", "type" => "date" ],
          ]
        ],
      ],
    ];
    $this->section_contacts_local = [
      "title" => "Notfallkontakt Zuhause",
      "slug" => "notfallkontakte_local",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "name" => "fullname",
          "label" => "Vor- und Nachname",
          "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "beziehungsgrad",
          "label" => "Beziehungsgrad",
          "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "email",
          "label" => "e-Mailadresse",
          "type" => "email", "sanitize" => "email",
          "length" => 128,
        ],
        [
          "name" => "telefon",
          "label" => "Telefonnummer",
          "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "skype", "label" => "Skypekontakt", "type" => "text",
          "sanitize" => "text_field",
          "length" => 64,
        ],
        [
          "name" => "adresse",
          "label" => "Adresse",
          "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 256,
        ],
        [
          "name" => "anmerkungen",
          "label" => "Allfällige wichtige Informationen, ggf. Daten weitere
          Kontaktpersonen",
          "type" => "textarea", "sanitize" => "textarea_field",
        ],
        [
          "name" => "ist_im_einsatzland",
          "label" => "Hidden Checkbox: nicht im Einsatzland",
          "type" => "hidden", "value" => "0",
          "validate" => "regex", "regex" => '/^[01]$/',
        ],
      ],
    ];
    $this->section_contacts_remote = [
      "title" => "Notfallkontakt im Einsatzland",
      "slug" => "notfallkontakte_remote",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "type" => "paragraph",
          "content" => "(Zusätzlich zur Praktikumsstelle, Landesvertretung
          und Einsatzorganisation – kann auch erst vor Ort nachgereicht werden)",
        ],
        [
          "name" => "fullname",
          "label" => "Vor- und Nachname",
          "type" => "text", "sanitize" => "text_field",
          "length" => 64,
        ],
        [
          "name" => "beziehungsgrad",
          "label" => "Beziehungsgrad",
          "type" => "text", "sanitize" => "text_field",
          "length" => 32,
        ],
        [
          "name" => "email",
          "label" => "e-Mailadresse",
          "type" => "email", "sanitize" => "email",
          "length" => 128,
        ],
        [
          "name" => "telefon",
          "label" => "Telefonnummer",
          "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "length" => 32,
        ],
        [
          "name" => "skype", "label" => "Skypekontakt", "type" => "text",
          "sanitize" => "text_field",
          "length" => 64,
        ],
        [
          "name" => "adresse",
          "label" => "Adresse",
          "type" => "text", "sanitize" => "text_field",
          "length" => 256,
        ],
        [
          "name" => "anmerkungen",
          "label" => "Allfällige wichtige Informationen, ggf. Daten weiterer
          Kontaktpersonen",
          "type" => "textarea", "sanitize" => "textarea_field",
        ],
        [
          "name" => "ist_im_einsatzland",
          "label" => "Hidden Checkbox: im Einsatzland",
          "type" => "hidden", "value" => "1",
          "validate" => "regex", "regex" => '/^[01]$/',
        ],
      ],
    ];
    $this->section_files = [
      "title" => "Hochladen der Anmeldeunterlagen",
      "slug" => "files",
      "submit_label" => "Zwischenspeichern",
      "enctype" => "multipart/form-data",
      "fields" => [
        [
          "name" => "picture",
          "label" => "Foto",
          "type" => "file",
          "allowed" => ["jpg", "jpeg", "png"],
          "validate" => "file",
        ],
        [
          "name" => "motivation",
          "label" => "Motivationsschreiben",
          "type" => "file",
          "allowed" => ["pdf"],
          "validate" => "file",
        ],
        [
          "name" => "identity",
          "label" => "Identitätsnachweis",
          "type" => "file",
          "allowed" => ["pdf"],
          "validate" => "file",
        ],
        [
          "name" => "languagetest",
          "label" => "Sprachtestergebnis",
          "type" => "file",
          "allowed" => ["pdf"],
          "validate" => "file",
        ],
      ],
    ];
    $this->section_subsidy = [
      "title" => "Zuschussansuchen",
      "slug" => "subsidy",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "type" => "paragraph",
          "content" => "Ansuchen um gesamten OEZA-Zuschuss:"
        ],
        [
          "type" => "paragraph",
          "content" => "Der OEZA-Zuschuss kann bis zu insgesamt 700€ gewährt
            werden. Die Überweisung des OEZA-Zuschusses erfolgt nach dem
            Reflexionsseminar und nach Erfüllung folgender Kriterien:",
        ],
        [
          "type" => "paragraph",
          "content" => "
            <ul>
              <li>KEIN Erhalt von Studienbeihilfe/Selbsterhalterstipendium
                und/oder KEINE Landesförderung und/oder andere Förderung aus
                öffentlichen Mitteln während des Praktikumssemesters</li>
              <li>Teilnahme an der Seminarreihe (Vorbereitungs- und
                Reflexionsseminar)</li>
              <li>Praktikumsbestätigung durch die Praktikumsstelle</li>
              <li>Studienerfolgsnachweis (FH) über das absolvierte Praktikum</li>
              <li>Einreichung des Datenblattes nach Rückkehr</li>
              <li>Einreichung der Bestätigung „Übergabe Zuschuss an
                Praktikumsstelle</li>
            </ul>
            "
        ],
        [
          "name" => "oeza_zuschuss",
          "type" => "checkbox", "validate" => "checkbox",
          "label" => "Ich erkläre, dass ich KEINE Studienbeihilfe/Selbsterhalterstipendium
            während des Praktikumssemesters beziehen werde und/oder KEINE
            Landesförderung und/oder andere öffentliche Förderungen beziehen
            kann bzw. den Kriterien für den Erhalt dieser Förderungen nicht
            entspreche. Daher suche ich hiermit um Zuschuss aus Mitteln der
            Österreichischen Entwicklungszusammenarbeit (OEZA) - genannt
            OEZA-Zuschuss - an. Ich verpflichte mich bei Nichterfüllung bzw.
            nicht zeitgerechter Einhaltung der Zuschusskriterien (wie oben definiert)
            zur Rückerstattung eines ggf. bereits ausbezahlten OEZA-Zuschusses. ",
        ],
        [
          "type" => "paragraph",
          "content" => "<br>ODER:",
        ],
        [
          "type" => "paragraph",
          "content" => "Ansuchen um ergänzenden OEZA-Zuschuss:"
        ],
        [
          "type" => "paragraph",
          "content" => "Ein ergänzender OEZA-Zuschuss kann gewährt werden, wenn
            anderswo eingebrachte Förderansuchen insgesamt weniger als 700€ an
            öffentlichen Mitteln erbringen (Beispiele für andere öffentliche
            Zuschüsse: SUS des BMWFW für Bezieherinnen von
            Studienbeihilfe/Selbsterhalterstipendium; Landesförderung Kärnten:
            Lernen ohne Grenzen; Landesförderung Niederösterreich:
            TOP-Stipendium... o.a.). Der ergänzende OEZA-Zuschuss kann derartige
            andere öffentliche Zuschüsse auf insgesamt 700€ ergänzen. Die
            Überweisung des ergänzenden OEZA-Zuschusses erfolgt ebenfalls nach
            dem Reflexionsseminar und nach Erfüllung folgender Kriterien:",
        ],
        [
          "type" => "paragraph",
          "content" => "
            <ul>
              <li>Erhalt von Studienbeihilfe/Selbsterhalterstipendium
                und/oder Landesförderung und/oder andere Förderung aus öffentlichen
                Mitteln während des Praktikumssemesters</li>
              <li>Teilnahme an der Seminarreihe (Vorbereitungs- und Reflexionsseminar)</li>
              <li>Praktikumsbestätigung durch die Praktikumsstelle</li>
              <li>Studienerfolgsnachweis (FH) über das absolvierte Praktikum</li>
              <li>Einreichung des Datenblattes nach Rückkehr</li>
              <li>Einreichung der Bestätigung „Übergabe Zuschuss an Praktikumsstelle</li>
            </ul>
            "
        ],
        [
          "name" => "oeza_zuschuss_ergaenzend",
          "type" => "checkbox", "validate" => "checkbox",
          "label" => "Ich erkläre, dass ich Studienbeihilfe/Selbsterhalterstipendium
            und/oder eine Landesförderung und/oder eine andere Förderung aus
            öffentlichen Mitteln während des Praktikumssemesters beziehen werde.
            Somit ersuche ich um ergänzenden OEZA-Zuschuss an. Ich werde dafür
            meine anderen öffentlichen Zuschuss-Ansuchen sowie die
            Antwortschreiben ehestmöglich kommunizieren.",
        ],
      ]
    ];
    $this->section_final = [
      "title" => "Verpflichtungserklärung",
      "slug" => "final",
      "submit_label" => "Speichern",
      "fields" => [
        [
          "type" => "paragraph",
          "content" => "Ich verpflichte mich, die im Folgenden genannten
            Punkte zu erfüllen:",
        ],
        [
          "type" => "paragraph",
          "content" => "
            <ul>
              <li>Die Lernziele mit der Praktikumsstelle zeitgerecht zu vereinbaren</li>
              <li>Die Seminarreihe (Vorbereitungs- und Nachbereitungsseminare)
                zur Gänze zu besuchen</li>
              <li>Die individuelle inhaltliche und praktische Vorbereitung
                auf das Einsatzland und Projekt durchzuführen:
              </li>
              <ul>
                <li>Inhaltliche Auseinandersetzung mit dem lokalen
                  und globalen Kontext der Praktikumsstelle, der Tätigkeit und
                  der Zielgruppe</li>
                <li>Austausch mit Rückkehrer*innen, Expert*innen,
                  Personen, aktuellen Informationen aus dem Einsatzland</li>
                <li>Gegebenenfalls Erstellung von praktikumsbezogenen
                  Unterlagen zur Präsentation bei den Seminarreihen</li>
                <li>Selbstständige Erledigung von allen organisatorischen
                  Schritten wie Flugbuchung, Versicherungen, Impfungen, Visa etc.</li>
              </ul>
              <li>An den Einführungs- und Nachbereitungstreffen meiner
                Einsatzorganisation teilzunehmen und die Termine dafür zeitgerecht
                individuell mit dieser zu vereinbaren (1. Termin möglichst vor dem
                Vorbereitungsseminar)</li>
              <li>Die Amts- oder Verkehrssprache der Praktikumsstelle
                (Englisch, Französisch, Spanisch oder Portugiesisch) zu beherrschen
                und dies mit Sprachtest nachzuweisen (Niveau mind. B2 nach dem
                europäischen Referenzrahmen).</li>
              <li>Dafür ggf. auch einen notwendigen mehrwöchigen Sprachkurs
                vor dem Praktikum (ggf. vor Ort) zu planen und zu absolvieren.</li>
              <li>Grundkenntnisse der Lokalsprache möglichst frühzeitig
                vor der Ausreise zu erwerben.</li>
              <li>Den Teilnahmebeitrag zum GSD-Praktikaprogramm von
                350€ ggf. zu leisten (dieser Beitrag wird jedoch in der Regel von
                FH-Studiengängen zum Teil oder zur Gänze übernommen – Infos dazu
                erfolgen bei den Informationsveranstaltungen).</li>
              <li>Entsprechende Zuschussansuchen laut bereitgestellten
                Informationen zu erstellen.</li>
              <li>Mich um einen angemessenen Versicherungsschutz (Kranken-,
                Unfall- und Haftpflichtversicherung) zu kümmern.</li>
              <li>Die Absolvierung des Praktikums von der Praktikumsstelle
                im Einsatzland bestätigen zu lassen. Die Wochen-Mindestdauer verkürzt
                sich nicht bei Leistung einer erhöhten Wochenstundenanzahl. Dauer ohne
                Sprachkurs gewöhnlich mindestens 15 Wochen; Berufsbegleitender Studiengang:
                Dauer ohne Sprachkurs gewöhnlich mindestens 10 Wochen.</li>
              <li>Ggf. an internen und externen (Vernetzungs-)Veranstaltungen
                der FH sowie der Einsatzorganisation im Zusammenhang mit der
                Absolvierung des Berufspraktikums mitzuarbeiten.</li>
              <li>Ggf. zur Vernetzung mit mir nachfolgenden Praktikant*innen
                zur Verfügung zu stehen und zu diesem Zweck der Übergabe meiner
                Kontaktdaten (Name, E-Mail-Adresse, Telefonnummer) zuzustimmen.</li>
              <li>Zum Zweck der Informationsweitergabe an nachfolgende
                Praktikant*innen im Rahmen der Rückkehrmeldung Fragen zum
                Praktikumsaufenthalt (=Teilnahmebericht) zu beantworten, die
                anonymisiert auf der GSD-Webseite veröffentlich werden können.</li>
            </ul>
            ",
        ],
        [
          "type" => "paragraph",
          "content" => "Erklärung:",
        ],
        [
          "type" => "paragraph",
          "content" => "Ich absolviere mein Praktikum in einem Land Afrikas,
            Asiens oder Lateinamerikas eigenverantwortlich aus freien Stücken
            und bin mir bewusst, dass damit ggf. besondere physische und
            psychische Risiken verbunden sind. Ich bestätige, im Zusammenhang
            mit meinem Auslandsaufenthalt selbst für ausreichenden
            Versicherungsschutz zu sorgen und sicher zu gehen, dass zum
            Zeitpunkt der Ausreise ein vollständiger Impfschutz gegen COVID-19
            aufrecht ist.",
        ],
        [
          "type" => "paragraph",
          "content" => "Hiermit bestätige ich weiter, dass meine Notfallkontakte
            mit der Weitergabe ihrer/seiner Daten einverstanden sind und im
            Notfall kontaktiert werden dürfen.",
        ],
        [
          "type" => "paragraph",
          "content" => "Des Weiteren werde ich für die Zeit meines
            Auslandsaufenthaltes der zuständigen Vertretung meines Landes meine
            Reisedaten für den Notfall zur Kenntnis bringen (z.B. durch
            Reiseregistrierung beim Außenministerium).",
        ],
        [
          "type" => "paragraph",
          "content" => "Datenverarbeitungsklausel:",
        ],
        [
          "type" => "paragraph",
          "content" => "Ich nehme zur Kenntnis , dass sämtliche personenbezogene
            Daten vom GSD-Praktikaprogramm, von der Programmträgerin FH Campus
            Wien sowie von Mitarbeiter*innen meiner Fachhochschule für die
            Zwecke der Programmverwaltung, Evaluierung und Vernetzung
            automationsunterstützt für 10 Jahre gespeichert, verarbeitet und
            verwendet werden können, sowie zum Zweck der Projektabrechnung der
            Fördergeberin übermittelt werden. ",
        ],
        [
          "type" => "paragraph",
          "content" => "Weiters  nehme ich zur Kenntnis, dass mein Teilnahmebericht
            im Rahmen der Rückkehrmeldung anonymisiert auf der Website
            https://globalsocialdialog.at/ veröffentlicht wird. Details zur
            Datenschutzerklärung sind auch unter
            https://globalsocialdialog.at/datenschutz/ zu finden.",
        ],
        [
          "name" => "datenverarbeitung",
          "type" => "checkbox", "validate" => "checkbox",
          "label" => "Weiters willige ich ein, dass meine Daten zum Zwecke der
            Übermittlung von Programmneuigkeiten und  Veranstaltungseinladungen
            per E-Mail sowie zur Vernetzung von Alumni*ae  auch darüber hinaus
            gespeichert werden dürfen und zum Zweck der Informationsweitergabe
            vereinzelt an nachfolgende Praktikant*innen übermittelt werden
            können (durch die Programmverantwortlichen).",
        ],
        [
          "type" => "paragraph",
          "content" => "Dort, wo meine Einwilligung in die Datenverarbeitung
            erforderlich ist, habe ich das Recht, meine Einwilligung jederzeit
            (mündlich oder schriftlich) gegenüber der FH Campus Wien und/oder
            meiner Fachhochschule zu widerrufen. Der Widerruf meiner
            Einwilligung bezieht sich nicht auf Daten, die bis zu meinem
            Widerruf bereits verarbeitet worden sind.",
        ],
        [
          "name" => "verpflichtungserklaerung",
          "type" => "checkbox", "validate" => "checkbox",
          "mandatory" => true,
          "label" => "Ich stimme hiermit allen oben genannten Bedingungen zu und
            erkläre alle Angaben richtig und vollständig gemacht zu haben.",
        ],
        [
          "type" => "paragraph",
          "content" => "Wenn du diesen Abschnitt speicherst und alle
            Pflichtfelder ausgefüllt sind, erscheint der Button „Datenblatt
            speichern und einreichen“. Auch nach dem Einreichen kannst du
            jederzeit Änderungen vornehmen und in den einzelnen
            Formularabschnitten abspeichern. Wir überprüfen deine Daten nach
            der Fixanmeldefrist.",
        ],
      ],
    ];
  }

  /**
   * main static function that is called through the shorttag.
   * it initialises the pre registration form and hands over controll to the
   * controller
   */
  public static function display()
  {
    $html = "";

    if ( ! is_user_logged_in() ) {
      $html .= Notification::error([
        "message" => 'Du bist nicht eingelogged. Dieser Bereich ist nur mit deinem
        GSD-Account nutzbar. Wenn du die Voranmeldung schon ausgefüllt hast und
        dein Account auch nach mehreren Tagen noch nicht freigeschalten
        wurde, melde dich bitte bei der Projektadministration.'
      ]);
      $html .= '<p align="center"><a href="/wp-login.php">Zum Login</a></p>';
      return $html;
    }

    global $wpdb;
    $user = wp_get_current_user();
    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_TEILNEHMERIN .
      " WHERE matrikelnr = %s";
    $q = $wpdb->prepare( $q, $user->data->user_login );
    $r = $wpdb->get_row( $q, ARRAY_A );

    if( empty( $r ) ) {
      if ( current_user_can("manage_options") ) {
        $html .= Notification::warning([
          "message" => "Du bist als Administrator*in angemeldet. Zum Bearbeiten
            einzelner Teilnehmer*innen verwende bitte das Backend."
        ]);
      } else {
        $html .= Notification::error([
          "message" => 'Es sind keine Daten in der GSD Datenbank zu deinem Account vorhanden!<br>
          Bitte kontaktiere umgehend die Projektadministration!'
        ]);
      }
      return $html;
    }

    $full_reg = new Full_Registration( $r );
    $html .= $full_reg->controller();

    return $html;
  }

  /**
   * this is the main entry into the form's processing, which should be called
   * by the static display function, right after it has instatiated the full_reg
   * object
   */
  public function controller () {
    $html = '';

    // should we present and edit form rather then the overview?
    if ( isset( $_GET["edit"] ) ) {
      $edit = true;
      switch( $_GET["edit"] ) {
        case "person":
          $html .= $this->handle_section( $this->section_person, $this->participant );
          return $html;

        case "hochschule":
          $html .= $this->handle_section( $this->section_school, $this->participant );
          return $html;

        case "einsatzorganisation":
          $html .= $this->handle_section( $this->section_orga, $this->participant );
          return $html;

        case "praktikum":
          $html .= $this->handle_section( $this->section_internship, $this->participant );
          return $html;

        case "sprachkenntnisse":
          $html .= $this->handle_section( $this->section_language, $this->participant );
          return $html;

        case "notfallkontakte_local":
          $html .= $this->handle_section( $this->section_contacts_local, $this->contacts_local );
          return $html;

        case "notfallkontakte_remote":
          $html .= $this->handle_section( $this->section_contacts_remote, $this->contacts_remote );
          return $html;

        case "files":
          $html .= $this->handle_section( $this->section_files, $this->files );
          return $html;

        case "subsidy":
          $html .= $this->handle_section( $this->section_subsidy, $this->participant );
          return $html;

        case "final":
          $html .= $this->handle_section( $this->section_final, $this->participant );
          return $html;

        default:
          $html .= Notification::error([
            "message" => "The chosen section is not editable!"
          ]);
          return $html;
      }
    } elseif ( isset( $_GET["submit-pre-reg"] ) ) {
      if ( $this->missing_info() ) {
        $html .= Notification::error([
          "message" => "Formular-Daten unvollständig!<br>
          Bitte notiere die aktuelle Zeit und kontaktiere mit dieser Information
          umgehend die Projektleitung."
        ]);
        return $html;
      }
      if ( $this->complete_submission() !== 1 ) {
        $html .= Notification::error([
          "message" => "Datenbank-Fehler beim Einreichen der Formular-Daten!<br>
          Bitte notiere die aktuelle Zeit und kontaktiere mit dieser Information
          umgehend die Projektleitung."
        ]);
        return $html;
      }
      $html .= $this->render_submission_success();
      return $html;
    }

    $html .= $this->render_intro();
    $html .= $this->render_comments( $this->participant );
    $html .= $this->render_section( $this->section_person, $this->participant );
    $html .= $this->render_section( $this->section_school, $this->participant );
    $html .= $this->render_section( $this->section_orga, $this->participant );
    $html .= $this->render_section( $this->section_internship, $this->participant );
    $html .= $this->render_section( $this->section_language, $this->participant );
    $html .= $this->render_section( $this->section_contacts_local, $this->contacts_local );
    $html .= $this->render_section( $this->section_contacts_remote, $this->contacts_remote );
    $html .= $this->render_section( $this->section_files, $this->files );
    $html .= $this->render_section( $this->section_subsidy, $this->participant );
    $html .= $this->render_section( $this->section_final, $this->participant );

    $html .= '
    <div class="data-section print-button">
    <a href="'.plugin_dir_url(__FILE__).'print-datasheet.php" target="_blank">
    <span class="dashicons dashicons-printer"></span> Datenblatt drucken
    </a>
    </div>
    ';
    if ( $this->participant["verpflichtungserklaerung"] ) {
      switch ( $this->participant["status"] ) {
        case 1:
          $missing = $this->missing_info();
          if ( ! $missing ) {
            $html .= '
            <div class="data-section print-button">
            <a href="?submit-pre-reg=true">
            <span class="dashicons dashicons-upload"></span> Datenblatt speichern und einreichen
            </a>
            </div>
            ';
          } else {
            $html .= '
            <div class="warning">
              <span class="dashicons dashicons-bell"></span> <b>Datenblatt noch nicht einreichbar</b><br>
              <p>
                Es fehlen noch folgende Daten um das Datenblatt einreichen zu können:
              </p>
              <ul>';
            foreach( $missing as $fieldname ) {
              $html .= '
                <li>'.$fieldname.'</li>';
            }
            $html .= '
              </ul>
            </div>
            ';
          }
          break;
        case 12:
          $html .= '
          <div class="data-section status-info">
            Deine Fixanmeldung ist bereits eingereicht und wird von der
            Projektleitung geprüft.
          </div>
          ';
          break;
        case 2:
          $html .= '
          <div class="data-section status-info">
            Du bist für das GSD-Praktikum fix angemeldet.
          </div>
          ';
          break;
        case 3:
          $html .= '
          <div class="data-section status-info">
            Dein GSD-Praktikum wurde abgebrochen.
          </div>
          ';
          break;
      }
    }

    return $html;
  }

  private function missing_info() {
    $missing = [];
    $sections = [
      $this->section_person, $this->section_school, $this->section_orga,
      $this->section_internship, $this->section_language,
    ];
    foreach ( $sections as $section ) {
      foreach ( $section["fields"] as $field ) {
        if ( isset($field["name"]) && isset($field["mandatory"]) ) {
          if ( empty($this->participant[$field["name"]]) ) {
            // values explicitly set to false (:= 0) (e.g. in checkboxes) are ok
            if ( $this->participant[$field["name"]] != "0" ) {
              $missing[] = $field["name"];
            }
          }
        }
      }
    }
    // check if all needed files are uploaded
    $files_needed = ['picture', 'motivation', 'identity', 'languagetest'];
    foreach ( $this->files as $file ) {
      if ( in_array($file['type'], $files_needed) ) {
        $i = array_search($file['type'], $files_needed);
        unset($files_needed[$i]);
      }
    }
    foreach ( $files_needed as $f ) {
      $missing[] = "Datei: $f";
    }
    // check if data for local emergency contact was entered
    foreach ( $this->section_contacts_local["fields"] as $field ) {
      if ( isset($field["name"]) && isset($field["mandatory"]) ) {
        if ( !isset($this->contacts_local[$field["name"]]) ) {
          $missing[] = "Notfallkontakt Lokal: ".$field["name"];
        }
      }
    }
    // check if data for remote emergency contact was entered
    foreach ( $this->section_contacts_remote["fields"] as $field ) {
      if ( isset($field["name"]) && isset($field["mandatory"]) ) {
        if ( !isset($this->contacts_remote["name"]) ) {
          $missing[] = "Notfallkontakt im Einsatzland: ".$field["name"];
        }
      }
    }
    // for the organisation we either need einsatzorga_id or name and website
    if ( ! empty($missing) ) {
      if ( in_array("einsatzorga_id", $missing) ) {
        // if einsatzorga_id is missing, we have to make sure we have the other two
        // only then we can remove einsatzorga_id
        if (
          !in_array("einsatzorga_other_name", $missing) &&
          !in_array("einsatzorga_other_website", $missing)
        ) {
          $i = array_search("einsatzorga_id", $missing);
          unset($missing[$i]);
        }
      } else {
        // when einsatzorga_id is available we can remove the others (if the exist)
        if (($i = array_search("einsatzorga_other_name", $missing)) !== false) {
          unset($missing[$i]);
        }
        if (($i = array_search("einsatzorga_other_website", $missing)) !== false) {
          unset($missing[$i]);
        }
      }
    }
    return $missing;
  }

  private function handle_section( $section, $data ) {
    $html = '';

    $form = new Form( $section );
    // we'll load the whole data set into the fixed data section, in case
    // any form field is included that is not editable but should be displayed
    // with the value already set in the DB
    $form->load_fixed_data( $data );

    // if post data was already submitted, we'll process it
    if ( isset( $_POST[ $section["slug"] ] ) ) {
      if ( ! $form->validate_nonce( $_POST ) ) {
        $html .= Notification::nonce();
        return $html;
      }
      $form->load_data( $_POST );
      // check if there have been any errors with the submitted data
      if ( ! empty( $form->errors ) ) {
        if ( ! empty( $form->errors["missing"] ) ) {
          $html .= Notification::field_issues(
            "Folgende Pflichtfelder sind nicht (oder falsch) ausgefüllt worden:",
            $form->errors["missing"],
          );
        }
        if ( ! empty( $form->errors["validation"] ) ) {
          $html .= Notification::field_issues(
            "Folgende Felder enthalten noch ungültige Zeichen:",
            $form->errors["validation"],
          );
        }
      }
      // if the form validation detected no errors, we can store the data
      else {
        // if nothing was updated we'll receive 0, otherwise 1 or more.
        // but in case of an error we'll receive false
        $retval = $this->store( $section, $form->get_submitted_data() );
        if ( $retval === false ) {
          return Notification::error([
            "message" => "Fehler beim Schreiben der Voranmeldungsdaten in die Datenbank!<br>
            Bitte notiere die aktuelle Zeit und kontaktiere mit dieser Information
            umgehend die Projektleitung."
          ]);
        } else if ( $retval === 0 ) {
          $html .= Notification::warning([
            "message" => "Daten wurden nicht verändert. (Leere Felder werden nicht aktualisiert!)"
          ]);
        } else {
          // When the registration was successfull, we render a corresponding
          // message and return, because we don't need the registration form
          $html .= $this->render_success();
          return $html;
        }
      }
    }

    // if nothing was yet submitted, we have to still load data from the db
    else {
      $form->load_data( $data );
    }

    // render the form and return the full html output
    $html .= $form->render();
    return $html;
  }

  private function complete_submission () {
    global $wpdb;
    $table = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $where = [ "id" => $this->participant["id"] ];
    $update_data = ["status" => 12];
    return $wpdb->update( $table, $update_data, $where );
  }

  private function store ( $section, $data ) {
    global $wpdb;

    // empty array of column names and values to be updated
    $update_data = [];

    // those fields can be directly taken from the validated
    // form data, if they are not empty
    $take_if_not_empty = [];
    foreach ( $section["fields"] as $field ) {
      if ( $field["type"] == "section" ) {
        foreach ( $field["fields"] as $f ) {
          // only named fields should be stored
          if ( isset( $f["name"] ) ) {
            $take_if_not_empty[] = $f["name"];
          }
        }
      } else {
        // only named fields should be stored
        if ( isset( $field["name"] ) ) {
          $take_if_not_empty[] = $field["name"];
        }
      }
    }
    foreach ( $take_if_not_empty as $key ) {
      if ( isset( $data[$key] ) && $data[$key] !== '' ) {
        $update_data[$key] = $data[$key];
      }
    }

    // if all fields are empty, nothing gets updated
    if ( empty( $update_data ) ) {
      return 0;
    }

    // in case we are handling an emergency contact entry
    $contact = false;
    if ( isset($_POST["notfallkontakte_local"]) ) $contact = 'local';
    if ( isset($_POST["notfallkontakte_remote"]) ) $contact = 'remote';
    if ( $contact ) {
      $table = $wpdb->prefix . GSD_TABLE_NOTFALLKONTAKT;
      $where = [
        "teilnehmerin_id" => $this->participant["id"],
        "ist_im_einsatzland" => $contact == 'local' ? 0 : 1,
      ];
      // if there is only the one hidden field set, we'll treat it as unchanged
      if ( count($update_data) == 1 ) {
        return 0;
      }
      // now we have to chek if there already is an entry to update,
      // otherwise we have to insert
      if ( $contact == 'local' ) {
        if ( empty( $this->contacts_local ) ) {
          $update_data["teilnehmerin_id"] = $this->participant["id"];
          return $wpdb->insert( $table, $update_data );
        } else {
          return $wpdb->update( $table, $update_data, $where );
        }
      } else {
        if ( empty( $this->contacts_remote ) ) {
          $update_data["teilnehmerin_id"] = $this->participant["id"];
          return $wpdb->insert( $table, $update_data );
        } else {
          return $wpdb->update( $table, $update_data, $where );
        }
      }
    }

    // in case we are handling file uploads
    else if ( isset($_POST["files"]) ) {
      $update_count = 0;
      foreach ( $update_data as $key => $file ) {
        // create the target filename and move the uploaded file
        $target_base = GSD_UPLOAD_DIR . '/';
        $target_file = $this->participant["matrikelnr"] . '_' .
          $key . '_' . date('Y-m-d_H-i-s') . '_' . $file["name"];
        move_uploaded_file( $file["tmp_name"], $target_base.$target_file );

        // check if there is already an existing entry
        $updating = false;
        foreach ( $this->files as $f ) {
          if ( $f["type"] == $key ) {
            $updating = true;
          }
        }

        // now inser/update the file entry in the databse
        $table = $wpdb->prefix . GSD_TABLE_FILE;
        $where = [
          "teilnehmerin_id" => $this->participant["id"],
          "type" => $key,
        ];
        $values = [
          "teilnehmerin_id" => $this->participant["id"],
          "type" => $key,
          "filepath" => $target_file,
          "updated" => date( "c" ),
        ];
        if ( $updating ) {
          $num = $wpdb->update( $table, $values, $where );
        } else {
          $num = $wpdb->insert( $table, $values );
        }
        if ( $num === false ) {
          return false;
        }
        $update_count += $num;
      }
      return $update_count;
    }

    // changing plain participant data is much more easy, because we know that
    // there is already an entry
    else {
      // we only have to check einsatzorga_id has if it is "OTHER"
      if ( isset( $data["einsatzorga_id"] ) && $data["einsatzorga_id"] == "OTHER" ) {
        $update_data["einsatzorga_id"] = null;
        // TODO: this is hardcoded now due to time availability; should be automated in improved version
        if ( ! empty( $data["einsatzorga_other_name"] ) ) $update_data["einsatzorga_other_name"] = $data["einsatzorga_other_name"];
        if ( ! empty( $data["einsatzorga_other_website"] ) ) $update_data["einsatzorga_other_website"] = $data["einsatzorga_other_website"];
      }
      // and for language courses we also have to pull in the alternative data
      if ( isset( $data["sprachkurs"] ) ) {
        $update_data["sprachkurs"] = $data["sprachkurs"];
        // TODO: this is hardcoded now due to time availability; should be automated in improved version
        if ( ! empty( $data["sprachkurs_institut"] ) ) $update_data["sprachkurs_institut"] = $data["sprachkurs_institut"];
        if ( ! empty( $data["sprachkurs_institut_website"] ) ) $update_data["sprachkurs_institut_website"] = $data["sprachkurs_institut_website"];
        if ( ! empty( $data["sprachkurs_beginn"] ) ) $update_data["sprachkurs_beginn"] = $data["sprachkurs_beginn"];
        if ( ! empty( $data["sprachkurs_ende"] ) ) $update_data["sprachkurs_ende"] = $data["sprachkurs_ende"];
      }
      // now we can update the row and return the number of rows (should be 1 or
      // 0 in case nothing changed)
      $table = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
      $where = [ "id" => $this->participant["id"] ];
      $update_data["updated"] = date( "c" );
      return $wpdb->update($table, $update_data, $where );
    }
  }

  private function render_success () {
    $html = Notification::success([
      "message" => "Daten aktualisiert."
    ]);
    $query_string = $_SERVER["QUERY_STRING"];
    // remove the edit parameter from the query string
    // in case it is not the first parameter:
    $query_string = preg_replace( '/\\&edit=[a-z_\-]+/', '', $query_string );
    // in case it is the first parameter
    $query_string = preg_replace( '/^edit=[a-z_\-]+[\\&]?/', '', $query_string );
    // in case it is the only parameter
    $query_string = preg_replace( '/^edit=person$/', '', $query_string );
    $html .= '<a href="?'.$query_string.'">Zurück zur Übersicht</a>';
    return $html;
  }

  private function render_submission_success () {
    // create mail notification for admins
    $name = "{$this->participant["firstname"]} {$this->participant["lastname"]}";
    $matrikelnr = "{$this->participant["matrikelnr"]}";
    $admin_url = get_admin_url();
    $msg = "
Hello schnello again,

**$name** (Matrikelnummer **$matrikelnr**) hat gerede deren
Fixanmeldungs-Formular eingereicht. Um die Daten zu prüfen und die Person
zu den fix angemeldeten zu übernehmen, gehe zur GSD-Datenbank-Übersicht:

  $admin_url

liebste grüße,
automagia
    ";
    $admins = get_users([ "role" => "Administrator" ]);
    foreach ( $admins as $admin ) {
      wp_mail($admin->data->user_email, "GSD-Fixanmeldungs-Formular eingereicht", $msg);
    }

    // render notification for website user
    $html = Notification::success([
      "message" => "Daten erfolgreich eingereicht."
    ]) . '
      <p>
        Vielen Dank für das Einreichen des Formulars. Wir werden die Daten
        ehestmöglich prüfen und deine Fixanmeldung bestätigen oder dich
        gegebenenfalls kontaktieren. Du kannst im Formular weiterhin Daten
        speichern, sollte sich etwas ändern.
      </p>
      <p align="center">
        <a href="?">Zurück zum Formular</a>
      </p>
    ';
    return $html;
  }

  private function render_comments( $data ) {
    $html = '';
    if ( ! empty( $data["comment_for_participant"] ) ) {
      $html .= '
        <div class="data-section">
          <h3 class="section-title">Anmerkungen der Projektleitung</h3>
          <div class="fixed">
            <div class="label">
              Die Projektleitung hat folgende Notiz für dich gespeichert:
            </div>
            <div class="value">
              ' . nl2br( esc_html( $data["comment_for_participant"] ) ) . '
            </div>
          </div>
        </div>
      ';
    }
    return $html;
  }

  private function render_intro () {
    $html = '
    <div class="data-section">
      <h3 class="section-title">Einleitung</h3>
      <div class="fixed">
        <div class="label">
          Bitte beachte folgende Punkte beim Ausfüllen der folgenden Formulare:
        </div>
        <div class="value">
          <ul>
            <li>Grundsätzlich müssen alle Felder ausgefüllt werden – auch jene, die nicht mit Sternchen markiert sind.</li>
            <li>Felder mit Sternchen müssen zum Zeitpunkt der Fixanmeldung auf jeden Fall ausgefüllt sein,
              damit diese eingereicht werden kann. Falls eine solche Pflicht-Info zu diesem Zeitpunkt noch
              nicht erbracht werden kann, bitte eine Begründung in das Feld schreiben.</li>
            <li>Kann bei einem erfragten Datum noch kein genaues Datum genannt werden, bitte den 1.1. des voraussichtlichen Monats angeben.</li>
            <li>Felder ohne Sternchen können, wenn nötig, nachgereicht werden – diese dann bitte vorerst leer lassen.</li>
            <li>Spätestens bis zum Termin des Vorbereitungsseminars müssen alle Infos vollständig vorhanden oder ihr Fehlen gut begründet sein.</li>
            <li>Das Datenblatt muss als Ausdruck zum Vorbereitungsseminar mitgebracht und im Rahmen des Seminares unterschrieben werden.</li>
            <li>Um die Fixanmeldung abzuschließen musst du am Ende dieser Übersichtsseite auf
              <b style="color: darkturquoise;">"Datenblatt speichern und einreichen"</b>
              klicken (der Button ist verfügbar, sobald du die Pflichtfelder
              in allen Formularabschnitten ausgefüllt hast).</li>
            <li>Das Datenblatt kann nach dem Abspeichern und Einreichen jederzeit überarbeitet werden.</li>
          </ul>
        </div>
      </div>
    </div>
    ';
    return $html;
  }

  public function render_section ( $config, $data ) {
    $html = '
    <div class="data-section">
      <h3 class="section-title">' . esc_html($config["title"]) . '</h3>';

    // first we'll display a section with non-editable fields (if any)
    if( ! empty( $config["fixed_fields"] ) ) {
      $html .= $this->render_section_fields( $config["slug"], false, $config["fixed_fields"], $data );
    }

    // then a section with editable fields (if any) follows
    if( ! empty( $config["fields"] ) ) {
      $html .= $this->render_section_fields( $config["slug"], true, $config["fields"], $data );
    }

    // close the data section div and return the generated html
    $html .= '
    </div> <!-- data-section -->
    ';
    return $html;
  }

  private function render_section_fields ( $slug, $editable, $fields, $data, $recurse=false ) {
    $html = '';

    // the opening tags (and optional edit link) should only be added, if we
    // are not inside a subsection recursion
    if ( ! $recurse ) {
      $css_class = $editable ? 'editable' : 'fixed';
      if ( empty( $_SERVER["QUERY_STRING"] ) ) {
        $edit_link = "edit=" . $slug;
      } else {
        $edit_link = $_SERVER["QUERY_STRING"] . "&edit=" . $slug;
      }
      $html .= '
      <div class="' . $css_class . '">
      ';
      if ( $editable ) {
        $html .= '<a class="button" href="?'.$edit_link.'"><span class="dashicons dashicons-edit"></span></a>';
      }
    }

    // now process the fields
    foreach ( $fields as $field ) {
      // if we hit a (sub)section, we'll add a heading and recurse
      if ( $field["type"] == "section" ) {
        $html .= '<h4 class="section-title">' . $field["header"] . '</h4>
        ' . $this->render_section_fields( $slug, $editable, $field["fields"], $data, true );
      }

      // this is the standard output, if the type is not a section
      else {
        $html .= $this->render_field( $field, $data );
      }
    }

    // again, closing div only if not in recursion
    if ( ! $recurse ) {
      $html .= '
      </div> <!-- ' . $css_class . ' -->';
    }

    return $html;
  }

  private function render_field ( $field, $data ) {
    if ( empty( $field["options_extended"] ) ) {
      $options = empty( $field["options"] ) ? false : $field["options"];
    } else {
      $options = $field["options_extended"];
    }
    return Form::render_field( $field, $data, $options );
  }

}
