<?php
namespace GSDDB;

class Form
{
  private $title;
  private $slug;
  private $method;
  private $action;
  private $enctype;
  private $prefill = [];   // for already submitted (& sanitized) form data
  private $prefill_temp;   // only used temporarily when loading submitted data
  private $elements = [];  // internal html representation of the form
  private $fields = [];    // fields as configured at object instantiation
  private $fixed_fields = []; // non-editable fields
  private $fixed_data = [];   // data for fixed fields
  private $submit_label = []; // the text to be used for the submit button
  public $errors = [];     // error entries (if any) after valdation

  public function __construct( $config )
  {
    $this->title  = $config["title"];
    $this->slug = $config["slug"];
    $this->method = empty( $config["method"] ) ? 'POST' : $config["method"];
    $this->action = empty( $config["action"] ) ? '' : $config["action"];
    $this->enctype = empty( $config["enctype"] ) ? '' : $config["enctype"];
    $this->fields = $config["fields"];
    $this->fixed_fields = empty( $config["fixed_fields"] ) ? '' : $config["fixed_fields"];
    $this->submit_label = empty( $config["submit_label"] ) ? 'Absenden' : $config["submit_label"];
  }

  public function get_submitted_data () {
    return $this->prefill;
  }

  public function validate_nonce ( $data ) {
    // this could also go into load_data(), but we want to validate the nonce
    // before we do anything else with the data
    if ( ! isset( $data[ $this->slug ] ) ) {
      return false;
    }
    if ( ! wp_verify_nonce( $data[ $this->slug . '_nonce' ], $this->method ) ) {
      return false;
    }
    return true;
  }

  public function load_fixed_data ( $data ) {
    $this->fixed_data = $data;
  }

  public function load_data ( $data ) {
    // load the temporary prefill data array
    $this->prefill_temp = $data;

    // run through all form fields to check for and sanitize/validate prefilled data
    foreach ( $this->fields as $field ) {
      // a section field has to be run through for all containing fields
      if ( $field["type"] == "section" ) {
        foreach ( $field["fields"] as $section_field ) {
          $this->sanitize_and_validate( $section_field );
          $this->check_missing( $section_field );
        }
      }
      // otherwise we can directly use the field
      else {
        $this->sanitize_and_validate( $field );
        $this->check_missing( $field );
      }
    }

    // now $this->prefill should be filled with all sanitized and valid data
    // so we clean up prefill_temp
    $this->prefill_temp = null;
  }

  private function sanitize_and_validate ( $field ) {
    // only named fields are checked (i.e. those that are actual input fields)
    if ( ! isset( $field["name"] ) ) {
      return;
    }
    // first sanitize, then validate
    if ( ! empty( $field["sanitize"] ) ) {
      $this->sanitize( $field );
    }
    // check if the field has a maxlength and if the value is too long
    if ( isset($field["length"]) ) {
      $name = $field["name"];
      $value = isset( $this->prefill_temp[ $name ] ) ? $this->prefill_temp[ $name ] : null;
      if ( isset($value) && strlen($value) > $field["length"] ) {
        $this->add_error( "length", $field );
      }
    }
    if ( ! empty( $field["validate"] ) ) {
      $this->validate( $field );
    }
    // if neither sanitization nor validation is configured, we
    // will take only a generically sanitized value
    if ( empty( $field["sanitize"] ) && empty( $field["validate"] ) ) {
      $this->sanitize_generic( $field );
    }
  }

  private function sanitize ( $field ) {
    $name = $field["name"];

    // sanitizing only makes sense for non-empty fields
    if ( isset( $this->prefill_temp[ $name ] ) ) {

      // this seems a bit tedious, as we just could use a quick
      // function_exists ( "sanitize_".$field["sanitize"] )
      // with a follow up eval() call.
      // BUT: better be defensive than insecure ;)
      switch ( $field["sanitize"] ) {
        case "email":
          $this->prefill[ $name ] = sanitize_email( $this->prefill_temp[ $name ] );
          break;

        case "file_name":
          $this->prefill[ $name ] = sanitize_file_name( $this->prefill_temp[ $name ] );
          break;

        case "text_field":
          // single/double quotes and backslashes should not be url encoded
          // before we go into the WP sanitization functions
          $this->prefill[ $name ] = preg_replace('/\\\\(["\'\\\\])/', '$1', $this->prefill_temp[ $name ]);
          $this->prefill[ $name ] = sanitize_text_field( $this->prefill[ $name ] );
          break;

        case "textarea_field":
          // single/double quotes and backslashes should not be url encoded
          // before we go into the WP sanitization functions
          $this->prefill[ $name ] = preg_replace('/\\\\(["\'\\\\])/', '$1', $this->prefill_temp[ $name ]);
          $this->prefill[ $name ] = sanitize_textarea_field( $this->prefill[ $name ] );
          break;

        case "title":
          $this->prefill[ $name ] = sanitize_title( $this->prefill_temp[ $name ] );
          break;

        case "user":
          $this->prefill[ $name ] = sanitize_user( $this->prefill_temp[ $name ] );
          break;

        case "url":
          // sanitize_url() is deprecated: use esc_url_raw instead
          $this->prefill[ $name ] = esc_url_raw( $this->prefill_temp[ $name ] );
          break;

        default:
        // also here, if the sanitization function is not available, apply the generic one
          $this->sanitize_generic( $field );
      }
    }
  }

  private function sanitize_generic ( $field ) {
    $name = $field["name"];
    if ( isset( $this->prefill_temp[ $name ] ) ) {
      $this->prefill[ $name ] = esc_html( $this->prefill_temp[ $name ] );
    }
  }

  private function validate ( $field ) {
    $name = $field["name"];
    // we only validate fields that are set and not ''. mandatory fields will be
    // checked by the check_missing() function
    // but einsatzorgas are special, because a NULL in the db becomes a "" and
    // should be validated, because it is translated to "OTHER"
    if (
      isset( $this->prefill_temp[ $name ] )
      && $this->prefill_temp[ $name ] !== ''
      || $field["type"] == "einsatzorga"
      || $field["type"] == "language_course"
      || $field["type"] == "checkbox"
    ) {
      $value = isset( $this->prefill_temp[ $name ] ) ? $this->prefill_temp[ $name ] : null;
      // if this field was not sanitized before, we'll set it here before
      // validating. otherwise it will be missing, if not validated. but the
      // expected behaviour is that it stay in the prefill but with a corresponding
      // validation error
      if ( empty( $this->prefill[ $name ] ) ) {
        $this->prefill[ $name ] = $value;
      }
      // now validate, depending on which validation function was chosen
      switch ( $field["validate"] ) {
        case "regex":
          if ( ! preg_match( $field["regex"], $value ) ) {
            $this->add_error( "validation", $field );
          }
          break;

        case "options":
          if ( $field["type"] != "einsatzorga" ) {
            // TODO: this is a bit tricky. array_keys returns an array only with
            // integers, if all the keys are numbers. but it returns the keys as
            // stringsm if at least one of the keys is not a number. so there will
            // always be cases where strict checking fails, without further data
            // preparation. and if we do not apply strict checking, when a key of
            // eg "1a" is compared to a 1, the comparisn is true.
            // we accept that for now, but it would be nice to implement strict checking
            if ( ! in_array( $value, array_keys( $field["options"] ) ) ) {
              $this->add_error( "validation", $field );
            }
          } else {
            // "null" is  valid, because it translates to OTHER in the select option
            if ( $value !== null && ! in_array( $value, array_keys( $field["options"] ) ) ) {
              $this->add_error( "validation", $field );
            }
            if ( $value === null ) {
              $this->prefill[ $name ] = "OTHER";
            }
            foreach( $field["alternative_fields"] as $f_name => $f ) {
              $this->prefill[$f_name] = $this->prefill_temp[$f_name];
            }
          }
          break;

        case "checkbox":
          // did you know that ( "true" === 0 ) evaluates to 1?
          // i didn't! i do now. and i do now hate PHP even more!
          if ( $value === null || $value === 0 || $value === "0" ) {
            $this->prefill[ $name ] = 0;
          } else if ( $value === "true" || $value === "1" ) {
            $this->prefill[ $name ] = 1;
          } else {
            $this->add_error( "validation", $field );
          }
          if ( $field["type"] == "language_course" ) {
            foreach( $field["fields_if_yes"] as $f_name => $f ) {
              // TODO: we should check if type is date and do proper validation
              $this->prefill[$f_name] = $this->prefill_temp[$f_name];
            }
          }
          break;

        default:
          // if no validation is available, we still have to check if data was
          // sanitized, otherwise we'll have to sanitize it here
          if ( empty( $field["sanitize"] ) ) {
            $this->sanitize_generic( $field );
          }
          // also we'll print an error to the log, because atm. this only means
          // a programmer issue when writing the form config array
          error_log('[GSD error]: Validation function not available: ' . $field["validate"] .
            ' | form title: ' . $this->title . ' | form slug: ' . $this->slug .
            ' | form field: ' . json_encode($field)
          );
      }
    }

    // we'll handle files separately, as their logic is a bit different
    // files don't need a "validate" key in the field config, we'll always
    // validate them
    else if ( $field["type"] == "file" ) {
      if ( isset( $_FILES[$name] ) ) {
        // if there was no file, this is ok, we'll just return without validation error
        if ( $_FILES[$name]["error"] === UPLOAD_ERR_NO_FILE ) {
          return;
        }
        // in all other error cases, we'll add a validation error before returning
        if ( $_FILES[$name]["error"] !== UPLOAD_ERR_OK ) {
          $this->add_error( "validation", $field );
          return;
        }
        // now check for the file name extension
        $fname = $_FILES[$name]["name"];
        $ext = explode('.', $fname);
        $ext = strtolower($ext[count($ext)-1]);
        if ( ! in_array( $ext, $field["allowed"] ) ) {
          $this->add_error( "validation", $field );
          return;
        }
        // TODO: also add file size as a validation parameter
        // now we can set the file data for later processing
        $this->prefill[$name] = $_FILES[$name];
      }
      // if no files have been uploaded, we have to check if there already is
      // already an existing upload
      else {
        foreach ( $this->prefill_temp as $f ) {
          if ( $f["type"] == $name ) {
            // we store it under the "current" key, so it does not interfere with new uploads
            $this->prefill[$name] = [
              "current" => $f,
            ];
          }
        }
      }
    }
  }

  private function check_missing ( $field ) {
    if ( isset( $field["mandatory"] ) && $field["mandatory"] ) {
      if (
        ! isset($this->prefill[$field["name"]]) ||
        ( empty( $this->prefill[ $field["name"] ] ) && $this->prefill[ $field["name"] ] !== "0" )
      ) {
        $this->add_error( "missing", $field );
      }
    }
  }

  private function add_error ( $type, $field ) {
    if( empty( $this->errors[ $type ] ) ) {
      $this->errors[ $type ] = array();
    }
    $this->errors[ $type ][ $field["name"] ] = $field["label"];
  }

  public function render () {
    $enctype = empty( $this->enctype ) ? '' : ' enctype="'.$this->enctype.'"';
    $html = '
      <div class="gsd-form">
        <form method="'.esc_attr($this->method).'" action="'.esc_attr($this->action).'"'.$enctype.'>
        ';

    if ( $this->title ) {
      $html .= "
          <h1>" . esc_html($this->title) . "</h1>";
    }
    if ( $this->has_mandatory_field() ) {
      $html .= '
          <p class="mandatory-field-note">
            Pflichtfelder sind mit einem <span class="mandatory-field">*</span> gekennzeichnet.
          </p>
          <div class="data-section">';
    }

    // render non-editable fields
    if( ! empty( $this->fixed_fields ) ) {
      $html .= '
            <div class="fixed">';
      foreach ( $this->fixed_fields as $field ) {
        $options = empty( $field["options"] ) ? false : $field["options"];
        $html .= Form::render_field( $field, $this->fixed_data, $options );
      }
      $html .= '
            </div> <!--fixed -->';
    }

    // prepare all form input fields
    foreach ( $this->fields as $field ) {
      switch ( $field["type"] ) {
        case "section":
          $this->add_section( $field["header"], $field["fields"] );
          break;

        default:
          $this->add_field( $field );
      }
    }

    // now add all field's html representations to the output in the edit section
    $html .= '
            <div class="editable">';
    foreach ( $this->elements as $element ) {
      $html .= $element . "\n";
    }

    // and add a nonce field, the submit button and close the form
    $html .= '
              <br><input type="submit" name="' . esc_attr($this->slug) . '" value="' . esc_attr($this->submit_label) . '">
            </div> <!-- editable -->
          </div> <!-- data-section -->
          ' . wp_nonce_field($this->method, $this->slug. '_nonce', true, false) . '
        </form>
      </div> <!-- gsd-form -->';
    return $html;
  }

  private function has_mandatory_field () {
    foreach ( $this->fields as $field ) {
      if ( $field["type"] == "section" ){
        foreach ( $field as $f ) {
          if ( isset( $field["mandatory"] ) && $field["mandatory"] === true ) {
            return true;
          }
        }
      } else {
        if ( isset( $field["mandatory"] ) && $field["mandatory"] === true ) {
          return true;
        }
      }
    }
    return false;
  }

  private function has_error ( $name ) {
    if ( $this->errors ) {
      if (
        isset( $this->errors["missing"] ) &&
        in_array( $name, array_keys( $this->errors["missing"] ) )
      ) {
        return true;
      }
      if (
        isset( $this->errors["validation"] ) &&
        in_array( $name, array_keys( $this->errors["validation"] ) )
      ) {
        return true;
      }
    }
    return false;
  }

  private function add_section ( $header, $fields ) {
    $this->elements[] = '<div class="form-section">'."\n";
    $this->elements[] = '<h3>' . esc_html($header) . '</h3>'."\n";
    foreach ( $fields as $field ) {
      $this->add_field( $field );
    }
    $this->elements[] = '</div> <!-- form-section -->'."\n";
  }

  private function add_field ( $field ) {
    $pattern = false;
    if ( ! empty( $field["regex"] ) ) {
      $pattern = $field["regex"];
    }
    $mandatory = false;
    if ( ! empty( $field["mandatory"] ) ) {
      $mandatory = true;
    }
    $maxlen = false;
    if ( ! empty( $field["length"] ) ) {
      $maxlen = $field["length"];
    }
    switch ( $field["type"] ) {
      case "number":
      case "email":
      case "tel":
      case "text":
        $this->add_input_text( $field["name"], $field["label"], $mandatory,
          $field["type"] , $pattern, $maxlen );
        break;

      case "textarea":
        $this->add_textarea( $field );
        break;

      case "date":
        $this->add_input_date( $field["name"], $field["label"], $mandatory );
        break;

      case "paragraph":
        $this->add_text_paragraph( $field["content"] );
        break;

      case "heading":
        $this->add_heading( $field["label"] );
        break;

      case "dropdown":
        $this->add_dropdown( $field );
        break;

      case "radio":
        $this->add_radio_buttons( $field["name"], $field["label"], $mandatory, $field["options"] );
        break;

      case "checkbox":
        $this->add_checkbox( $field["name"], $field["label"], $mandatory );
        break;

      case "einsatzorga":
        $this->add_einsatzorga( $field );
        break;

      case "hidden":
        $this->add_hidden ( $field );
        break;

      case "file":
        $this->add_file ( $field );
        break;

      case "language_course":
        $this->add_language_course ( $field );
        break;
    }
  }

  public function add_text_paragraph ($text) {
    $text = Form::unescape_simple_tags( esc_html($text) );
    $this->elements[] = "<p>" . $text . "</p>\n";
  }

  public function add_heading ($heading) {
    $this->elements[] = "<h2>" . esc_html($heading) . "</h2>\n";
  }

  public function add_input_text ($name, $label, $mandatory=false, $type="text", $pattern=false, $maxlen=false ) {
    $value = "";
    if ( isset( $this->prefill[$name] ) ) {
      $value = $this->prefill[$name];
    }

    $error_class = "";
    if ( $this->has_error( $name ) ) {
      $error_class = " field-error";
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';
    $length = $maxlen ? ' maxlength="'.$maxlen.'"' : '';
    // if a regex pattern is set the pattern argument in the input element
    // has to be without leading and closing slashes
    $pattern = $pattern ? ' pattern="'.substr($pattern, 1, -1).'"' : '';

    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <label for="' . esc_attr($name) . '">' . esc_html($label) . ':</label><br>
      <input type="'. esc_attr($type) .'" name="' . esc_attr($name) . '" id="' .
        esc_attr($name) . '" value="' . esc_attr($value) . '"' . $pattern . $length . '>
    </div>';
    $this->elements[] = $html;
  }

  public function add_hidden ( $field ) {
    $name = $field["name"];
    $value = $field["value"];
    $html = '
      <input type="hidden" name="'.esc_attr($name).'" value="'.esc_attr($value).'">';
    $this->elements[] = $html;
  }

  public function add_file ( $field ) {
    $name = $field["name"];
    $label = $field["label"];
    $allowed = $field["allowed"];
    $mandatory = empty( $field["mandatory"] ) ? false : true;
    $explanation = empty( $field["explanation"] ) ? false : $field["explanation"];

    $error_class = "";
    if ( $this->has_error( $name ) ) {
      $error_class = " field-error";
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    $accept = '';
    foreach ( $allowed as $ext ) {
      $accept .= ".$ext, ";
    }
    // clear a trailing ", "
    $accept = substr( $accept, 0, strlen($accept) - 2);

    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <label for="' . esc_attr($name) . '">' . esc_html($label) . ' (' . esc_attr($accept) . '):</label><br>';
    if ( $explanation ) {
      $html .= '<div class="explanation">' . esc_html($explanation) . '</div>';
    }
    if ( isset( $this->prefill[$name] ) && isset( $this->prefill[$name]["current"] ) ) {
      $fname = $this->prefill[$name]["current"]["filepath"];
      $html .= 'Bereits hochgeladen:<br>
        <a href="' . esc_url( GSD_UPLOAD_URL.'/'.$fname ) . '">' .
        esc_url('/'.$fname) . '</a><br>
        Bei Bedarf aktuellere Version auswählen:';
    }
    $html .= '
      <input type="file" id="' . esc_attr($name) . '" name="' . esc_attr($name) .
        '" accept="' . esc_attr($accept) . '">
    </div>';
    $this->elements[] = $html;
  }

  public function add_textarea ( $field ) {
    $name = $field["name"];
    $label = $field["label"];
    $mandatory = empty( $field["mandatory"] ) ? false : true;
    $explanation = empty( $field["explanation"] ) ? false : $field["explanation"];

    $value = "";
    if ( isset( $this->prefill[$name] ) ) {
      $value = $this->prefill[$name];
    }

    $error_class = "";
    if ( $this->has_error( $name ) ) {
      $error_class = " field-error";
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <label for="' . esc_attr($name) . '">' . esc_html($label) . ':</label><br>';
    if ( $explanation ) {
      $html .= '<div class="explanation">' . esc_html($explanation) . '</div>';
    }
    $html .= '
      <textarea id="' . esc_attr($name) . '" name="' . esc_attr($name) .
        '" rows="5" cols="40">' . esc_attr($value) . '</textarea>
    </div>';
    $this->elements[] = $html;
  }

  public function add_input_date ($name, $label, $mandatory=false) {
    $value = "";
    if ( isset( $this->prefill[$name] ) ) {
      $value = $this->prefill[$name];
    }

    $error_class = "";
    if ( $this->has_error( $name ) ) {
      $error_class = " field-error";
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    // TODO: add date input field support for older browsers?
    // i.e. detect unsupported date input and add note on format: YYYY-MM-DD?
    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <label for="' . esc_attr($name) . '">' . esc_html($label) . ':</label><br>
      <input type="date" name="' . esc_attr($name) . '" id="' . esc_attr($name) .
        '" value="' . esc_attr($value) . '">
    </div>';
    $this->elements[] = $html;
  }

  public function add_dropdown ( $field ) {
    $name = $field["name"];
    $label = $field["label"];
    $options = $field["options"];
    $mandatory = empty( $field["mandatory"] ) ? false : true;
    $explanation = empty( $field["explanation"] ) ? false : $field["explanation"];

    $error_class = "";
    if ( $this->has_error( $name ) ) {
      $error_class = " field-error";
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <label for="' . esc_attr($name) . '">' . esc_html($label) . ':</label>
      <select name="' . esc_attr($name) . '" id="' . esc_attr($name) . '">
      ';
    foreach ( $options as $key => $text ) {
      $html .= '<option value="' . esc_attr($key) . '"';
      if ( isset ( $this->prefill[$name] ) ) {
        if ( $this->prefill[$name] == $key ) {
          $html .= ' selected="selected"';
        }
      }
      $html .= '>' . esc_html($text) . "</option>\n";
    }
    $html .= '      </select>';
    if ( $explanation ) {
      $html .= '
      <div class="explanation">' . esc_html($explanation) . '</div>';
    }
    $html .= '
    </div>';
    $this->elements[] = $html;
  }

  public function add_radio_buttons ($name, $label, $mandatory=false, $options=[]) {
    $error_class = "";
    if ( $this->has_error( $name ) ) {
      $error_class = " field-error";
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    $html = '
    <div class="gsd-form-field' . $error_class . '">
      ' . $asterisk . esc_attr($label) . ':<br>';

    foreach ( $options as $key => $text ) {
      $checked = '';
      if ( isset( $this->prefill[$name] ) ) {
        if ( $this->prefill[$name] == $key ) {
          $checked = ' checked';
        }
      }
      $html .= '
      <input type="radio" name="' . esc_attr($name) . '" ' .
        'id="' . esc_attr($name.$key) . '" ' .
        'value="' . esc_attr($key) . '"' . $checked . '>
      <label for="' . esc_attr($name.$key) . '">' . esc_html($text) . '</label><br>';
    }

    $html .= '
    </div>';
    $this->elements[] = $html;
  }

  public function add_checkbox ($name, $label, $mandatory=false) {
    $html = '';
    $value = "";

    if ( isset( $this->prefill[$name] ) ) {
      $value = $this->prefill[$name];
    }

    $checked = '';
    if ( $value === 1 ) {
      $checked = ' checked';
    }

    $error_class = '';
    if ( $this->has_error( $name ) ) {
      $error_class = ' field-error';
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    $label = Form::unescape_simple_tags( esc_html($label) );
    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <table><tr>
        <td><input type="checkbox" name="' . esc_attr($name) . '" ' .
          'id="' . esc_attr($name) . '" value="true"' . $checked . '></td>
        <td><label for="' . esc_attr($name) . '">' . $label . '</label></td>
      </tr></table>
    </div>';
    $this->elements[] = $html;
  }

  public function add_einsatzorga ( $field ) {
    $name = $field["name"];
    $label = $field["label"];
    $options = $field["options"];
    $alternative_fields = $field["alternative_fields"];
    $mandatory = empty( $field["mandatory"] ) ? false : true;

    $error_class = "";
    if ( $this->has_error( $name ) ) {
      $error_class = " field-error";
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <label for="' . esc_attr($name) . '">' . esc_html($label) . ':</label>
      <select name="' . esc_attr($name) . '" id="' . esc_attr($name) . '">
      ';
    foreach ( $options as $key => $text ) {
      $html .= '<option value="' . esc_attr($key) . '"';
      if ( isset ( $this->prefill[$name] ) ) {
        if ( $this->prefill[$name] == $key ) {
          $html .= ' selected="selected"';
        }
      } else if ( $key == "OTHER" ) {
        $html .= ' selected="selected"';
      }
      $html .= '>' . esc_html($text) . "</option>\n";
    }
    $html .= '
      </select>
      <div class="dropdown-alternatives">';
    foreach ( $alternative_fields as $f_name => $f) {
      $val = empty( $this->prefill[$f_name] ) ? '' : $this->prefill[$f_name];
      $type = isset( $f["type"] ) ? $f["type"] : 'text';
      $html .= '
        <label for="' . esc_attr($f_name) . '">' . esc_html($f["label"]) . ':</label><br>
        <input type="' . esc_attr($type) . '" id="' . esc_attr($f_name) .
          '" name="' . esc_attr($f_name) . '" value="' . esc_attr($val) . '"><br>';
    }
    $html .= '
      </div>
    </div>';
    $this->elements[] = $html;
  }

  public function add_language_course ( $field ) {
    $name = $field["name"];
    $label = $field["label"];
    $fields_if_yes = $field["fields_if_yes"];
    $mandatory = empty( $field["mandatory"] ) ? false : true;

    $html = '';
    $value = '';
    if ( isset( $this->prefill[$name] ) ) {
      $value = $this->prefill[$name];
    }

    $checked = '';
    if ( $value === "true" || $value == 1 ) {
      $checked = ' checked';
    }

    $error_class = '';
    if ( $this->has_error( $name ) ) {
      $error_class = ' field-error';
    }

    $asterisk = $mandatory ? '<span class="mandatory-field">*</span> ' : '';

    $html = '
    <div class="gsd-form-field' . $error_class . '">' . $asterisk . '
      <table><tr>
        <td><input type="checkbox" name="' . esc_attr($name) . '" ' .
          'id="' . esc_attr($name) . '" value="true"' . $checked . '></td>
        <td><label for="' . esc_attr($name) . '">' . esc_html($label) . '</label></td>
      </tr></table>';
    $html .= '
      </select>
      <div class="dropdown-alternatives">
        Nur auszufüllen wenn <i>' . esc_html($label) . '</i> angehakt:<br>';
    foreach ( $fields_if_yes as $f_name => $f) {
      $val = empty( $this->prefill[$f_name] ) ? '' : $this->prefill[$f_name];
      $type = isset( $f["type"] ) ? $f["type"] : 'text';
      $html .= '
        <label for="' . esc_attr($f_name) . '">' . esc_html($f["label"]) . ':</label><br>
        <input type="' . esc_attr($type) . '" id="' . esc_attr($f_name) .
          '" name="' . esc_attr($f_name) . '" value="' . esc_attr($val) . '"><br>';
    }
    $html .= '
    </div>';

    $this->elements[] = $html;
  }


  public static function render_field ( $config, $data, $options=false ) {
    $html = '';

    $type = $config["type"];

    switch( $config["type"] ) {
      case "einsatzorga":
        if ( $options ) {
          $html .= Form::render_einsatzorga( $config, $data, $options );
        } else {
          $html .= Form::render_standard_field( $config, $data );
        }
        break;

      case "school":
        if ( $options ) {
          $html .= Form::render_school( $config, $data, $options );
        } else {
          $html .= Form::render_standard_field( $config, $data );
        }
        break;

      case "language_course":
        $html .= Form::render_language_course( $config, $data );
        break;

      case "hidden":
        // hidden fields don't get rendered
        break;

      case "paragraph":
        $content = Form::unescape_simple_tags ( esc_html( $config["content"] ) );
        $html .= "<p>" . $content . "</p>\n";
        break;

      case "heading":
        $html .= "<h4>" . esc_html( $config["label"] ) . "</h4>\n";
        break;

      case "file":
        $html .= Form::render_file ( $config, $data );
        break;

      case "checkbox":
        $html .= Form::render_checkbox( $config, $data );
        break;

      default:
        $html .= Form::render_standard_field( $config, $data );
        break;
    }
    return $html;
  }

  public static function render_standard_field ( $config, $data ) {
    $field = $config["name"];
    $content_escaped = empty( $data[$field] ) ?
      '<span class="missing">(fehlt noch)</span>' :
      esc_html( $data[$field] );
    $html = '
    <div class="field '.esc_attr( $config["type"] ).'-field">
      <div class="label">' . esc_html( $config["label"] ) . ':</div>
      <div class="value">' . $content_escaped . '</div>
    </div>';
    return $html;
  }

  public static function render_checkbox ( $config, $data ) {
    $name = $config["name"];
    $label = Form::unescape_simple_tags( esc_html( $config["label"] ) );
    $html = '
    <div class="field '.esc_attr( $config["type"] ).'-field">
      <div class="label">' . $label . ':</div>
      <div class="value">';
    if ( isset( $data[$name] ) ) {
      if ( $data[$name] == 1 ) {
        $html .= '
        <span class="dashicons dashicons-yes-alt"></span> JA';
      } else {
        $html .= '
        <span class="dashicons dashicons-dismiss"></span> NEIN';
      }
    }
    else {
      $html .= '<span class="missing">(fehlt noch)</span>';
    }
    $html .= '
      </div>
    </div>';
    return $html;
  }

  public static function get_file_field_entry ( $type, $data ) {
    if ( empty( $data ) ) {
      return false;
    }
    foreach ( $data as $f ) {
      if ( $f["type"] == $type ) {
        return $f;
      }
    }
    return false;
  }

  public static function render_file ( $config, $data ) {
    $name = $config["name"];
    $file = Form::get_file_field_entry( $name, $data );
    if ( $file ) {
      $content_escaped = 'Hochgeladen am '.esc_html($file["updated"]);
      if ( $name == "picture" ) {
        $content_escaped .= '<br>
        <img src="' . esc_url(GSD_UPLOAD_URL.'/'.$file["filepath"]) . '" class="profile">';
      } else {
        // to be on the safe side we escape it and have to prepend a '/'
        // so esc_url() does not prepend a protocol schema.
        // this first character we want to remove then again
        $escaped_filename = esc_url('/'.$file["filepath"]);
        $escaped_filename = substr( $escaped_filename, 1 );
        $content_escaped .= '<br>
          <a href="' . GSD_UPLOAD_URL.'/'.$escaped_filename . '">' .
          $escaped_filename . '</a>';
      }
    } else {
      $content_escaped = '<span class="missing">(fehlt noch)</span>';
    }
    $html = '
    <div class="field '.esc_attr( $config["type"] ).'-field">
      <div class="label">' . esc_html( $config["label"] ) . ':</div>
      <div class="value">' . $content_escaped . '</div>
    </div>';
    return $html;
  }

  public static function render_school ( $config, $data, $options ) {
    $id = $data[ $config["name"] ];
    $value = $options[ $id ];
    $html = Form::render_standard_field(
      [
        "name" => $config["name"],
        "label" => $config["label"],
        "type" => "text",
      ],
      [
        $config["name"] => $value,
      ]
    );
    return $html;
  }

  public static function render_einsatzorga ( $config, $data, $options ) {
    $html = '';
    $field = $config["name"];
    $id = $data[$field];

    // in case an integer id is set (otherwise it should be "", resp. NULL in the db)
    if( preg_match( '/^[0-9]+$/', $id ) ) {
      $content_escaped = '';
      if( ! empty( $options[$id]["website"] ) ) {
        $content_escaped .= '<a href="'.esc_url($options[$id]["website"]).'">';
      }
      $content_escaped .= esc_html($options[$id]["name"]);
      if( ! empty( $options[$id]["website"] ) ) {
        $content_escaped .= '</a>';
      }
      $html .= '
      <div class="field text-field">
        <div class="label">' . esc_html($config["label"]) . ':</div>
        <div class="value">' . $content_escaped . '</div>
      </div>';
    }
    // in case an unlisted orga was chosen
    else {
      foreach( $config["alternative_fields"] as $f => $f_config ) {
        $html .= '
        <div class="field text-field">
          <div class="label">' . esc_html( $f_config["label"] ) . ':</div>
          <div class="value">' . esc_html( $data[$f] ) . '</div>
        </div>';
      }
    }

    return $html;
  }

  public static function render_language_course ( $config, $data ) {
    $html = '';
    $field = $config["name"];

    if( ! $data[$field] ) {
      $html .= '
      <div class="field text-field">
        <div class="label">' . esc_html($config["label"]) . ':</div>
        <div class="value"><span class="dashicons dashicons-dismiss"></span> NEIN</div>
      </div>';
      return $html;
    }

    $html .= '
    <div class="field text-field">
      <div class="label">' . esc_html($config["label"]) . ':</div>
      <div class="value"><span class="dashicons dashicons-yes-alt"></span> JA</div>
    </div>';
    foreach( $config["fields_if_yes"] as $f => $f_config ) {
      $value_escaped = empty( $data[$f] ) ?
        '<span class="missing">(fehlt noch)</span>' :
        esc_html( $data[$f] );
      $html .= '
      <div class="field text-field">
        <div class="label">' . esc_html($f_config["label"]) . ':</div>
        <div class="value">' . $value_escaped . '</div>
      </div>';
    }

    return $html;
  }

  /**
   * Unescape some simple tags (like b, i, br), if used without any arguments
   */
  public static function unescape_simple_tags ( $string ) {
    $allowed_tags = ["b", "i", "br", "p", "li", "ul", "ol"];
    foreach( $allowed_tags as $tag ) {
      $string = preg_replace('/&lt;'.$tag.'&gt;/', "<$tag>", $string);
      $string = preg_replace('/&lt;\/'.$tag.'&gt;/', "</$tag>", $string);
    }
    return $string;
  }
}
