<?php
namespace GSDDB;

class Database
{
  public static function get_dropdown_array_hochschule ()
  {
    global $wpdb;
    $tbl_kohorte = $wpdb->prefix . GSD_TABLE_KOHORTE;
    $tbl_kohorte_hochschule = $wpdb->prefix . GSD_TABLE_KOHORTE_HOCHSCHULE;
    $tbl_hochschule = $wpdb->prefix . GSD_TABLE_HOCHSCHULE;

    $query = "SELECT hochschule_id, name FROM $tbl_kohorte AS k
      JOIN $tbl_kohorte_hochschule AS k_h ON k.id = k_h.kohorte_id
      JOIN $tbl_hochschule AS h ON k_h.hochschule_id = h.id
      WHERE k.active = 1";

    $result = $wpdb->get_results( $query , ARRAY_A );

    $arr = [];
    foreach( $result as $row ) {
      $arr[$row["hochschule_id"]] = $row["name"];
    }
    return $arr;
  }

  public static function get_dropdown_array_kohorte () {
    global $wpdb;
    $tbl_kohorte = $wpdb->prefix . GSD_TABLE_KOHORTE;

    $query = "SELECT * FROM $tbl_kohorte";
    $result = $wpdb->get_results( $query , ARRAY_A );
    $arr = [];
    foreach( $result as $row ) {
      $arr[$row["id"]] = $row["label"];
    }
    return $arr;
  }

  public static function get_kohorte_by_hochschule ( $id ) {
    global $wpdb;
    $tbl_kohorte = $wpdb->prefix . GSD_TABLE_KOHORTE;
    $tbl_kohorte_hochschule = $wpdb->prefix . GSD_TABLE_KOHORTE_HOCHSCHULE;
    $tbl_hochschule = $wpdb->prefix . GSD_TABLE_HOCHSCHULE;

    $query = "SELECT kohorte_id FROM $tbl_kohorte AS k
      JOIN $tbl_kohorte_hochschule AS k_h ON k.id = k_h.kohorte_id
      JOIN $tbl_hochschule AS h ON k_h.hochschule_id = h.id
      WHERE k.active = 1 AND h.id = %d";
    $query = $wpdb->prepare( $query, $id );

    $result = $wpdb->get_results( $query , ARRAY_A );
    if ( empty( $result ) ) {
      return null;
    }
    return $result[0]["kohorte_id"];
  }

  public static function get_dropdown_array_einsatzorga ()
  {
    global $wpdb;
    $query = "SELECT id, name FROM " . $wpdb->prefix . GSD_TABLE_EINSATZORGA;
    $result = $wpdb->get_results( $query , ARRAY_A );

    $arr = [];
    foreach( $result as $row ) {
      $arr[$row["id"]] = $row["name"];
    }
    $arr["OTHER"] = "Sonstige";
    return $arr;
  }

  public static function get_einsatzorgas ()
  {
    global $wpdb;
    $query = "SELECT id, name, website FROM " . $wpdb->prefix . GSD_TABLE_EINSATZORGA;
    $result = $wpdb->get_results( $query , ARRAY_A );
    $arr = [];
    foreach( $result as $row ) {
      $arr[$row["id"]] = [
        "name" => $row["name"],
        "website" => $row["website"],
      ];
    }
    return $arr;
  }

  public static function get_radio_options_sprachkenntnisse ()
  {
    return [
      "<B2" => "< B2",
      ">=B2" => "B2 oder höher",
    ];
  }

  public static function get_radio_options_studien ()
  {
    return [
      "Soziale Arbeit" => "Soziale Arbeit",
      "Anderes" => "Anderes",
    ];
  }

  public static function get_dropdown_array_sprechkenntnisse () {
    return [
      "A1" => "A1",
      "A2" => "A2",
      "B1" => "B1",
      "B2" => "B2",
      "C1" => "C1",
      "C2" => "C2",
    ];
  }
}
