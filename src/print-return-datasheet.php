<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/wp-load.php');

if ( ! is_user_logged_in() ) {
  $msg = '[GSD error]: print-return-datasheet.php: an unauthenticated user accessed the print script!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  error_log($msg);
  exit;
}

function respond_with_error ($data) {
  header('Content-Type: application/json');
  http_response_code(400);
  echo json_encode($data);
  exit;
}

$wp_user = wp_get_current_user();
$user_id = $wp_user->user_login;
$print_on_load = 'onload="window.print()"';

if ( current_user_can("manage_options") && !empty( $_GET["id"] ) ) {
  if ( empty( $_GET["_wpnonce"] ) ) {
    respond_with_error([
      "error" => "missing nonce",
    ]);
  }
  if ( ! wp_verify_nonce($_GET["_wpnonce"], 'datasheet') ) {
    respond_with_error([
      "error" => "invalid nonce",
    ]);
  }
  if ( ! preg_match('/^[a-z0-9]+$/', $_GET["id"]) ) {
    respond_with_error([
      "error" => "malformed id parameter",
    ]);
  }
  $user_id = $_GET["id"];
  $print_on_load = '';
}

$tbl_return_form = $wpdb->prefix . GSD_TABLE_RETURN_FORM;
$tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
$tbl_einsatzorga = $wpdb->prefix . GSD_TABLE_EINSATZORGA;
$tbl_hochschule= $wpdb->prefix . GSD_TABLE_HOCHSCHULE;
$tbl_file= $wpdb->prefix . GSD_TABLE_FILE;

$query = "SELECT rt.*, status FROM $tbl_return_form AS rt
            LEFT JOIN $tbl_teilnehmerin AS tn
            ON rt.teilnehmerin_id = tn.id
            WHERE tn.matrikelnr = %s";
$query = $wpdb->prepare( $query, $user_id );
$user = $wpdb->get_row( $query );

if ( empty( $user ) ) {
  echo '
    No GSD participant data could be found for your account.
    This should not happen. Please contact the site administrator!
  ';
  $msg = '[GSD error]: print-return-datasheet.php: a user without participant data accessed the print script!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  $msg .= ' wp_user: ' . print_r($wp_user, true);
  error_log($msg);
  exit;
}

if ( $user->einsatzorga_id ) {
  $query = $wpdb->prepare("SELECT name, website FROM $tbl_einsatzorga WHERE id = %d", $user->einsatzorga_id);
  $einsatzorga = $wpdb->get_row( $query );
}

$query = $wpdb->prepare("SELECT * FROM $tbl_hochschule WHERE id = %d", $user->hochschule_id);
$hochschule = $wpdb->get_row( $query );

$query = $wpdb->prepare("SELECT * FROM $tbl_file WHERE teilnehmerin_id = %d", $user->teilnehmerin_id);
$files = $wpdb->get_results( $query );

switch ( $user->status ) {
  case 0: $status = "Teilnehmer*in in Voranmeldung"; break;
  case 1: $status = "Vorangemeldete*r Teilnehmer*in<br>(Fixanmeldung noch nicht eingereicht)"; break;
  case 12: $status = "Vorangemeldete*r Teilnehmer*in<br>(Fixanmeldung bereits eingereicht)"; break;
  case 2: $status = "Registrierte*r Teilnehmer*in"; break;
  case 21: $status = "Registrierte*r Teilnehmer*in<br>(mit eingereichtem Rückmeldefomular)"; break;
  case 3: $status = "Praktikum abgebrochen"; break;
}

?><!DOCTYPE html>
<html lang="de-AT">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?= plugin_dir_url(__FILE__).'../assets/css/print-datasheet.css' ?>">
  <title>GSD Datenblatt zur Rückkehr-Meldung</title>
</head>
<body <?= $print_on_load; ?>>
  <div class="header">
    <div>
      <h1>GSD-Rückkehrmelde-Datenblatt</h1>
      <h2><?= $user->firstname . ' ' . $user->lastname ?></h2>
      <p>
        Erstellungszeitpunkt: <?= date('r'); ?>
      </p>
      <p>
        Aktueller Status: <?= $status ?>
      </p>
    </div>
    <div class="logo">
      <img src="<?= plugin_dir_url(__FILE__).'../assets/img/gsd_print_logo.png' ?>">
    </div>
  </div>

  <div class="content">
    <h3>Persönliche Daten</h3>
    <div class="row">
      <div class="col">Vorname:</div>
      <div class="col"><?= $user->firstname ?></div>
    </div>
    <div class="row">
      <div class="col">Nachname:</div>
      <div class="col"><?= $user->lastname ?></div>
    </div>
    <div class="row">
      <div class="col">E-Mailadresse:</div>
      <div class="col"><?= $user->email ?></div>
    </div>
    <div class="row">
      <div class="col">Adressdaten:</div>
      <div class="col"><?= $user->adresse ?></div>
    </div>
    <div class="row">
      <div class="col">Telefonnummer:</div>
      <div class="col"><?= $user->telefon ?></div>
    </div>
    <div class="row">
      <div class="col">Skypekontakt:</div>
      <div class="col"><?= $user->skype ?></div>
    </div>

    <h3>FH-bezogene Daten</h3>
    <div class="row">
      <div class="col">Fachhochschule:</div>
      <div class="col"><?= $hochschule->name ?></div>
    </div>
    <div class="row">
      <div class="col">Matrikelnummer:</div>
      <div class="col"><?= $user->matrikelnr ?></div>
    </div>
    <div class="row">
      <div class="col">Name FH-Praxislektor*in:</div>
      <div class="col"><?= $user->lektorin ?></div>
    </div>

    <h3>Einsatzorganisation</h3>
    <div class="row">
      <div class="col">Einsatzorganisation:</div>
      <div class="col"><?php
        echo $user->einsatzorga_id ?
          $einsatzorga->name.' , '.$einsatzorga->website :
          $user->einsatzorga_other_name.' , '.$user->einsatzorga_other_website;
      ?></div>
    </div>
    <div class="row">
      <div class="col">Name Kontaktperson der Einsatzorganisation:</div>
      <div class="col"><?= $user->einsatzorga_kontakt ?></div>
    </div>
    <div class="row">
      <div class="col">E-Mailadresse Kontaktperson der Einsatzorganisation:</div>
      <div class="col"><?= $user->einsatzorga_kontakt_email ?></div>
    </div>
    <div class="row">
      <div class="col">Telefon Kontaktperson der Einsatzorganisation:</div>
      <div class="col"><?= $user->einsatzorga_kontakt_telefon ?></div>
    </div>

    <h3>Praktikumsstelle</h3>
    <div class="row">
      <div class="col">Name (und ggf. Abkürzung) Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle ?></div>
    </div>
    <div class="row">
      <div class="col">Land:</div>
      <div class="col"><?= $user->praktikumsstelle_land ?></div>
    </div>
    <div class="row">
      <div class="col">Adresse Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_adresse ?></div>
    </div>
    <div class="row">
      <div class="col">Website Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_website ?></div>
    </div>
    <div class="row">
      <div class="col">Name Praxisanleiter*in vor Ort:</div>
      <div class="col"><?= $user->praktikumsstelle_kontakt ?></div>
    </div>
    <div class="row">
      <div class="col">E-Mailadresse Praxisanleiter*in vor Ort:</div>
      <div class="col"><?= $user->praktikumsstelle_kontakt_email ?></div>
    </div>
    <div class="row">
      <div class="col">Telefon Praxisanleiter*in vor Ort:</div>
      <div class="col"><?= $user->praktikumsstelle_kontakt_telefon ?></div>
    </div>
    <div class="row">
      <div class="col">Ausreise:</div>
      <div class="col"><?= $user->ausreise ?></div>
    </div>
    <div class="row">
      <div class="col">Rückreise:</div>
      <div class="col"><?= $user->rueckkehr ?></div>
    </div>
    <div class="row">
      <div class="col">Praktikumsbeginn:</div>
      <div class="col"><?= $user->praktikum_beginn ?></div>
    </div>
    <div class="row">
      <div class="col">Praktikumsende:</div>
      <div class="col"><?= $user->praktikum_ende ?></div>
    </div>
    <div class="row">
      <div class="col">Praktikumsdauer (in Wochen):</div>
      <div class="col"><?= $user->praktikum_dauer ?></div>
    </div>

    <div class="row">
      <div class="col">Beschreibung Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_beschreibung ?></div>
    </div>
    <div class="row">
      <div class="col">Arbeitsbereiche, Tätigkeitsbereiche, Sektore:</div>
      <div class="col"><?= $user->praktikumsstelle_sektoren ?></div>
    </div>
    <div class="row">
      <div class="col">Tätigkeitsbereich / Anforderungsprofil Praktikant*in:</div>
      <div class="col"><?= $user->anforderungsprofil ?></div>
    </div>

    <h3>Sprachkenntnisse</h3>
    <div class="row">
      <div class="col">Erforderliche Sprachkenntnisse:</div>
      <div class="col"><?= $user->sprachmindestniveau ?></div>
    </div>
    <div class="row">
      <div class="col">Sprachniveau Amts-/Verbindungssprache:</div>
      <div class="col"><?= $user->sprachniveau_amtssprache ?></div>
    </div>
    <div class="row">
      <div class="col">Sprachniveau Lokalsprache:</div>
      <div class="col"><?= $user->sprachniveau_lokalsprache ?></div>
    </div>

    <h3>Erfahrungen im Praktikum</h3>
    <div class="row">
      <div class="col">Allgemeines - gut zu wissen:</div>
      <div class="col"><?= $user->erfahrungen_allgemein ?></div>
    </div>
    <div class="row">
      <div class="col">Kommunikation:</div>
      <div class="col"><?= $user->erfahrungen_kommunikation ?></div>
    </div>
    <div class="row">
      <div class="col">Wohnsituation:</div>
      <div class="col"><?= $user->erfahrungen_wohnsituation ?></div>
    </div>
    <div class="row">
      <div class="col">Weiterbildung vor Ort:</div>
      <div class="col"><?= $user->erfahrungen_weiterbildung ?></div>
    </div>
    <div class="row">
      <div class="col">Sicherheit:</div>
      <div class="col"><?= $user->erfahrungen_sicherheit ?></div>
    </div>
    <div class="row">
      <div class="col">Persönliches Highlight:</div>
      <div class="col"><?= $user->erfahrungen_highlight ?></div>
    </div>
    <div class="row">
      <div class="col">Persönliches Lowlight:</div>
      <div class="col"><?= $user->erfahrungen_lowlight ?></div>
    </div>
    <div class="row">
      <div class="col">Persönliche Rückmeldung (wird nicht veröffentlicht!):</div>
      <div class="col"><?= $user->erfahrungen_rueckmeldung ?></div>
    </div>

    <h3>Kosten</h3>
    <div class="row">
      <div class="col">Gesamtkosten (in EUR):</div>
      <div class="col"><?= $user->kosten_gesamt ?></div>
    </div>
    <div class="row">
      <div class="col">Erläuterung zu den Kosten des Praktikums:</div>
      <div class="col"><?= $user->kosten_kommentar ?></div>
    </div>
    <div class="row">
      <div class="col">Zusatzkosten Sprachtraining (in EUR):</div>
      <div class="col"><?= $user->kosten_sprachtraining ?></div>
    </div>

    <h3>Nachhaltigkeit</h3>
    <div class="row">
      <div class="col">Weitergabe von Erfahrungen:</div>
      <div class="col"><?= $user->nachhaltigkeit ?></div>
    </div>


    <h3>Bestätigungen</h3>
    <?php
    $files_missing = true;
    foreach ( $files as $file ) {
      switch ( $file->type ) {
        case 'cf_subsidy':
        case 'cf_internship':
        case 'cf_university':
          $files_missing = false;
          break;
      }
      if (!$files_missing) break;
    }
    if ( $files_missing ) {
      ?>
      <div class="row">
        <div class="col">Es wurden noch keine Dokumente hochgeladen.</div>
      </div>
      <?php
    } else {
      foreach ( $files as $file ) {
        switch ( $file->type ) {
          case 'cf_internship': $label = 'Praktikumsbestätigung durch Praktikumsstelle';
            break;
          case 'cf_university': $label = 'Praktikumsanerkennung durch die FH';
            break;
          case 'cf_subsidy': $label = 'Empfangsbestätigung Zuschuss Praktikumstelle';
            break;
          default:
            $label = false;
        }
        if ( $label ) {
          ?>
          <div class="row">
            <div class="col"><?= $label ?>:</div>
            <div class="col"><?= 'Zuletzt hochgeladen: '.$file->updated; ?></div>
          </div>
          <?php
        }
      }
    }
    ?>

    <h3>Förderungen</h3>
    <div class="row">
      <div class="col">Aus öffentlichen Mitteln erhalten/zugesagt/erwartet:</div>
      <div class="col"><?= $user->foerdermittel ?></div>
    </div>
    <div class="row">
      <div class="col">Anmerkung zu Förderstellen:</div>
      <div class="col"><?= $user->foerderstellen ?></div>
    </div>
    <div class="row">
      <div class="col">Art des Zuschuss:</div>
      <div class="col"><?= $user->zuschuss ?></div>
    </div>
    <div class="row">
      <div class="col">Überweisungsdaten:</div>
      <div class="col"><?= $user->ueberweisungsdaten ?></div>
    </div>

    <h3>Datenverarbeitung</h3>
    <div class="row">
      <div class="col">Ich stimme folgenden Datenschutbestimmungen zu:</div>
      <div class="col">
        <p>
          Ich nehme zur Kenntnis, dass sämtliche personenbezogene
          Daten vom GSD-Praktikaprogramm von der Programmträgerin
          FH Campus Wien sowie von Mitarbeiter*innen meiner Fachhochschule
          für die Zwecke der Programmverwaltung, Evaluierung und Vernetzung
          automationsunterstützt für 10 Jahre gespeichert, verarbeitet und
          verwendet werden können, sowie zum Zweck der Projektabrechnung der
          Fördergeberin übermittelt werden.
        </p>
        <p>
          Ich nehme weiters zur Kenntnis, dass (sofern ich dem in der
          Fixanmeldung zugestimmt habe) meine Daten zum Zwecke der
          Übermittlung von Programmneuigkeiten und Veranstaltungseinladungen
          per E-Mail sowie zur Vernetzung von Alumni*ae auch über die 10 Jahre
          hinaus gespeichert werden dürfen und zum Zweck der
          Informationsweitergabe vereinzelt an nachfolgende Praktikant*innen
          übermittelt werden können (durch die Programmverantwortlichen).
        </p>
        <p>
          Weiters nehme ich zur Kenntnis, dass mein Teilnahmebericht im Rahmen
          der Rückkehrmeldung anonymisiert auf der Website
          globalsocialdialog.at veröffentlicht wird.
        </p>

        <p>
          Dort, wo meine Einwilligung in die Datenverarbeitung
          erforderlich ist, habe ich das Recht, meine Einwilligung jederzeit
          (mündlich oder schriftlich) gegenüber der FH Campus Wien und/oder
          meiner Fachhochschule zu widerrufen. Der Widerruf meiner
          Einwilligung bezieht sich nicht auf Daten, die bis zu meinem
          Widerruf bereits verarbeitet worden sind.
          Details zur Datenschutzerklärung sind auch unter:
          globalsocialdialog.at/datenschutz zu finden.
        </p>
        <p>
          <b>
            Bitte gib die (mit Unterschrift und Stempel)
            unterzeichnete „Empfangsbestätigung Zuschuss Praktikumsstelle“
            (ORIGINAL), die unterzeichnete Praktikumsbestätigung (KOPIE), die
            Anerkennung des Praktikums durch die FH (z.b. Studienerfolgsnachweis)
            (KOPIE), sowie die Zusatzvereinbarung für Ausreisen im COVID-Kontext
            (ORIGINAL) und einen unterschriebenen Ausdruck dieses
            Rückkehrmelde-Datenblatts beim Reflexionsseminar ab!
          </b>
        </p>

      </div>
    </div>

    <div class="signature">
      <div>
        .............................. , am <?= date('m.d.Y'); ?> <br>
        <span class="label-place">Ort</span>
      </div>
      <div>
        ................................................. <br>
        Unterschrift
      </div>
    </div>

  </div>

  <div class="footer">
    <p>
      Dieser Auszug aus der GSD-Datenbank spiegelt den Datenstand mit folgendem
      Zeitstempel wider: <?= date('r'); ?>. Der Datensatz kann im Nachhinein
      verändert werden. Der Auszug wurde vom Skript unter folgender URL
      erstellt: <?= plugin_dir_url(__FILE__).basename(__FILE__) ?>
    </p>
  </div>
</body>
</html>
