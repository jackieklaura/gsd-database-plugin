<?php
namespace GSDDB;

class Activation {
  public static function activate () {
    // create the upload directory, if it is not there yet
    if ( ! is_dir( GSD_UPLOAD_DIR ) ) {
      mkdir ( GSD_UPLOAD_DIR, 0750 );
    }

    // set up the database tables
    global $wpdb;

    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $tbl_kohorte = $wpdb->prefix . GSD_TABLE_KOHORTE;
    $tbl_hochschule = $wpdb->prefix . GSD_TABLE_HOCHSCHULE;
    $tbl_kohorte_hochschule = $wpdb->prefix . GSD_TABLE_KOHORTE_HOCHSCHULE;
    $tbl_einsatzorga = $wpdb->prefix . GSD_TABLE_EINSATZORGA;
    $tbl_file = $wpdb->prefix . GSD_TABLE_FILE;
    $tbl_notfallkontakt = $wpdb->prefix . GSD_TABLE_NOTFALLKONTAKT;
    $tbl_return_form = $wpdb->prefix . GSD_TABLE_RETURN_FORM;
    $tbl_lektorin = $wpdb->prefix . GSD_TABLE_LECTURER;

    $charset_collate = $wpdb->get_charset_collate();

    // TODO: check why we get an error when updating tables with
    // foreign key constraints and add those constraints to teilnehmerin_id
    // (but not wp_user, because users and participants should be able to exist
    // without each other)

    $sql = [];
    $sql[] = "CREATE TABLE $tbl_teilnehmerin (
      id INT NOT NULL AUTO_INCREMENT,
      wp_user BIGINT(20) UNSIGNED,
      status TINYINT NOT NULL DEFAULT 0,
      kohorte_id INT NOT NULL,
      firstname VARCHAR(64),
      lastname VARCHAR(64),
      email VARCHAR(128),
      adresse VARCHAR(256),
      telefon VARCHAR(32),
      skype VARCHAR(64),
      birthdate VARCHAR(10),
      staatsangehoerigkeit VARCHAR(32),
      passnummer VARCHAR(32),
      ausstellungsdatum DATE,
      ausstellungsort VARCHAR(32),
      anmerkungen TEXT,
      studienbeihilfe BOOLEAN,
      hochschule_id INT,
      studium VARCHAR(64),
      vollzeit BOOLEAN,
      matrikelnr VARCHAR(16),
      lektorin VARCHAR(64),
      lektorin_email VARCHAR(128),
      lektorin_telefon VARCHAR(32),
      einsatzorga_id INT,
      einsatzorga_other_name VARCHAR(64),
      einsatzorga_other_website VARCHAR(64),
      einsatzorga_kontakt VARCHAR(64),
      einsatzorga_kontakt_email VARCHAR(128),
      einsatzorga_kontakt_telefon VARCHAR(32),
      einsatzland VARCHAR(64),
      praktikumsstelle VARCHAR(64),
      praktikumsstelle_adresse VARCHAR(256),
      praktikumsstelle_telefon VARCHAR(32),
      praktikumsstelle_website VARCHAR(64),
      praktikumsstelle_taetigkeitsfeld VARCHAR(256),
      praktikumsstelle_kontakt VARCHAR(64),
      praktikumsstelle_kontakt_email VARCHAR(128),
      praktikumsstelle_kontakt_telefon VARCHAR(32),
      praktikumsstelle_beschreibung TEXT,
      praktikum_beginn DATE,
      praktikum_ende DATE,
      praktikum_dauer TINYINT,
      arbeitsbereich VARCHAR(64),
      arbeitsbereich_sektor TEXT,
      arbeitsbereich_anforderung TEXT,
      erfahrung TEXT,
      ausreise VARCHAR(32),
      rueckkehr VARCHAR(32),
      sprache VARCHAR(32),
      sprachkenntnis VARCHAR(5),
      sprachkenntnis_testergebnis VARCHAR(2),
      sprachkenntnis_planung TEXT,
      sprachkurs BOOLEAN,
      sprachkurs_institut VARCHAR(64),
      sprachkurs_institut_website VARCHAR(64),
      sprachkurs_beginn DATE,
      sprachkurs_ende DATE,
      oeza_zuschuss BOOLEAN,
      oeza_zuschuss_ergaenzend BOOLEAN,
      datenverarbeitung BOOLEAN,
      verpflichtungserklaerung BOOLEAN,
      comment_internal TEXT,
      comment_for_participant TEXT,
      updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY  (id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_kohorte (
      id INT NOT NULL AUTO_INCREMENT,
      label VARCHAR(8),
      active BOOLEAN NOT NULL DEFAULT 0,
      PRIMARY KEY  (id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_hochschule (
      id INT NOT NULL AUTO_INCREMENT,
      name VARCHAR(64),
      PRIMARY KEY  (id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_kohorte_hochschule (
      kohorte_id INT NOT NULL,
      hochschule_id INT NOT NULL,
      PRIMARY KEY  (kohorte_id, hochschule_id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_einsatzorga (
      id INT NOT NULL AUTO_INCREMENT,
      name VARCHAR(64),
      website VARCHAR(64),
      PRIMARY KEY  (id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_file (
      id INT NOT NULL AUTO_INCREMENT,
      teilnehmerin_id INT NOT NULL,
      type VARCHAR(16),
      filepath VARCHAR(512),
      updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY  (id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_notfallkontakt (
      id INT NOT NULL AUTO_INCREMENT,
      teilnehmerin_id INT NOT NULL,
      fullname VARCHAR(64),
      email VARCHAR(128),
      telefon VARCHAR(32),
      skype VARCHAR(64),
      adresse VARCHAR(256),
      beziehungsgrad VARCHAR(32),
      ist_im_einsatzland BOOLEAN NOT NULL DEFAULT 0,
      anmerkungen TEXT,
      PRIMARY KEY  (id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_return_form (
      id INT NOT NULL AUTO_INCREMENT,
      teilnehmerin_id INT NOT NULL,
      firstname VARCHAR(64),
      lastname VARCHAR(64),
      email VARCHAR(128),
      adresse VARCHAR(256),
      telefon VARCHAR(32),
      skype VARCHAR(64),
      matrikelnr VARCHAR(16),
      hochschule_id INT,
      lektorin VARCHAR(64),

      einsatzorga_id INT,
      einsatzorga_other_name VARCHAR(64),
      einsatzorga_other_website VARCHAR(64),
      einsatzorga_kontakt VARCHAR(64),
      einsatzorga_kontakt_email VARCHAR(128),
      einsatzorga_kontakt_telefon VARCHAR(32),

      praktikum_beginn DATE,
      praktikum_ende DATE,
      praktikum_dauer TINYINT,
      ausreise VARCHAR(32),
      rueckkehr VARCHAR(32),
      praktikumsstelle VARCHAR(64),
      praktikumsstelle_website VARCHAR(64),
      praktikumsstelle_land VARCHAR(64),
      praktikumsstelle_adresse VARCHAR(256),
      praktikumsstelle_kontakt VARCHAR(64),
      praktikumsstelle_kontakt_email VARCHAR(128),
      praktikumsstelle_kontakt_telefon VARCHAR(32),
      praktikumsstelle_aenderung BOOLEAN DEFAULT 0,

      praktikumsstelle_beschreibung TEXT,
      praktikumsstelle_sektoren TEXT,
      anforderungsprofil TEXT,
      sprachmindestniveau TEXT,
      sprachniveau_amtssprache TEXT,
      sprachniveau_lokalsprache TEXT,

      erfahrungen_allgemein TEXT,
      erfahrungen_kommunikation TEXT,
      erfahrungen_wohnsituation TEXT,
      erfahrungen_weiterbildung TEXT,
      erfahrungen_sicherheit TEXT,
      erfahrungen_highlight TEXT,
      erfahrungen_lowlight TEXT,
      erfahrungen_rueckmeldung TEXT,

      kosten_gesamt INT,
      kosten_kommentar TEXT,
      kosten_sprachtraining INT,

      nachhaltigkeit TEXT,

      foerdermittel INT,
      foerderstellen TEXT,
      zuschuss VARCHAR(64),
      ueberweisungsdaten TEXT,
      kenntnisnahme BOOLEAN DEFAULT 0,

      updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      PRIMARY KEY (id)
    ) $charset_collate;
    ";

    $sql[] = "CREATE TABLE $tbl_lektorin (
      wp_user BIGINT(20) UNSIGNED,
      hochschule_id INT,
      PRIMARY KEY (wp_user)
    ) $charset_collate;
    ";

    // TODO: discuss with project lead
    //    should we add ON DELETE CASCADE to the foreign key contraint?
    //    how/where should data be retained after the programme?

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
  }
}
