<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/wp-load.php');

if ( ! is_user_logged_in() ) {
  $msg = '[GSD error]: print-datasheet.php: an unauthenticated user accessed the print script!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  error_log($msg);
  exit;
}

function respond_with_error ($data) {
  header('Content-Type: application/json');
  http_response_code(400);
  echo json_encode($data);
  exit;
}

$wp_user = wp_get_current_user();
$user_id = $wp_user->user_login;
$print_on_load = 'onload="window.print()"';

if (
  ( current_user_can("manage_options") || current_user_can("gsd_lecturer_read") )
  && !empty( $_GET["id"] )
) {
  if ( empty( $_GET["_wpnonce"] ) ) {
    respond_with_error([
      "error" => "missing nonce",
    ]);
  }
  if ( ! wp_verify_nonce($_GET["_wpnonce"], 'datasheet') ) {
    respond_with_error([
      "error" => "invalid nonce",
    ]);
  }
  if ( ! preg_match('/^[a-z0-9]+$/', $_GET["id"]) ) {
    respond_with_error([
      "error" => "malformed id parameter",
    ]);
  }
  $user_id = $_GET["id"];
  $print_on_load = '';
}

$tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
$tbl_einsatzorga = $wpdb->prefix . GSD_TABLE_EINSATZORGA;
$tbl_hochschule= $wpdb->prefix . GSD_TABLE_HOCHSCHULE;
$tbl_file= $wpdb->prefix . GSD_TABLE_FILE;
$tbl_notfallkontakt= $wpdb->prefix . GSD_TABLE_NOTFALLKONTAKT;

$query = $wpdb->prepare("SELECT * FROM $tbl_teilnehmerin WHERE matrikelnr = %s", $user_id);
$user = $wpdb->get_row( $query );

if ( empty( $user ) ) {
  echo '
    No GSD participant data could be found for your account.
    This should not happen. Please contact the site administrator!
  ';
  $msg = '[GSD error]: print-datasheet.php: a user with not participant data accessed the print script!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  $msg .= ' wp_user: ' . print_r($wp_user, true);
  error_log($msg);
  exit;
}

if ( $user->einsatzorga_id ) {
  $query = $wpdb->prepare("SELECT name, website FROM $tbl_einsatzorga WHERE id = %d", $user->einsatzorga_id);
  $einsatzorga = $wpdb->get_row( $query );
}

$query = $wpdb->prepare("SELECT * FROM $tbl_hochschule WHERE id = %d", $user->hochschule_id);
$hochschule = $wpdb->get_row( $query );

$query = $wpdb->prepare("SELECT * FROM $tbl_file WHERE teilnehmerin_id = %d", $user->id);
$files = $wpdb->get_results( $query );

$query = $wpdb->prepare("SELECT * FROM $tbl_notfallkontakt WHERE teilnehmerin_id = %d AND ist_im_einsatzland = 0", $user->id);
$notfallkontakt_home = $wpdb->get_row( $query );
$query = $wpdb->prepare("SELECT * FROM $tbl_notfallkontakt WHERE teilnehmerin_id = %d AND ist_im_einsatzland = 1", $user->id);
$notfallkontakt_remote = $wpdb->get_row( $query );

switch ( $user->status ) {
  case 0: $status = "Teilnehmer*in in Voranmeldung"; break;
  case 1: $status = "Vorangemeldete*r Teilnehmer*in<br>(Fixanmeldung noch nicht eingereicht)"; break;
  case 12: $status = "Vorangemeldete*r Teilnehmer*in<br>(Fixanmeldung bereits eingereicht)"; break;
  case 2: $status = "Registrierte*r Teilnehmer*in"; break;
  case 21: $status = "Registrierte*r Teilnehmer*in<br>(mit eingereichtem Rückmeldefomular)"; break;
  case 3: $status = "Praktikum abgebrochen"; break;
}

?><!DOCTYPE html>
<html lang="de-AT">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?= plugin_dir_url(__FILE__).'../assets/css/print-datasheet.css' ?>">
  <title>GSD Datenblatt</title>
</head>
<body <?= $print_on_load; ?>>
  <div class="header">
    <div>
      <h1>GSD-Fixanmeldedatenblatt</h1>
      <h2><?= $user->firstname . ' ' . $user->lastname ?></h2>
      <p>
        Erstellungszeitpunkt: <?= date('r'); ?>
      </p>
      <p>
        Aktueller Status: <?= $status ?>
      </p>
    </div>
    <div class="logo">
      <img src="<?= plugin_dir_url(__FILE__).'../assets/img/gsd_print_logo.png' ?>">
    </div>
  </div>

  <div class="content">
    <h3>Persönliche Daten</h3>
    <div class="row">
      <div class="col">Vorname:</div>
      <div class="col"><?= $user->firstname ?></div>
    </div>
    <div class="row">
      <div class="col">Nachname:</div>
      <div class="col"><?= $user->lastname ?></div>
    </div>
    <div class="row">
      <div class="col">Geburtsdatum:</div>
      <div class="col"><?= $user->birthdate ?></div>
    </div>
    <div class="row">
      <div class="col">E-Mailadresse:</div>
      <div class="col"><?= $user->email ?></div>
    </div>
    <div class="row">
      <div class="col">Adressdaten:</div>
      <div class="col"><?= $user->adresse ?></div>
    </div>
    <div class="row">
      <div class="col">Telefonnummer:</div>
      <div class="col"><?= $user->telefon ?></div>
    </div>
    <div class="row">
      <div class="col">Skypekontakt:</div>
      <div class="col"><?= $user->skype ?></div>
    </div>
    <div class="row">
      <div class="col">Staatsangehörigkeit:</div>
      <div class="col"><?= $user->staatsangehoerigkeit ?></div>
    </div>
    <div class="row">
      <div class="col">Reisepassnummer:</div>
      <div class="col"><?= $user->passnummer ?></div>
    </div>
    <div class="row">
      <div class="col">Reisepass Ausstellungsdatum:</div>
      <div class="col"><?= $user->ausstellungsdatum ?></div>
    </div>
    <div class="row">
      <div class="col">Reisepass Ausstellungsort:</div>
      <div class="col"><?= $user->ausstellungsort ?></div>
    </div>
    <div class="row">
      <div class="col">Ich beziehe Studienbeihilfe bzw. Selbsterhalterstipendium:</div>
      <div class="col"><?= $user->studienbeihilfe ? 'Ja' : 'Nein' ?></div>
    </div>
    <div class="row">
      <div class="col">Evtl. Erkrankungen, Allergien, spezielle Bedürfnisse:</div>
      <div class="col"><?= $user->anmerkungen ?></div>
    </div>

    <h3>FH-bezogene Daten</h3>
    <div class="row">
      <div class="col">Fachhochschule:</div>
      <div class="col"><?= $hochschule->name ?></div>
    </div>
    <div class="row">
      <div class="col">Matrikelnummer:</div>
      <div class="col"><?= $user->matrikelnr ?></div>
    </div>
    <div class="row">
      <div class="col">Studiengang:</div>
      <div class="col"><?= $user->studium ?></div>
    </div>
    <div class="row">
      <div class="col">Vollzeitstudium:</div>
      <div class="col"><?= $user->vollzeit ? 'Ja' : 'Nein' ?></div>
    </div>
    <div class="row">
      <div class="col">Name FH-Praxislektor*in:</div>
      <div class="col"><?= $user->lektorin ?></div>
    </div>
    <div class="row">
      <div class="col">E-Mailadresse FH-Praxislektor*in:</div>
      <div class="col"><?= $user->lektorin_email ?></div>
    </div>
    <div class="row">
      <div class="col">Telefon FH-Praxislektor*in:</div>
      <div class="col"><?= $user->lektorin_telefon ?></div>
    </div>

    <h3>Einsatzorganisation</h3>
    <div class="row">
      <div class="col">Einsatzorganisation:</div>
      <div class="col"><?php
        echo $user->einsatzorga_id ?
          $einsatzorga->name.' , '.$einsatzorga->website :
          $user->einsatzorga_other_name.' , '.$user->einsatzorga_other_website;
      ?></div>
    </div>
    <div class="row">
      <div class="col">Name Kontaktperson der Einsatzorganisation:</div>
      <div class="col"><?= $user->einsatzorga_kontakt ?></div>
    </div>
    <div class="row">
      <div class="col">E-Mailadresse Kontaktperson der Einsatzorganisation:</div>
      <div class="col"><?= $user->einsatzorga_kontakt_email ?></div>
    </div>
    <div class="row">
      <div class="col">Telefon Kontaktperson der Einsatzorganisation:</div>
      <div class="col"><?= $user->einsatzorga_kontakt_telefon ?></div>
    </div>

    <h3>Praktikumsstelle</h3>
    <div class="row">
      <div class="col">Einsatzland:</div>
      <div class="col"><?= $user->einsatzland ?></div>
    </div>
    <div class="row">
      <div class="col">Name (und ggf. Abkürzung) Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle ?></div>
    </div>
    <div class="row">
      <div class="col">Adresse Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_adresse ?></div>
    </div>
    <div class="row">
      <div class="col">Telefon Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_telefon ?></div>
    </div>
    <div class="row">
      <div class="col">Website Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_website ?></div>
    </div>
    <div class="row">
      <div class="col">Arbeitsbereich:</div>
      <div class="col"><?= $user->arbeitsbereich ?></div>
    </div>
    <div class="row">
      <div class="col">Name Praxisanleiter*in vor Ort:</div>
      <div class="col"><?= $user->praktikumsstelle_kontakt ?></div>
    </div>
    <div class="row">
      <div class="col">E-Mailadresse Praxisanleiter*in vor Ort:</div>
      <div class="col"><?= $user->praktikumsstelle_kontakt_email ?></div>
    </div>
    <div class="row">
      <div class="col">Telefon Praxisanleiter*in vor Ort:</div>
      <div class="col"><?= $user->praktikumsstelle_kontakt_telefon ?></div>
    </div>
    <div class="row">
      <div class="col">Ausreise:</div>
      <div class="col"><?= $user->ausreise ?></div>
    </div>
    <div class="row">
      <div class="col">Rückreise:</div>
      <div class="col"><?= $user->rueckkehr ?></div>
    </div>
    <div class="row">
      <div class="col">Praktikumsbeginn:</div>
      <div class="col"><?= $user->praktikum_beginn ?></div>
    </div>
    <div class="row">
      <div class="col">Praktikumsende:</div>
      <div class="col"><?= $user->praktikum_ende ?></div>
    </div>
    <div class="row">
      <div class="col">Praktikumsdauer (in Wochen):</div>
      <div class="col"><?= $user->praktikum_dauer ?></div>
    </div>
    <div class="row">
      <div class="col">Beschreibung Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_beschreibung ?></div>
    </div>
    <div class="row">
      <div class="col">Arbeitsbereiche/Sektoren, in denen die/der Praktikant*in tätig sein wird:</div>
      <div class="col"><?= $user->arbeitsbereich_sektor ?></div>
    </div>
    <div class="row">
      <div class="col">Tätigkeitsbereich / Anforderungsprofil Praktikant*in:</div>
      <div class="col"><?= $user->arbeitsbereich_anforderung ?></div>
    </div>
    <div class="row">
      <div class="col">Bisherige Erfahrung:</div>
      <div class="col"><?= $user->erfahrung ?></div>
    </div>

    <h3>Sprachkenntnisse</h3>
    <div class="row">
      <div class="col">Sprache Einsatzland:</div>
      <div class="col"><?= $user->sprache ?></div>
    </div>
    <div class="row">
      <div class="col">Aktuelle Sprachkenntnisse (nach europ. Referenzrahmen) laut Sprachtest:</div>
      <div class="col"><?= $user->sprachkenntnis_testergebnis ?></div>
    </div>
    <div class="row">
      <div class="col">Geplante Maßnahmen zum Spracherwerb, sofern das Sprachtestergebnis unter B2 liegt:</div>
      <div class="col"><?= $user->sprachkenntnis_planung ?></div>
    </div>
    <div class="row">
      <div class="col">Sprachkurs vor Ort:</div>
      <div class="col"><?php
        echo $user->sprachkurs ?
          $user->sprachkurs_institut.' , '.$user->sprachkurs_institut_website.', von '.$user->sprachkurs_beginn.' bis '.$user->sprachkurs_ende :
          'Nein'
      ?></div>
    </div>

    <h3>Notfallkontakt Zuhause</h3>
    <div class="row">
      <div class="col">Vor- und Nachname:</div>
      <div class="col"><?= empty($notfallkontakt_home) ? '' : $notfallkontakt_home->fullname ?></div>
    </div>
    <div class="row">
      <div class="col">Beziehungsgrad:</div>
      <div class="col"><?= empty($notfallkontakt_home) ? '' : $notfallkontakt_home->beziehungsgrad ?></div>
    </div>
    <div class="row">
      <div class="col">e-Mailadresse:</div>
      <div class="col"><?= empty($notfallkontakt_home) ? '' : $notfallkontakt_home->email ?></div>
    </div>
    <div class="row">
      <div class="col">Telefonnummer:</div>
      <div class="col"><?= empty($notfallkontakt_home) ? '' : $notfallkontakt_home->telefon ?></div>
    </div>
    <div class="row">
      <div class="col">Skypekontakt:</div>
      <div class="col"><?= empty($notfallkontakt_home) ? '' : $notfallkontakt_home->skype ?></div>
    </div>
    <div class="row">
      <div class="col">Adresse:</div>
      <div class="col"><?= empty($notfallkontakt_home) ? '' : $notfallkontakt_home->adresse ?></div>
    </div>
    <div class="row">
      <div class="col">Allfällige wichtige Informationen, ggf. Daten weitere Kontaktpersonen:</div>
      <div class="col"><?= empty($notfallkontakt_home) ? '' : $notfallkontakt_home->anmerkungen ?></div>
    </div>

    <h3>Notfallkontakt im Einsatzland</h3>
    <div class="row">
      <div class="col">Vor- und Nachname:</div>
      <div class="col"><?= empty($notfallkontakt_remote) ? '' : $notfallkontakt_remote->fullname ?></div>
    </div>
    <div class="row">
      <div class="col">Beziehungsgrad:</div>
      <div class="col"><?= empty($notfallkontakt_remote) ? '' : $notfallkontakt_remote->beziehungsgrad ?></div>
    </div>
    <div class="row">
      <div class="col">e-Mailadresse:</div>
      <div class="col"><?= empty($notfallkontakt_remote) ? '' : $notfallkontakt_remote->email ?></div>
    </div>
    <div class="row">
      <div class="col">Telefonnummer:</div>
      <div class="col"><?= empty($notfallkontakt_remote) ? '' : $notfallkontakt_remote->telefon ?></div>
    </div>
    <div class="row">
      <div class="col">Skypekontakt:</div>
      <div class="col"><?= empty($notfallkontakt_remote) ? '' : $notfallkontakt_remote->skype ?></div>
    </div>
    <div class="row">
      <div class="col">Adresse:</div>
      <div class="col"><?= empty($notfallkontakt_remote) ? '' : $notfallkontakt_remote->adresse ?></div>
    </div>
    <div class="row">
      <div class="col">Allfällige wichtige Informationen, ggf. Daten weiterre Kontaktpersonen:</div>
      <div class="col"><?= empty($notfallkontakt_remote) ? '' : $notfallkontakt_remote->anmerkungen ?></div>
    </div>

    <h3>Anmeldeunterlagen</h3>
    <?php
    $files_missing = true;
    foreach ( $files as $file ) {
      switch ( $file->type ) {
        case 'picture':
        case 'motivation':
        case 'identity':
        case 'languagetest':
          $files_missing = false;
          break;
      }
      if (!$files_missing) break;
    }
    if ( $files_missing ) {
      ?>
      <div class="row">
        <div class="col">Es wurden noch keine Dokumente hochgeladen.</div>
      </div>
      <?php
    } else {
      foreach ( $files as $file ) {
        switch ( $file->type ) {
          case 'picture': $label = 'Foto';
            break;
          case 'motivation': $label = 'Motivationsschreiben';
            break;
          case 'identity': $label = 'Identitätsnachweis';
            break;
          case 'languagetest': $label = 'Sprachtestergebnis';
            break;
          default:
            $label = false;
        }
        if ( $label ) {
          ?>
          <div class="row">
            <div class="col"><?= $label ?>:</div>
            <div class="col"><?= 'Zuletzt hochgeladen: '.$file->updated; ?></div>
          </div>
          <?php
        }
      }
    }
    ?>

    <h3>Zuschussansuchen</h3>
    <div class="row">
      <div class="col">Ansuchen um gesamten OEZA-Zuschuss:</div>
      <div class="col"><?= $user->oeza_zuschuss ? 'Ja' : 'Nein' ?></div>
    </div>
    <div class="row">
      <div class="col">Ansuchen um ergänzenden OEZA-Zuschuss:</div>
      <div class="col"><?= $user->oeza_zuschuss_ergaenzend ? 'Ja' : 'Nein' ?></div>
    </div>

    <h3>Verpflichtungserklärung</h3>
    <div class="row">
      <div class="col">Ich stimmte hiermit folgender Verpflichtungs-erklärung zu:</div>
      <div class="col">
        <p>Ich verpflichte mich, die im Folgenden genannten Punkte zu erfüllen:</p>
        <ul>
          <li>Die Lernziele mit der Praktikumsstelle zeitgerecht zu vereinbaren</li>
          <li>Die Seminarreihe (Vorbereitungs- und Nachbereitungsseminare) zur Gänze zu besuchen</li>
          <li>Die individuelle inhaltliche und praktische Vorbereitung
            auf das Einsatzland und Projekt durchzuführen:
            <ul>
              <li>Inhaltliche Auseinandersetzung mit dem lokalen
                und globalen Kontext der Praktikumsstelle, der Tätigkeit und der Zielgruppe</li>
              <li>Austausch mit Rückkehrer*innen, Expert*innen,
                Personen, aktuellen Informationen aus dem Einsatzland</li>
              <li>Gegebenenfalls Erstellung von praktikumsbezogenen
                Unterlagen zur Präsentation bei den Seminarreihen</li>
              <li>Selbstständige Erledigung von allen organisatorischen
                Schritten wie Flugbuchung, Versicherungen, Impfungen, Visa etc.</li>
            </ul>
          </li>
          <li>An den Einführungs- und Nachbereitungstreffen meiner
            Einsatzorganisation teilzunehmen und die Termine dafür zeitgerecht
            individuell mit dieser zu vereinbaren (1. Termin möglichst vor dem
            Vorbereitungsseminar)</li>
          <li>Die Amts- oder Verkehrssprache der Praktikumsstelle
            (Englisch, Französisch, Spanisch oder Portugiesisch) zu beherrschen
            und dies mit Sprachtest nachzuweisen (Niveau mind. B2 nach dem
            europäischen Referenzrahmen).</li>
          <li>Dafür ggf. auch einen notwendigen mehrwöchigen Sprachkurs
            vor dem Praktikum (ggf. vor Ort) zu planen und zu absolvieren.</li>
          <li>Grundkenntnisse der Lokalsprache möglichst frühzeitig
            vor der Ausreise zu erwerben.</li>
          <li>Den Teilnahmebeitrag zum GSD-Praktikaprogramm von
            350€ ggf. zu leisten (dieser Beitrag wird jedoch in der Regel von
            FH-Studiengängen zum Teil oder zur Gänze übernommen – Infos dazu
            erfolgen bei den Informationsveranstaltungen).</li>
          <li>Entsprechende Zuschussansuchen laut bereitgestellten
            Informationen zu erstellen.</li>
          <li>Mich um einen angemessenen Versicherungsschutz (Kranken-,
            Unfall- und Haftpflichtversicherung) zu kümmern.</li>
          <li>Die Absolvierung des Praktikums von der Praktikumsstelle
            im Einsatzland bestätigen zu lassen. Die Wochen-Mindestdauer verkürzt
            sich nicht bei Leistung einer erhöhten Wochenstundenanzahl. Dauer ohne
            Sprachkurs gewöhnlich mindestens 15 Wochen; Berufsbegleitender Studiengang:
            Dauer ohne Sprachkurs gewöhnlich mindestens 10 Wochen.</li>
          <li>Ggf. an internen und externen (Vernetzungs-)Veranstaltungen
            der FH sowie der Einsatzorganisation im Zusammenhang mit der
            Absolvierung des Berufspraktikums mitzuarbeiten.</li>
          <li>Ggf. zur Vernetzung mit mir nachfolgenden Praktikant*innen
            zur Verfügung zu stehen und zu diesem Zweck der Übergabe meiner
            Kontaktdaten (Name, E-Mail-Adresse, Telefonnummer) zuzustimmen.</li>
          <li>Zum Zweck der Informationsweitergabe an nachfolgende
            Praktikant*innen im Rahmen der Rückkehrmeldung Fragen zum
            Praktikumsaufenthalt (=Teilnahmebericht) zu beantworten, die
            anonymisiert auf der GSD-Webseite veröffentlich werden können.</li>
        </ul>
        <p>Erklärung:</p>
        <p>Ich absolviere mein Praktikum in einem Land Afrikas,
          Asiens oder Lateinamerikas eigenverantwortlich aus freien Stücken
          und bin mir bewusst, dass damit ggf. besondere physische und
          psychische Risiken verbunden sind. Ich bestätige, im Zusammenhang
          mit meinem Auslandsaufenthalt selbst für ausreichenden
          Versicherungsschutz zu sorgen und sicher zu gehen, dass zum
          Zeitpunkt der Ausreise ein vollständiger Impfschutz gegen COVID-19
          aufrecht ist.</p>
        <p>Hiermit bestätige ich, dass meine Notfallkontakte
          mit der Weitergabe ihrer/seiner Daten einverstanden sind und im
          Notfall kontaktiert werden dürfen.</p>
        <p>Des Weiteren werde ich für die Zeit meines
          Auslandsaufenthaltes der zuständigen Vertretung meines Landes meine
          Reisedaten für den Notfall zur Kenntnis bringen (z.B. durch
          Reiseregistrierung beim Außenministerium).</p>
        <p>Datenverarbeitungsklausel:</p>
        <p>Ich nehme zur Kenntnis , dass sämtliche personenbezogene
          Daten vom GSD-Praktikaprogramm, von der Programmträgerin FH Campus
          Wien sowie von Mitarbeiter*innen meiner Fachhochschule für die
          Zwecke der Programmverwaltung, Evaluierung und Vernetzung
          automationsunterstützt für 10 Jahre gespeichert, verarbeitet und
          verwendet werden können, sowie zum Zweck der Projektabrechnung der
          Fördergeberin übermittelt werden.</p>
        <p>Weiters  nehme ich zur Kenntnis, dass mein Teilnahmebericht
          im Rahmen der Rückkehrmeldung anonymisiert auf der Website
          https://globalsocialdialog.at/ veröffentlicht wird. Details zur
          Datenschutzerklärung sind auch unter
          https://globalsocialdialog.at/datenschutz/ zu finden.</p>
        <p>Weiters willige ich ein, dass meine Daten zum Zwecke der
          Übermittlung von Programmneuigkeiten und  Veranstaltungseinladungen
          per E-Mail sowie zur Vernetzung von Alumni*ae  auch darüber hinaus
          gespeichert werden dürfen und zum Zweck der Informationsweitergabe
          vereinzelt an nachfolgende Praktikant*innen übermittelt werden
          können (durch die Programmverantwortlichen).</p>
        <p>Dort, wo meine Einwilligung in die Datenverarbeitung
          erforderlich ist, habe ich das Recht, meine Einwilligung jederzeit
          (mündlich oder schriftlich) gegenüber der FH Campus Wien und/oder
          meiner Fachhochschule zu widerrufen. Der Widerruf meiner
          Einwilligung bezieht sich nicht auf Daten, die bis zu meinem
          Widerruf bereits verarbeitet worden sind.</p>
      </div>
    </div>

    <div class="signature">
      <div>
        .............................. , am <?= date('m.d.Y'); ?> <br>
        <span class="label-place">Ort</span>
      </div>
      <div>
        ................................................. <br>
        Unterschrift
      </div>
    </div>

  </div>

  <div class="footer">
    <p>
      Dieser Auszug aus der GSD-Datenbank spiegelt den Datenstand mit folgendem
      Zeitstempel wider: <?= date('r'); ?>. Der Datensatz kann im Nachhinein
      verändert werden. Der Auszug wurde vom Skript unter folgender URL
      erstellt: <?= plugin_dir_url(__FILE__).basename(__FILE__) ?>
    </p>
  </div>

</body>
</html>
