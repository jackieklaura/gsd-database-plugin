<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/wp-load.php');

if ( ! is_user_logged_in() ) {
  $msg = '[GSD error]: print-return-report.php: an unauthenticated user accessed the print script!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  error_log($msg);
  exit;
}

if ( ! current_user_can("manage_options") ) {
  $msg = '[GSD error]: print-return-report.php: a non-admin user accessed the print script!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  error_log($msg);
  exit;
}

function respond_with_error ($data) {
  header('Content-Type: application/json');
  http_response_code(400);
  echo json_encode($data);
  exit;
}

// validate GET parameters and nonce
if ( empty( $_GET["_wpnonce"] ) ) {
  respond_with_error([
    "error" => "missing nonce",
  ]);
}
if ( ! wp_verify_nonce($_GET["_wpnonce"], 'report') ) {
  respond_with_error([
    "error" => "invalid nonce",
  ]);
}
if ( empty( $_GET["id"] ) ) {
  respond_with_error([
    "error" => "missing parameter: id",
  ]);
}
if ( ! preg_match('/^[a-z0-9]+$/', $_GET["id"]) ) {
  respond_with_error([
    "error" => "malformed id parameter",
  ]);
}
$id = $_GET["id"];

// query all required info from database
$tbl_return_form = $wpdb->prefix . GSD_TABLE_RETURN_FORM;
$tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
$tbl_einsatzorga = $wpdb->prefix . GSD_TABLE_EINSATZORGA;

$query = "SELECT rt.* FROM $tbl_return_form AS rt
            LEFT JOIN $tbl_teilnehmerin AS tn
            ON rt.teilnehmerin_id = tn.id
            WHERE tn.matrikelnr = %s";
$query = $wpdb->prepare( $query, $id );
$user = $wpdb->get_row( $query );

if ( empty( $user ) ) {
  echo '
    No GSD participant data could be found with this participant id.
    This should not happen. Please contact the site administrator!
  ';
  $msg = '[GSD error]: print-return-report.php: non-existing participant id was queried!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  $msg .= ' wp_user: ' . print_r($wp_user, true);
  error_log($msg);
  exit;
}

// if einsatzorga is preset, we need that info as well
if ( $user->einsatzorga_id ) {
  $query = $wpdb->prepare("SELECT name, website FROM $tbl_einsatzorga WHERE id = %d", $user->einsatzorga_id);
  $einsatzorga = $wpdb->get_row( $query );
}


// output the report
?><!DOCTYPE html>
<html lang="de-AT">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="<?= plugin_dir_url(__FILE__).'../assets/css/print-datasheet.css' ?>">
  <title>GSD Teilnehmer*innen Bericht</title>
</head>
<body>
  <div class="header">
    <div>
      <h1>GSD Teilnehmer*innen Bericht</h1>
      <h2><?= $user->praktikumsstelle; ?></h2>
    </div>
    <div class="logo">
      <img src="<?= plugin_dir_url(__FILE__).'../assets/img/gsd_print_logo.png' ?>">
    </div>
  </div>

  <div class="content">

    <h3>Einsatzorganisation</h3>
    <?php
    $eo_name = $user->einsatzorga_id ? $einsatzorga->name : $user->einsatzorga_other_name;
    $eo_url = $user->einsatzorga_id ? $einsatzorga->website : $user->einsatzorga_other_website;
    ?>
    <div class="row">
      <div class="col">Einsatzorganisation:</div>
      <div class="col"><?= $eo_name ?></div>
    </div>
    <div class="row">
      <div class="col">Website der Einsatzorganisation:</div>
      <div class="col"><a href="<?= $eo_url ?>"><?= $eo_url ?></a></div>
    </div>

    <h3>Praktikumsstelle</h3>
    <div class="row">
      <div class="col">Praktikumsdauer (in Wochen):</div>
      <div class="col"><?= $user->praktikum_dauer ?></div>
    </div>
    <div class="row">
      <div class="col">Name (und ggf. Abkürzung) Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle ?></div>
    </div>
    <div class="row">
      <div class="col">Website Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_website ?></div>
    </div>
    <div class="row">
      <div class="col">Land:</div>
      <div class="col"><?= $user->praktikumsstelle_land ?></div>
    </div>

    <div class="row">
      <div class="col">Beschreibung Praktikumsstelle:</div>
      <div class="col"><?= $user->praktikumsstelle_beschreibung ?></div>
    </div>
    <div class="row">
      <div class="col">Arbeitsbereiche, Tätigkeitsbereiche, Sektore:</div>
      <div class="col"><?= $user->praktikumsstelle_sektoren ?></div>
    </div>
    <div class="row">
      <div class="col">Tätigkeitsbereich / Anforderungsprofil Praktikant*in:</div>
      <div class="col"><?= $user->anforderungsprofil ?></div>
    </div>

    <div class="row">
      <div class="col">Erforderliche Sprachkenntnisse:</div>
      <div class="col"><?= $user->sprachmindestniveau ?></div>
    </div>
    <div class="row">
      <div class="col">Sprachniveau Amts-/Verbindungssprache:</div>
      <div class="col"><?= $user->sprachniveau_amtssprache ?></div>
    </div>
    <div class="row">
      <div class="col">Sprachniveau Lokalsprache:</div>
      <div class="col"><?= $user->sprachniveau_lokalsprache ?></div>
    </div>

    <h3>Besondere Erfahrungen und Hinweise</h3>
    <div class="row">
      <div class="col">Allgemeines - gut zu wissen:</div>
      <div class="col"><?= $user->erfahrungen_allgemein ?></div>
    </div>
    <div class="row">
      <div class="col">Kommunikation:</div>
      <div class="col"><?= $user->erfahrungen_kommunikation ?></div>
    </div>
    <div class="row">
      <div class="col">Wohnsituation:</div>
      <div class="col"><?= $user->erfahrungen_wohnsituation ?></div>
    </div>
    <div class="row">
      <div class="col">Weiterbildung vor Ort:</div>
      <div class="col"><?= $user->erfahrungen_weiterbildung ?></div>
    </div>
    <div class="row">
      <div class="col">Sicherheit:</div>
      <div class="col"><?= $user->erfahrungen_sicherheit ?></div>
    </div>
    <div class="row">
      <div class="col">Persönliches Highlight:</div>
      <div class="col"><?= $user->erfahrungen_highlight ?></div>
    </div>

    <h3>Kosten</h3>
    <div class="row">
      <div class="col">Gesamtkosten (in EUR):</div>
      <div class="col"><?= $user->kosten_gesamt ?></div>
    </div>
    <div class="row">
      <div class="col">Erläuterung zu den Kosten des Praktikums:</div>
      <div class="col"><?= $user->kosten_kommentar ?></div>
    </div>
    <div class="row">
      <div class="col">Zusatzkosten Sprachtraining (in EUR):</div>
      <div class="col"><?= $user->kosten_sprachtraining ?></div>
    </div>

  </div>
</body>
</html>
