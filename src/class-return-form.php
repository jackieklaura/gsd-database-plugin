<?php
namespace GSDDB;

require_once plugin_dir_path( __FILE__ ) . 'class-form.php';
require_once plugin_dir_path( __FILE__ ) . 'class-notification.php';
require_once plugin_dir_path( __FILE__ ) . 'class-database.php';

class Return_Form
{
  private $participant;
  private $schools;
  private $orgas;
  private $files;
  private $section_person;
  private $section_orga;
  private $section_internship;
  private $section_experiences;
  private $section_costs;
  private $section_sustainability;
  private $section_files;
  private $section_final;
  public $return_data_available;

  public function __construct( $participant_data ) {
    global $wpdb;
    $this->participant = $participant_data;

    // initialise schools
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_HOCHSCHULE);
    foreach ( $r as $row ) {
      $this->schools[ $row->id ] = $row->name;
    }

    // initialise organisations
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_EINSATZORGA);
    foreach ( $r as $row ) {
      $this->orgas[ $row->id ] = [
        "name" => $row->name,
        "website" => $row->website
      ];
    }

    // TODO: initialise files
    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_FILE .
      " WHERE teilnehmerin_id = %d";
    $q = $wpdb->prepare( $q, $participant_data["teilnehmerin_id"] );
    $this->files = $wpdb->get_results( $q, ARRAY_A );

    // configure the form sections
    $this->section_person = [
      "title" => "Persönliche Daten",
      "slug" => "person",
      "submit_label" => "Zwischenspeichern",
      "fixed_fields" => [
        [
          "name" => "hochschule_id",
          "label" => "Fachhochschule", "type" => "school",
          "options" => Database::get_dropdown_array_hochschule(),
        ],
        [
          "name" => "matrikelnr",
          "label" => "Matrikelnummer", "type" => "text",
        ],
      ],
      "fields" => [
        [
          "name" => "firstname",
          "label" => "Vorname", "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "lastname",
          "label" => "Nachname", "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "email",
          "label" => "E-Mailadresse (mit langfristiger Erreichbarkeit!)",
          "type" => "email", "sanitize" => "email",
          "mandatory" => true,
          "length" => 128,
        ],
        [
          "name" => "adresse", "label" => "Adressdaten (PLZ, Ort, Straße)",
          "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 256,
        ],
        [
          "name" => "telefon", "label" => "Telefonnummer", "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "mandatory" => true,
          "length" => 32,
        ],
        [
          "name" => "skype", "label" => "Skypekontakt", "type" => "text",
          "sanitize" => "text_field",
          "length" => 64,
        ],
        [
          "name" => "lektorin",
          "label" => "Name FH-Praxislektor*in", "type" => "text",
          "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
      ],
    ];
    $this->section_orga = [
      "title" => "Einsatzorganisation bzw. vermittelnde Stelle / Person in Österreich / Europa",
      "slug" => "orga",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "name" => "einsatzorga_id", "label" => "Einsatzorganisation 📜",
          "type" => "einsatzorga",
          "options" => Database::get_dropdown_array_einsatzorga(),
          "options_extended" => Database::get_einsatzorgas(),
          "validate" => "options",
          "alternative_fields" => [
            "einsatzorga_other_name" => [ "label" => "Wenn \"Sonstige\", Name der Einsatzorganisation" ],
            "einsatzorga_other_website" => [ "label" => "Wenn \"Sonstige\", Website der Einsatzorganisation" ],
          ],
          "mandatory" => true,
        ],
        [
          "name" => "einsatzorga_kontakt",
          "label" => "Name Kontaktperson der Einsatzorganisation",
          "type" => "text", "sanitize" => "text_field",
          "mandatory" => true,
          "length" => 64,
        ],
        [
          "name" => "einsatzorga_kontakt_email",
          "label" => "E-Mailadresse Kontaktperson der Einsatzorganisation",
          "type" => "text", "sanitize" => "email",
          "mandatory" => true,
          "length" => 128,
        ],
        [
          "name" => "einsatzorga_kontakt_telefon",
          "label" => "Telefon Kontaktperson der Einsatzorganisation",
          "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "length" => 32,
        ],
      ],
    ];
    $this->section_internship = [
      "title" => "Praktikumsstelle",
      "slug" => "internship",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "name" => "praktikum_beginn",
          "label" => "Praktikumsbeginn", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "praktikum_ende",
          "label" => "Praktikumsende", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "praktikum_dauer",
          "label" => "Praktikumsdauer (in Wochen) 📜", "type" => "number",
          "validate" => "regex", "regex" => '/^[0-9]+$/',
          "mandatory" => true,
        ],
        [
          "name" => "ausreise",
          "label" => "Ausreise", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "rueckkehr",
          "label" => "Rückreise", "type" => "date",
          "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
          "format_info" => "YYYY-MM-DD",
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle",
          "label" => "Name (und ggf. Abkürzung) Praktikumsstelle 📜", "type" => "text",
          "sanitize" => "text_field",
          "length" => 64,
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle_website",
          "label" => "Website Praktikumsstelle 📜", "type" => "text",
          "sanitize" => "url",
          "length" => 64,
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle_land",
          "label" => "Land 📜", "type" => "text",
          "sanitize" => "text_field",
          "length" => 64,
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle_adresse",
          "label" => "Adresse Praktikumsstelle", "type" => "text",
          "sanitize" => "text_field",
          "length" => 256,
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle_kontakt",
          "label" => "Name Praxisanleiter*in vor Ort", "type" => "text",
          "sanitize" => "text_field",
          "length" => 64,
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle_kontakt_email",
          "label" => "E-Mailadresse Praxisanleiter*in vor Ort", "type" => "email",
          "sanitize" => "email",
          "length" => 128,
        ],
        [
          "name" => "praktikumsstelle_kontakt_telefon",
          "label" => "Telefon Praxisanleiter*in vor Ort", "type" => "tel",
          "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
          "format_info" => "only numbers and spaces and an optional '+' in the beginning",
          "length" => 32,
        ],
        [
          "name" => "praktikumsstelle_beschreibung",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Beschreibung Praktikumsstelle 📜",
          "explanation" => "Bitte formuliere hier eine Kurzbeschreibung deiner
            Praktikumsstelle auf Basis deiner eigenen Erfahrung/Kenntnis
            (Zielsetzungen, Zielgruppen und Angebote der Einrichtung, sowie z.B.
            Trägerschaft, Finanzierung, personelle Ausstattung, Funktion, etc.)",
          "mandatory" => true,
        ],
        [
          "name" => "praktikumsstelle_sektoren",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Arbeitsbereiche, Tätigkeitsbereiche, Sektoren 📜",
          "explanation" => "In welchen allgemeinen Arbeitsbereichen/Sektoren ist
            die Praktikumstelle tätig? Mögliche Bereiche: • Gesundheit
            • Mädchen- und Frauenrechte • Gender, Intersektionalität
            • Kinder-, Jugend- und Familienarbeit • Senior*innen-Arbeit
            • Sexarbeit • Inklusion von Menschen mit div. Einschränkungen
            • Materielle Grundsicherung • Wohnungslosigkeit
            • Bildung, Ausbildung, Beruf • Politische Bildung
            • Migration, Flucht, Integration • Gemeinwesenarbeit
            • Soziale Transformation • Öko-Soziale Transformation",
          "mandatory" => true,
        ],
        [
          "name" => "anforderungsprofil",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Tätigkeitsbereich / Anforderungsprofil Praktikant*in 📜",
          "explanation" => "Bitte beschreibe hier welche konkreteren Tätigkeiten
            du übernommen hast in der Einrichtung und welches Anforderungsprofil
            damit einhergeht (inkl. Voraussetzung für die erfolgreiche Mitarbeit
            bei diesen Tätigkeiten)",
          "mandatory" => true,
        ],
        [
          "type" => "heading",
          "label" => "Erforderliche Sprachkenntnisse",
        ],
        [
          "type" => "paragraph",
          "content" => "Welche Sprachkenntnisse sind deiner Erfahrung nach für
            eine sinnvolle Praktikumserfahrung erforderlich? (Bitte am ehesten
            Zutreffendes ankreuzen)",
        ],
        [
          "name" => "sprachmindestniveau",
          "label" => "Erforderliche Sprachkenntnisse 📜",
          "explanation" => "Welches Mindestsprachniveau der Amts-/Verbindungssprache
            auf der Skala 1-6 war auf Basis deiner Erfahrung für das
            Praktikum notwendig?",
          "type" => "radio",
          "options" => [
            "Einfache Fragen beantworten" => "Fragen verstehen und sich in einfachen Formulierungen verständlich machen",
            "An Gesprächen beteiligen" => "Sich an Gesprächen zu einfachen Alltagsthemen beteiligen",
            "Komplexe Sachverhalte darstellen" => "Komplexere Sachverhalte aus dem Alltag sprachlich angemessen darstellen",
            "Einfache Texte verfassen" => "Einfach Texte fehlerfrei verfassen",
            "Literatur und Zeitung verstehen" => "Literatur ebenso wie Zeitungen in der Fremdsprache sind mühelos zugänglich",
            "Redewendungen verstehen" => "Redewendungen verstehen und die Fremdsprache auf fast muttersprachlichem Niveau sprechen",
          ],
          "mandatory" => true,
        ],
        [
          "type" => "paragraph",
          "content" => "Welches Sprachniveau hast Du durch welche
            sprachvorbereitenden Maßnahmen in Österreich bzw. vor Ort erlangt?
            Welche Empfehlungen würdest du zukünftigen Praktikant*innen geben -
            insbesondere auch zur Kenntnis der Lokalsprache(n)?",
        ],
        [
          "name" => "sprachniveau_amtssprache",
          "label" => "Amts-/Verbindungssprache 📜", "type" => "text",
          "explanation" => "(eigenes Niveau, Lernmaßnahmen, Empfehlungen)",
          "sanitize" => "text_field",
          "mandatory" => true,
        ],
        [
          "name" => "sprachniveau_lokalsprache",
          "label" => "Lokalsprache 📜", "type" => "text",
          "explanation" => "z.B. Quechua, Swahili, Telugu (eigenes Niveau, Lernmaßnahmen, Empfehlungen)",
          "sanitize" => "text_field",
          "mandatory" => true,
        ],
      ],
    ];
    $this->section_experiences = [
      "title" => "Besondere Erfahrungen und Hinweise",
      "slug" => "experiences",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "type" => "paragraph",
          "content" => "Bitte teile hier persönliche Erfahrungen und Hinweise,
            insbesondere auch für zukünftige Praktikant*innen",
        ],
        [
          "name" => "erfahrungen_allgemein",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Allgemeines - gut zu wissen 📜",
          "explanation" => "z.B. in Bezug auf Vorbereitung, Praktikumsstelle,
            Organisation (Visum o.ä), Sprachkurs, Einleben, medizinische
            Versorgung etc…",
          "mandatory" => true,
        ],
        [
          "name" => "erfahrungen_kommunikation",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Kommunikation 📜",
          "explanation" => "Wie war der Zugang zu Telefon/Internet/PC? Empfehlungen?",
          "mandatory" => true,
        ],
        [
          "name" => "erfahrungen_wohnsituation",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Wohnsituation 📜",
          "explanation" => "Wie bzw. wo hast du gewohnt? War die Wohnsituation
            passend (z.B. einfach/teuer etc.)?",
          "mandatory" => true,
        ],
        [
          "name" => "erfahrungen_weiterbildung",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Weiterbildung vor Ort 📜",
          "explanation" => "Hattest du vor Ort Kontakt mit Hochschulen oder
            Institutionen der Sozialen Arbeit o.Ä.? (Wenn Ja: Name der
            Institution, Art des Kontaktes, Erfahrungen etc.)",
        ],
        [
          "name" => "erfahrungen_sicherheit",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Sicherheit 📜",
          "explanation" => "Wie war dein Gefühl der persönlichen Sicherheit?
            Welche Empfehlungen möchtest du weitergeben?",
          "mandatory" => true,
        ],
        [
          "name" => "erfahrungen_highlight",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Persönliches Highlight 📜",
          "explanation" => "Was war toll an der Praktikumserfahrung?",
          "mandatory" => true,
        ],
        [
          "name" => "erfahrungen_lowlight",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Persönliches Lowlight",
          "explanation" => "Was war nicht so toll an der Praktikumserfahrung?",
        ],
        [
          "name" => "erfahrungen_rueckmeldung",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Persönliche Rückmeldung (wird nicht veröffentlicht!)",
          "explanation" => "Was ich sonst noch mitteilen möchte bzw. rückmelden
            möchte an das GSD Programm?",
        ],
      ],
    ];
    $this->section_costs = [
      "title" => "Kosten",
      "slug" => "costs",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "type" => "paragraph",
          "content" => "Wie hoch waren die Gesamtkosten, die dir durch das
            GSD-Praktikum entstanden sind? (umfasst: die gewöhnliche Wohn-
            und Lebenshaltung im Einsatzland, Impfungen, Versicherungen, Hin-
            und Rückreise, etc. jedoch NICHT private Reisen o.ä.)",
        ],
        [
          "name" => "kosten_gesamt",
          "type" => "number", "sanitize" => "text_field",
          "label" => "Gesamtkosten (in EUR als Ganzzahl) 📜",
          "validate" => "regex", "regex" => "/^[0-9]+$/",
          "mandatory" => true,
        ],
        [
          "name" => "kosten_kommentar",
          "type" => "textarea", "sanitize" => "text_field",
          "label" => "Evtl. Erläuterung zu den Kosten des Praktikums 📜",
        ],
        [
          "name" => "kosten_sprachtraining",
          "type" => "number", "sanitize" => "text_field",
          "label" => "Zusatzkosten Sprachtraining (in EUR als Ganzzahl) 📜",
          "explanation" => "Bitte gib hier die Kosten eines ev. zusätzlichen
            Sprachtrainings (Kurs, Aufenthalt) getrennt von den restlichen
            Gesamtkosten an",
          "validate" => "regex", "regex" => "/^[0-9]+$/",
        ],
      ],
    ];
    $this->section_sustainability = [
      "title" => "Nachhaltigkeit und Engagement in Österreich",
      "slug" => "sustainability",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "name" => "nachhaltigkeit",
          "type" => "textarea", "sanitize" => "textarea_field",
          "label" => "Weitergabe von Erfahrungen",
          "explanation" => "Hast du vor die Erfahrungen aus deinem Praktikum
            weiterzutragen und in Österreich einzubringen? Wenn Ja, in welcher
            Form? (z.B. Engagement, langfristiger Kontakt mit Praktikumsstelle,
            wissenschaftliche Arbeit, Öffentlichkeitsarbeit etc.)",
        ],
      ],
    ];
    $this->section_files = [
      "title" => "Bestätigungen",
      "slug" => "files",
      "submit_label" => "Zwischenspeichern",
      "enctype" => "multipart/form-data",
      "fields" => [
        [
          "name" => "cf_internship",
          "label" => "Praktikumsbestätigung durch Praktikumsstelle",
          "explanation" => "Hiermit bestätigt die Praktikumsstelle mein erfolgtes Praktikum",
          "type" => "file",
          "allowed" => ["jpg", "jpeg", "png", "pdf"],
          "validate" => "file",
        ],
        [
          "name" => "cf_university",
          "label" => "Praktikumsanerkennung durch die FH",
          "explanation" => "Hiermit bestätige ich die Anerkennung durch meine FH
            (z.B. Studienerfolgsnachweis), in dem die Anerkennung meines
            Praktikums sichtbar ist [Ausnahme: Absolvent*innenpraktika]",
          "type" => "file",
          "allowed" => ["jpg", "jpeg", "png", "pdf"],
          "validate" => "file",
        ],
        [
          "name" => "cf_subsidy",
          "label" => "Empfangsbestätigung Zuschuss Praktikumstelle",
          "explanation" => "Hiermit bestätige ich die Übergabe des Zuschusses
            an die Praktikumsstelle",
          "type" => "file",
          "allowed" => ["jpg", "jpeg", "png", "pdf"],
          "validate" => "file",
        ],
      ],
    ];
    $this->section_final = [
      "title" => "Abschluss",
      "slug" => "final",
      "submit_label" => "Zwischenspeichern",
      "fields" => [
        [
          "type" => "paragraph",
          "content" => "Ich habe das Praktikum mit der oben angegebenen
            Dauer (ohne allfälligen Sprachkurs) absolviert und werde, so dies
            noch nicht geschehen ist, am Reflexionsseminar sowie an den
            Aktivitäten meiner österreichischen Einsatzorganisation zur
            Praktikumsnachbereitung teilnehmen.",
        ],
        [
          "name" => "foerdermittel",
          "type" => "number", "sanitize" => "text_field",
          "label" => "Aus öffentlichen Mitteln (Land, Bund), FH/Studiengang
            habe ich erhalten / zugesagt erhalten / erwarte ich insgesamt
            (in EUR als Ganzzahl)",
          "validate" => "regex", "regex" => "/^[0-9]+$/",
        ],
        [
          "name" => "foerderstellen",
          "type" => "textarea", "sanitize" => "text_field",
          "label" => "Aus welchen Förderstellen/-töpfen, wann erwartet, evtl. Schwierigkeiten?",
          "explanation" => "(Etwaige Zuschüsse über Global Social Dialog sind hier NICHT einzurechnen).",
        ],
        [
          "name" => "zuschuss",
          "type" => "radio",
          "label" => "Ich ersuche um Überweisung folgenden Zuschusses",
          "options" => [
            "OEZA-Zuschuss" => "OEZA-Zuschuss",
            "Ergänzeder OEZA-Zuschuss" => "Ergänzeder OEZA-Zuschuss",
          ],
        ],
        [
          "name" => "ueberweisungsdaten",
          "type" => "textarea", "sanitize" => "text_field",
          "label" => "Überweisungsdaten",
        ],
        [
          "name" => "kenntnisnahme",
          "type" => "checkbox", "validate" => "checkbox",
          "label" => "<p>Ich nehme zur Kenntnis, dass sämtliche personenbezogene
            Daten vom GSD-Praktikaprogramm von der Programmträgerin
            FH Campus Wien sowie von Mitarbeiter*innen meiner Fachhochschule
            für die Zwecke der Programmverwaltung, Evaluierung und Vernetzung
            automationsunterstützt für 10 Jahre gespeichert, verarbeitet und
            verwendet werden können, sowie zum Zweck der Projektabrechnung der
            Fördergeberin übermittelt werden.</p>

            <p>Ich nehme weiters zur Kenntnis, dass (sofern ich dem in der
            Fixanmeldung zugestimmt habe) meine Daten zum Zwecke der
            Übermittlung von Programmneuigkeiten und Veranstaltungseinladungen
            per E-Mail sowie zur Vernetzung von Alumni*ae auch über die 10 Jahre
            hinaus gespeichert werden dürfen und zum Zweck der
            Informationsweitergabe vereinzelt an nachfolgende Praktikant*innen
            übermittelt werden können (durch die Programmverantwortlichen).</p>

            Weiters nehme ich zur Kenntnis, dass mein Teilnahmebericht im Rahmen
            der Rückkehrmeldung anonymisiert auf der Website
            globalsocialdialog.at veröffentlicht wird.",
          "mandatory" => true,
        ],
        [
          "type" => "paragraph",
          "content" => "Dort, wo meine Einwilligung in die Datenverarbeitung
            erforderlich ist, habe ich das Recht, meine Einwilligung jederzeit
            (mündlich oder schriftlich) gegenüber der FH Campus Wien und/oder
            meiner Fachhochschule zu widerrufen. Der Widerruf meiner
            Einwilligung bezieht sich nicht auf Daten, die bis zu meinem
            Widerruf bereits verarbeitet worden sind.
            Details zur Datenschutzerklärung sind auch unter:
            globalsocialdialog.at/datenschutz zu finden.",
        ],
        [
          "type" => "paragraph",
          "content" => "<b>Bitte gib die (mit Unterschrift und Stempel)
            unterzeichnete „Empfangsbestätigung Zuschuss Praktikumsstelle“
            (ORIGINAL), die unterzeichnete Praktikumsbestätigung (KOPIE), die
            Anerkennung des Praktikums durch die FH (z.b. Studienerfolgsnachweis)
            (KOPIE), sowie die Zusatzvereinbarung für Ausreisen im COVID-Kontext
            (ORIGINAL) und einen unterschriebenen Ausdruck dieses
            Rückkehrmelde-Datenblatts beim Reflexionsseminar ab!</b>",
        ],
        [
          "type" => "paragraph",
          "content" => "Wir freuen uns auf das gemeinsame Reflexionsseminar!",
        ],
      ],
    ];
  }

  /**
   * main static function that is called through the shorttag.
   * it initialises the pre registration form and hands over controll to the
   * controller
   */
  public static function display()
  {
    $html = "";

    if ( ! is_user_logged_in() ) {
      $html .= Notification::error([
        "message" => 'Du bist nicht eingelogged. Dieser Bereich ist nur mit deinem
        GSD-Account nutzbar.'
      ]);
      $html .= '<p align="center"><a href="/wp-login.php">Zum Login</a></p>';
      return $html;
    }

    global $wpdb;
    $user = wp_get_current_user();
    $q = "SELECT firstname, lastname, email, adresse, telefon, skype, matrikelnr,
      hochschule_id, lektorin, einsatzorga_id, einsatzorga_other_name,
      einsatzorga_other_website, einsatzorga_kontakt, einsatzorga_kontakt_email,
      einsatzorga_kontakt_telefon, praktikum_beginn, praktikum_ende,
      praktikum_dauer, ausreise, rueckkehr, praktikumsstelle,
      praktikumsstelle_website, einsatzland AS praktikumsstelle_land,
      praktikumsstelle_adresse, praktikumsstelle_kontakt,
      praktikumsstelle_kontakt_email, praktikumsstelle_kontakt_telefon,
      praktikumsstelle_beschreibung,
      id AS teilnehmerin_id,
      status
      FROM {$wpdb->prefix}".GSD_TABLE_TEILNEHMERIN ." WHERE matrikelnr = %s";
    $q = $wpdb->prepare( $q, $user->data->user_login );
    $prefill = $wpdb->get_row( $q, ARRAY_A );

    if ( empty( $prefill ) ) {
      if ( current_user_can("manage_options") ) {
        $html .= Notification::warning([
          "message" => "Du bist als Administrator*in angemeldet. Zum Bearbeiten
            einzelner Teilnehmer*innen verwende bitte das Backend."
        ]);
      } else {
        $html .= Notification::error([
          "message" => 'Es sind keine Daten in der GSD Datenbank zu deinem Account vorhanden!<br>
          Bitte kontaktiere umgehend die Projektadministration!'
        ]);
      }
      return $html;
    }

    // user has to be fully registered to fill out the return form
    if( $prefill["status"] < 2 ) {
      $html .= Notification::error([
        "message" => 'Du befindest dich noch im Voranmeldungsprozess. Das
        Rückkehr-Datenblatt soll erst nach dem Praktikum ausgefüllt werden.'
      ]);
      $html .= '<p align="center"><a href="/wp-login.php">Zum Login</a></p>';
      return $html;
    }

    // add/overwrite all entries, that are already set in the return form table
    // as well as entries only available in the return form
    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_RETURN_FORM .
      " WHERE matrikelnr = %s";
    $q = $wpdb->prepare( $q, $user->data->user_login );
    $return_data = $wpdb->get_row( $q, ARRAY_A );
    if( $return_data ) {
      foreach ( $return_data as $key => $value ) {
        if ( !isset($prefill[$key]) ) {
          $prefill[$key] = $value;
        } elseif ( $value !== 0 and !empty($value) ) {
          $prefill[$key] = $value;
        }
      }
    }

    $return_form = new Return_Form( $prefill );
    if( $return_data ) {
      $return_form->return_data_available = true;
    } else {
      $return_form->return_data_available = false;
    }
    $html .= $return_form->controller();

    return $html;
  }

  /**
   * this is the main entry into the form's processing, which should be called
   * by the static display function, right after it has instatiated the full_reg
   * object
   */
  public function controller () {
    $html = '';

    // should we present and edit form rather then the overview?
    if ( isset( $_GET["edit"] ) ) {
      $edit = true;
      switch( $_GET["edit"] ) {
        case "person":
          $html .= $this->handle_section( $this->section_person, $this->participant );
          return $html;

        case "orga":
          $html .= $this->handle_section( $this->section_orga, $this->participant );
          return $html;

        case "internship":
          $html .= $this->handle_section( $this->section_internship, $this->participant );
          return $html;

        case "experiences":
          $html .= $this->handle_section( $this->section_experiences, $this->participant );
          return $html;

        case "costs":
          $html .= $this->handle_section( $this->section_costs, $this->participant );
          return $html;

        case "sustainability":
          $html .= $this->handle_section( $this->section_sustainability, $this->participant );
          return $html;

        case "files":
          $html .= $this->handle_section( $this->section_files, $this->files );
          return $html;

        case "final":
          $html .= $this->handle_section( $this->section_final, $this->participant );
          return $html;

        default:
          $html .= Notification::error([
            "message" => "The chosen section is not editable!"
          ]);
          return $html;
      }
    } elseif ( isset( $_GET["submit-return-data"] ) ) {
      if ( $this->missing_info() ) {
        $html .= Notification::error([
          "message" => "Formular-Daten unvollständig!<br>
          Bitte notiere die aktuelle Zeit und kontaktiere mit dieser Information
          umgehend die Projektleitung."
        ]);
        return $html;
      }
      if ( $this->complete_submission() !== 1 ) {
        $html .= Notification::error([
          "message" => "Datenbank-Fehler beim Einreichen der Formular-Daten!<br>
          Bitte notiere die aktuelle Zeit und kontaktiere mit dieser Information
          umgehend die Projektleitung."
        ]);
        return $html;
      }
      $html .= $this->render_submission_success();
      return $html;
    }

    $html .= $this->render_intro();
    $html .= $this->render_section( $this->section_person, $this->participant );
    $html .= $this->render_section( $this->section_orga, $this->participant );
    $html .= $this->render_section( $this->section_internship, $this->participant );
    $html .= $this->render_section( $this->section_experiences, $this->participant );
    $html .= $this->render_section( $this->section_costs, $this->participant );
    $html .= $this->render_section( $this->section_sustainability, $this->participant );
    $html .= $this->render_section( $this->section_files, $this->files );
    $html .= $this->render_section( $this->section_final, $this->participant );

    // datasheet print button should only be available if the return form has been
    // saved at least once (otherwise there is no return data set yet)
    if( $this->return_data_available ) {
      $html .= '
      <div class="data-section print-button">
      <a href="'.plugin_dir_url(__FILE__).'print-return-datasheet.php" target="_blank">
      <span class="dashicons dashicons-printer"></span> Rückkehrdatenblatt drucken
      </a>
      </div>
      ';
    }

    // submission of return form, only if necessary data was provided
    switch( $this->participant["status"] ) {
      case 2: // fully registered, return form not yet submitted
        $missing = $this->missing_info();
        if ( ! $missing ) {
          $html .= '
          <div class="data-section print-button">
          <a href="?submit-return-data=true">
          <span class="dashicons dashicons-upload"></span> Datenblatt speichern und einreichen
          </a>
          </div>
          ';
        } else {
          $html .= '
          <div class="warning">
            <span class="dashicons dashicons-bell"></span> <b>Datenblatt noch nicht einreichbar</b><br>
            <p>
              Es fehlen noch folgende Daten um das Rückkehr-Datenblatt einreichen zu können:
            </p>
            <ul>';
          foreach( $missing as $fieldname ) {
            $html .= '
              <li>'.$fieldname.'</li>';
          }
          $html .= '
            </ul>
          </div>
          ';
        }
        break;

      case 21: // already submitted return form
        $html .= '
        <div class="data-section status-info">
          Dein Rückkehrdatenblatt ist bereits eingereicht und wird von der
          Projektleitung geprüft.
        </div>
        ';
        break;

      case 3: // cancelled, no data submission possible
        $html .= '
        <div class="data-section status-info">
          Dein GSD-Praktikum wurde abgebrochen.
        </div>
        ';
        break;
    }

    return $html;
  }

  private function missing_info() {
    // TODO: refactor for DRYness!
    //   apart from the section config and emergency contacts, this function a
    //   duplicate of the Full_Registration::missing_info function

    $missing = [];
    $sections = [
      $this->section_person, $this->section_orga, $this->section_internship,
      $this->section_experiences, $this->section_costs,
      $this->section_sustainability, $this->section_final,
    ];
    foreach ( $sections as $section ) {
      foreach ( $section["fields"] as $field ) {
        if ( isset($field["name"]) && isset($field["mandatory"]) ) {
          if ( !isset($this->participant[$field["name"]]) ) {
            $missing[] = $field["name"];
          } elseif ( empty($this->participant[$field["name"]]) ) {
            // values explicitly set to false (:= 0) (e.g. in checkboxes) are ok
            if ( $this->participant[$field["name"]] != "0" ) {
              $missing[] = $field["name"];
            }
          }
        }
      }
    }
    // check if all needed files are uploaded
    $files_needed = ['cf_internship', 'cf_university', 'cf_subsidy'];
    foreach ( $this->files as $file ) {
      if ( in_array($file['type'], $files_needed) ) {
        $i = array_search($file['type'], $files_needed);
        unset($files_needed[$i]);
      }
    }
    foreach ( $files_needed as $f ) {
      $missing[] = "Datei: $f";
    }
    // for the organisation we either need einsatzorga_id or name and website
    if ( ! empty($missing) ) {
      if ( in_array("einsatzorga_id", $missing) ) {
        // if einsatzorga_id is missing, we have to make sure we have the other two
        // only then we can remove einsatzorga_id
        if (
          !in_array("einsatzorga_other_name", $missing) &&
          !in_array("einsatzorga_other_website", $missing)
        ) {
          $i = array_search("einsatzorga_id", $missing);
          unset($missing[$i]);
        }
      } else {
        // when einsatzorga_id is available we can remove the others (if the exist)
        if (($i = array_search("einsatzorga_other_name", $missing)) !== false) {
          unset($missing[$i]);
        }
        if (($i = array_search("einsatzorga_other_website", $missing)) !== false) {
          unset($missing[$i]);
        }
      }
    }
    return $missing;
  }

  private function handle_section( $section, $data ) {
    $html = '';

    $form = new Form( $section );
    // we'll load the whole data set into the fixed data section, in case
    // any form field is included that is not editable but should be displayed
    // with the value already set in the DB
    $form->load_fixed_data( $data );

    // if post data was already submitted, we'll process it
    if ( isset( $_POST[ $section["slug"] ] ) ) {
      if ( ! $form->validate_nonce( $_POST ) ) {
        $html .= Notification::nonce();
        return $html;
      }
      $form->load_data( $_POST );
      // check if there have been any errors with the submitted data
      if ( ! empty( $form->errors ) ) {
        if ( ! empty( $form->errors["missing"] ) ) {
          $html .= Notification::field_issues(
            "Folgende Pflichtfelder sind nicht (oder falsch) ausgefüllt worden:",
            $form->errors["missing"],
          );
        }
        if ( ! empty( $form->errors["validation"] ) ) {
          $html .= Notification::field_issues(
            "Folgende Felder enthalten noch ungültige Zeichen:",
            $form->errors["validation"],
          );
        }
      }
      // if the form validation detected no errors, we can store the data
      else {
        // if nothing was updated we'll receive 0, otherwise 1 or more.
        // but in case of an error we'll receive false
        $retval = $this->store( $section, $form->get_submitted_data() );
        if ( $retval === false ) {
          return Notification::error([
            "message" => "Fehler beim Schreiben der Voranmeldungsdaten in die Datenbank!<br>
            Bitte notiere die aktuelle Zeit und kontaktiere mit dieser Information
            umgehend die Projektleitung."
          ]);
        } else if ( $retval === 0 ) {
          $html .= Notification::warning([
            "message" => "Daten wurden nicht verändert. (Leere Felder werden nicht aktualisiert!)"
          ]);
        } else {
          // When the registration was successfull, we render a corresponding
          // message and return, because we don't need the registration form
          $html .= $this->render_success();
          return $html;
        }
      }
    }

    // if nothing was yet submitted, we have to still load data from the db
    else {
      $form->load_data( $data );
    }

    // render the form and return the full html output
    $html .= $form->render();
    return $html;
  }

  private function complete_submission () {
    global $wpdb;
    $table = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $where = [ "matrikelnr" => $this->participant["matrikelnr"] ];
    $update_data = ["status" => 21];
    return $wpdb->update( $table, $update_data, $where );
  }

  private function store ( $section, $data ) {
    global $wpdb;

    // empty array of column names and values to be updated
    $update_data = [];

    // those fields can be directly taken from the validated
    // form data, if they are not empty
    $take_if_not_empty = [];
    foreach ( $section["fields"] as $field ) {
      if ( $field["type"] == "section" ) {
        foreach ( $field["fields"] as $f ) {
          // only named fields should be stored
          if ( isset( $f["name"] ) ) {
            $take_if_not_empty[] = $f["name"];
          }
        }
      } else {
        // only named fields should be stored
        if ( isset( $field["name"] ) ) {
          $take_if_not_empty[] = $field["name"];
        }
      }
    }
    foreach ( $take_if_not_empty as $key ) {
      if ( isset( $data[$key] ) && $data[$key] !== '' ) {
        $update_data[$key] = $data[$key];
      }
    }

    // if all fields are empty, nothing gets updated
    if ( empty( $update_data ) ) {
      return 0;
    }

    // in case we are handling file uploads
    else if ( isset($_POST["files"]) ) {
      $update_count = 0;
      foreach ( $update_data as $key => $file ) {
        // create the target filename and move the uploaded file
        $target_base = GSD_UPLOAD_DIR . '/';
        $target_file = $this->participant["matrikelnr"] . '_' .
          $key . '_' . date('Y-m-d_H-i-s') . '_' . $file["name"];
        move_uploaded_file( $file["tmp_name"], $target_base.$target_file );

        // check if there is already an existing entry
        $updating = false;
        foreach ( $this->files as $f ) {
          if ( $f["type"] == $key ) {
            $updating = true;
          }
        }

        // now insert/update the file entry in the databse
        $table = $wpdb->prefix . GSD_TABLE_FILE;
        $where = [
          "teilnehmerin_id" => $this->participant["teilnehmerin_id"],
          "type" => $key,
        ];
        $values = [
          "teilnehmerin_id" => $this->participant["teilnehmerin_id"],
          "type" => $key,
          "filepath" => $target_file,
          "updated" => date( "c" ),
        ];
        if ( $updating ) {
          $num = $wpdb->update( $table, $values, $where );
        } else {
          $num = $wpdb->insert( $table, $values );
        }
        if ( $num === false ) {
          $wpdb->print_error(__FILE__.':'.__LINE__);
          return false;
        }
        $update_count += $num;
      }
      return $update_count;
    }

    // handling the main return form data
    else {
      // in case the form handles the einsatzoga we have to check if it was
      // chosen from the dropdown or alternative text was provided
      if ( isset( $data["einsatzorga_id"] ) && $data["einsatzorga_id"] == "OTHER" ) {
        $update_data["einsatzorga_id"] = null;
        // TODO: this is hardcoded now due to time availability; should be automated in improved version
        if ( ! empty( $data["einsatzorga_other_name"] ) ) $update_data["einsatzorga_other_name"] = $data["einsatzorga_other_name"];
        if ( ! empty( $data["einsatzorga_other_website"] ) ) $update_data["einsatzorga_other_website"] = $data["einsatzorga_other_website"];
      }

      // we want to know if the name of the internship organisation has changed
      // vs. the one provided in the full registration, and set a flag if so
      if ( isset( $update_data["praktikumsstelle"] ) ) {
        $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
        $q = "SELECT praktikumsstelle FROM $tbl_teilnehmerin WHERE matrikelnr = %s";
        $q = $wpdb->prepare( $q, $this->participant["matrikelnr"] );
        $return_data = $wpdb->get_row( $q, ARRAY_A );
        $update_data["praktikumsstelle_aenderung"] = 0;
        if ( $return_data["praktikumsstelle"] !== $update_data["praktikumsstelle"] ) {
          $update_data["praktikumsstelle_aenderung"] = 1;
        }
      }

      // before we can update the database, we have to check if the return form
      // was already stored once
      $q = "SELECT id FROM {$wpdb->prefix}".GSD_TABLE_RETURN_FORM .
        " WHERE matrikelnr = %s";
      $q = $wpdb->prepare( $q, $this->participant["matrikelnr"] );
      $return_data = $wpdb->get_row( $q, ARRAY_A );

      // now either perform an update or an insert
      $table = $wpdb->prefix . GSD_TABLE_RETURN_FORM;
      if ( $return_data ) {
        $where = [ "id" => $return_data["id"] ];
        $update_data["updated"] = date( "c" );
        return $wpdb->update($table, $update_data, $where );
      } else {
        // on insert of a new return data set we have to make sure to include
        // all prefilled data, not only the data from the current section
        foreach ( $this->participant as $key => $value ) {
          // but do not store the status, which is from the main table
          if ( $key == "status" ) continue;
          if ( empty($update_data[$key]) ) {
            $update_data[$key] = $value;
          }
        }
        $update_data["created"] = date( "c" );
        return $wpdb->insert($table, $update_data );
      }
    }
  }

  private function render_success () {
    $html = Notification::success([
      "message" => "Daten aktualisiert."
    ]);
    $query_string = $_SERVER["QUERY_STRING"];
    // remove the edit parameter from the query string
    // in case it is not the first parameter:
    $query_string = preg_replace( '/\\&edit=[a-z_\-]+/', '', $query_string );
    // in case it is the first parameter
    $query_string = preg_replace( '/^edit=[a-z_\-]+[\\&]?/', '', $query_string );
    // in case it is the only parameter
    $query_string = preg_replace( '/^edit=person$/', '', $query_string );
    $html .= '<a href="?'.$query_string.'">Zurück zur Übersicht</a>';
    return $html;
  }

  private function render_submission_success () {
    // create mail notification for admins
    $name = "{$this->participant["firstname"]} {$this->participant["lastname"]}";
    $matrikelnr = "{$this->participant["matrikelnr"]}";
    $admin_url = get_admin_url();
    $msg = "
Hello schnello yet again,

**$name** (Matrikelnummer **$matrikelnr**) hat gerede deren
Rückkehr-Daten eingereicht. Um die Daten zu prüfen gehe zur
GSD-Datenbank-Übersicht:

  $admin_url

liebste grüße,
automagia
    ";
    $admins = get_users([ "role" => "Administrator" ]);
    foreach ( $admins as $admin ) {
      wp_mail($admin->data->user_email, "GSD-Rückkehr-Formular eingereicht", $msg);
    }

    // render notification for website user
    $html = Notification::success([
      "message" => "Daten erfolgreich eingereicht."
    ]) . '
      <p>
        Danke für die Einreichung der Rückkehrmeldung!
      </p>
      <p align="center">
        <a href="?">Zurück zum Formular</a>
      </p>
    ';
    return $html;
  }

  private function render_intro () {
    $html = '
    <div class="data-section">
      <h3 class="section-title">Einleitung</h3>
      <div class="fixed">
        <div class="label">
          Schön, dass du wieder da bist!
        </div>
        <div class="value">
          <ul>
            <li>Die folgende Rückkehrmeldung dient dem Bericht von Global Social
              Dialog gegenüber der Fördergeberin (Austrian Development Agency)
              und der Dokumentation. Antworten auf die mit 📜 markierten Fragen
              können anonymisiert auf der Website https://globalsocialdialog.at/
              veröffentlicht werden und dienen nachfolgenden Studierenden als
              Erstinformation über deine Praktikumsstelle bzw. -land.</li>
            <li>Um die Rückkehr-Meldung abzuschließen, klicke am Ende dieser
              Übersichtsseite auf "Datenblatt speichern und einreichen" (der
              Button ist verfügbar, sobald du die Pflichtfelder in allen
              Formularabschnitten ausgefüllt hast). Die Rückkehr-Meldung kann
              nach dem Abspeichern und Einreichen jederzeit überarbeitet
              werden.</li>
            <li>Das Rückkehrmelde-Datenblatt muss als Ausdruck zum
              Reflexionsseminar mitgebracht werden und im Rahmen des Seminares
              unterschrieben werden.</li>
          </ul>
        </div>
      </div>
    </div>
    ';
    return $html;
  }

  public function render_section ( $config, $data ) {
    $html = '
    <div class="data-section">
      <h3 class="section-title">' . esc_html($config["title"]) . '</h3>';

    // first we'll display a section with non-editable fields (if any)
    if( ! empty( $config["fixed_fields"] ) ) {
      $html .= $this->render_section_fields( $config["slug"], false, $config["fixed_fields"], $data );
    }

    // then a section with editable fields (if any) follows
    if( ! empty( $config["fields"] ) ) {
      $html .= $this->render_section_fields( $config["slug"], true, $config["fields"], $data );
    }

    // close the data section div and return the generated html
    $html .= '
    </div> <!-- data-section -->
    ';
    return $html;
  }

  private function render_section_fields ( $slug, $editable, $fields, $data, $recurse=false ) {
    $html = '';

    // the opening tags (and optional edit link) should only be added, if we
    // are not inside a subsection recursion
    if ( ! $recurse ) {
      $css_class = $editable ? 'editable' : 'fixed';
      if ( empty( $_SERVER["QUERY_STRING"] ) ) {
        $edit_link = "edit=" . $slug;
      } else {
        $edit_link = $_SERVER["QUERY_STRING"] . "&edit=" . $slug;
      }
      $html .= '
      <div class="' . $css_class . '">
      ';
      if ( $editable ) {
        $html .= '<a class="button" href="?'.$edit_link.'"><span class="dashicons dashicons-edit"></span></a>';
      }
    }

    // now process the fields
    foreach ( $fields as $field ) {
      // if we hit a (sub)section, we'll add a heading and recurse
      if ( $field["type"] == "section" ) {
        $html .= '<h4 class="section-title">' . $field["header"] . '</h4>
        ' . $this->render_section_fields( $slug, $editable, $field["fields"], $data, true );
      }

      // this is the standard output, if the type is not a section
      else {
        $html .= $this->render_field( $field, $data );
      }
    }

    // again, closing div only if not in recursion
    if ( ! $recurse ) {
      $html .= '
      </div> <!-- ' . $css_class . ' -->';
    }

    return $html;
  }

  private function render_field ( $field, $data ) {
    if ( empty( $field["options_extended"] ) ) {
      $options = empty( $field["options"] ) ? false : $field["options"];
    } else {
      $options = $field["options_extended"];
    }
    return Form::render_field( $field, $data, $options );
  }

}
