<?php
namespace GSDDB;

class Plugin {
  public function init () {
    require_once plugin_dir_path( __FILE__ ) . 'class-pre-registration.php';
    add_shortcode( 'gsd_voranmeldung', 'GSDDB\Pre_Registration::display' );

    require_once plugin_dir_path( __FILE__ ) . 'class-full-registration.php';
    add_shortcode( 'gsd_fixanmeldung', 'GSDDB\Full_Registration::display' );

    require_once plugin_dir_path( __FILE__ ) . 'class-return-form.php';
    add_shortcode( 'gsd_rueckmeldung', 'GSDDB\Return_Form::display' );

    add_action ( 'admin_menu', 'GSDDB\Plugin::add_admin_menu' );

    add_action ( 'wp_enqueue_scripts', 'GSDDB\Plugin::load_frontend_styles' );
    add_action ( 'admin_enqueue_scripts', 'GSDDB\Plugin::load_backend_styles' );

    add_filter ( 'login_redirect', 'GSDDB\Plugin::redirect_login', 10, 3 );

    add_action ( 'init', 'GSDDB\Plugin::add_lecturer_role', 11 );
  }

  public static function add_lecturer_role () {
    $subscriber = get_role('subscriber');
    $cap = get_role('subscriber')->capabilities;
    $cap['gsd_lecturer_read'] = 1;
    $lecturer = add_role('lecturer', 'Lektor*in', $cap);
  }

  public static function redirect_login ( $redirect_to, $request, $user ) {
    // this snippet is by Niels Lange, found at
    // https://developer.wordpress.org/reference/hooks/login_redirect/
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
      //check for admins
      if (in_array('administrator', $user->roles) || in_array('lecturer', $user->roles)) {
        // redirect them to the default place
        return $redirect_to;
      } else {
        return home_url();
      }
    } else {
      return $redirect_to;
    }
  }

  public static function load_frontend_styles ()
  {
    wp_enqueue_style( 'gsd_frontend', plugin_dir_url( __FILE__ ) . '../assets/css/frontend.css' );
    wp_enqueue_style( 'dashicons' );
  }

  public static function load_backend_styles ()
  {
    wp_enqueue_style( 'gsd_backend', plugin_dir_url( __FILE__ ) . '../assets/css/backend.css' );
  }

  public static function add_admin_menu ()
  {
    require_once plugin_dir_path( __FILE__ ) . 'admin/class-overview.php';
    require_once plugin_dir_path( __FILE__ ) . 'admin/class-overview-returners.php';
    require_once plugin_dir_path( __FILE__ ) . 'admin/class-settings.php';

    add_menu_page( 'GSD Datenbank Übersicht', 'GSD Datenbank',
      'manage_options', 'gsd_menu', 'GSDDB\Admin\Overview::render',
      'dashicons-database', 99);

    add_submenu_page( 'gsd_menu', 'GSD Rückkehrmeldungen', 'Rückkehrmeldungen',
      'manage_options', 'gsd_menu_returners', 'GSDDB\Admin\Overview_Returners::render');

    add_submenu_page( 'gsd_menu', 'GSD Einstellungen', 'Einstellungen',
      'manage_options', 'gsd_menu_settings', 'GSDDB\Admin\Settings::render');

    add_submenu_page( 'gsd_menu', 'GSD Lektor*innen-Ansicht', 'Lektor*innen-Ansicht',
      'gsd_lecturer_read', 'gsd_menu_lecturer', 'GSDDB\Admin\Overview::render');

  }

  public static function render_error($msg, $log_msg=false, $file=false, $line=false) {
    if ($log_msg) {
      $m = '[GSD error]';
      if ( $file ) {
        $m .= ' '.$file;
      }
      if ( $line ) {
        $m .= $line;
      }
      $m .= ': ' . $log_msg;
      error_log($m);
    }
    $html = '<div class="error">'.$msg.'</div>';
    return $html;
  }

}
