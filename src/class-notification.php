<?php
namespace GSDDB;

class Notification
{
  public static function field_issues ( $message, $fields ) {
    $html = '
      <div class="error">
        <span class="dashicons dashicons-warning"></span> <b>Fehler:</b><br>
        ' . $message . '
        <ul>';
    foreach ( $fields as $key => $label ) {
      $html .= '
          <li>'.$label.'</li>';
    }
    $html .= '
        </ul>
      </div>';
    return $html;
  }

  public static function nonce () {
    error_log( '[GSD error]: Invalid nonce! POST data was: '.json_encode($_POST) );
    return Notification::error([
      "message" => "Invalid nonce! Sneaky you?!?<br>
      If you don't know what that means, it means that either somebody
      is hacking you, or something is really broken with the website.
      In any case, please contact us immediately."
    ]);
  }

  public static function error ( $error ) {
    $html = '
      <div class="error">
        <span class="dashicons dashicons-warning"></span> <b>Fehler:</b><br>
        ' . $error["message"] . '
      </div>';
    return $html;
  }

  public static function warning ( $warn ) {
    $html = '
      <div class="warning">
        <span class="dashicons dashicons-bell"></span> <b>Achtung:</b><br>
        ' . $warn["message"] . '
      </div>';
    return $html;
  }

  public static function success ( $info ) {
    $html = '
    <div class="success">
      <span class="dashicons dashicons-yes"></span>' . $info["message"] . '
    </div>';
    return $html;
  }
}
