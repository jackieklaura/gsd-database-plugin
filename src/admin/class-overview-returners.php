<?php
namespace GSDDB\Admin;

class Overview_Returners
{
  private $schools = [];
  private $orgas = [];
  private $cohorts = [];
  public $active_cohort;

  public function __construct () {
    global $wpdb;

    // initialise schools
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_HOCHSCHULE);
    foreach ( $r as $row ) {
      $this->schools[ $row->id ] = $row->name;
    }

    // initialise organisations
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_EINSATZORGA);
    foreach ( $r as $row ) {
      $this->orgas[ $row->id ] = [
        "name" => $row->name,
        "website" => $row->website
      ];
    }

    // initialise cohorts
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_KOHORTE);
    foreach ( $r as $row ) {
      $this->cohorts[ $row->id ] = $row->label;
    }
  }

  public static function render () {

    global $wpdb;
    $v = new Overview_Returners();

    // check if a specific cohort is alreay choosen, otherwise take the first available
    if ( ! empty( $_GET["kohorte"] ) ) {
      if ( array_search( $_GET["kohorte"], array_keys( $v->cohorts ) ) !== false ) {
        $v->active_cohort = $_GET["kohorte"];
      } else {
        $v->active_cohort = array_key_first( $v->cohorts );
      }
    } else {
      $v->active_cohort = array_key_first( $v->cohorts );
    }

    ?>
    <div class="wrap">
      <h1>GSD Datenbank: Rückkehrmeldungen</h1>

      <div class="cohort-selector">
        <form method="GET" action="">
          Aktuell ausgewählte Kohorte:
          <span class="currently-active"><?= $v->cohorts[$v->active_cohort] ?></span>
          <input type="hidden" name="page" value="gsd_menu">
          Umschalten auf:
          <?php
          foreach( $v->cohorts as $key => $label ) {
            if ( $key != $v->active_cohort ) {
              $uri = $_SERVER["SCRIPT_NAME"] . '?page=' . $_GET["page"] . '&kohorte=' . $key;
              echo '<a class="button" href="' . $uri . '">' . $label . '</a> ';
            }
          }
          ?>
          <!--
          Auf
          <select name="kohorte">
            <?php foreach( $v->cohorts as $key => $label ) : ?>
              <?php $selected = $key == $v->active_cohort ? ' selected="selected"' : ''; ?>
              <option value="<?= $key ?>"<?= $selected ?>><?= $label ?></option>
            <?php endforeach; ?>
          </select>
          <input class="button action" type="submit" value="umschalten">
          -->
        </form>
      </div>
    <?php

    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $tbl_returners = $wpdb->prefix . GSD_TABLE_RETURN_FORM;
    $q = "SELECT rt.*, tn.datenverarbeitung, tn.status FROM $tbl_returners AS rt
            LEFT JOIN $tbl_teilnehmerin AS tn
            ON rt.teilnehmerin_id = tn.id
            WHERE tn.kohorte_id = %d AND tn.status = 2";
    $q = $wpdb->prepare( $q, $v->active_cohort );
    $participants = $wpdb->get_results( $q );

    $q = "SELECT rt.*, tn.datenverarbeitung, tn.status FROM $tbl_returners AS rt
            LEFT JOIN $tbl_teilnehmerin AS tn
            ON rt.teilnehmerin_id = tn.id
            WHERE tn.kohorte_id = %d AND tn.status = 21";
    $q = $wpdb->prepare( $q, $v->active_cohort );
    $returners = $wpdb->get_results( $q );

    if( ! empty($participants) ) {
      $v->render_participants($participants, "participants");
    }
    if( ! empty($returners) ) {
      $v->render_participants($returners, "returners");
    }
    if ( empty($participants) && empty($returners) ) {
      ?>
      <div class="notice">Es sind noch keine Rückkehrmeldungen für diese Kohorte eingegangen!</div>
      <?php
    } else {
      ?>
      <h2>Mail-Adressat*innen-Kreise:</h2>
      <ul>
        <?php
        $addresses = '';
        foreach ( $participants as $person ) {
          $addresses .= $person->email . ';';
        }
        foreach ( $returners as $person ) {
          $addresses .= $person->email . ';';
        }
        // remove trailing ';'
        $addresses = substr( $addresses, 0, -1 );
        ?>
        <li>
          <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?> mit Rückkehrmeldung">Alle</a> &nbsp;&nbsp;&nbsp;
          [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?> mit Rückkehrmeldung">in CC</a> ] &nbsp;&nbsp;&nbsp;
          [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?> mit Rückkehrmeldung">in BCC</a> ]
        </li>

        <?php
        foreach ( $v->schools as $school_id => $school_name ) {
          $q = "SELECT rt.email FROM $tbl_returners AS rt
                  JOIN $tbl_teilnehmerin AS tn
                  ON rt.teilnehmerin_id = tn.id
                  WHERE tn.hochschule_id = %d AND tn.kohorte_id = %d";
          $q = $wpdb->prepare( $q, $school_id, $v->active_cohort );
          $result = $wpdb->get_results( $q );
          if ( $result ) {
            $addresses = '';
            foreach ( $result as $person ) {
              $addresses .= $person->email . ';';
            }
            // remove trailing ','
            $addresses = substr( $addresses, 0, -1 );
            ?>
            <li>
              <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle an der <?= esc_attr($school_name) ?> in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?> mit Rückkehrmeldung"><?= esc_html($school_name) ?></a> &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle an der <?= esc_attr($school_name) ?> in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?> mit Rückkehrmeldung">in CC</a> ] &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle an der <?= esc_attr($school_name) ?> in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?> mit Rückkehrmeldung">in BCC</a> ]
            </li>
            <?php
          }
        }
        ?>
      </ul>
      <h2>Exportieren in CSV Files:</h2>
      <ul>
        <li><a href="/wp-content/plugins/gsd-db/export-returners.php?set=all&kohorte=<?= $v->active_cohort ?>">Alle Rückkehr-Datensätze in Kohorte</a></li>
      </ul>
      <?php
    }
    ?>
    </div>
    <?php
  }

  public function render_error($msg, $log_msg=false, $file=false, $line=false) {
    if ($log_msg) {
      $m = '[GSD error]';
      if ( $file ) {
        $m .= ' '.$file;
      }
      if ( $line ) {
        $m .= $line;
      }
      $m .= ': ' . $log_msg;
      error_log($m);
    }
    $html = '<div class="error">'.$msg.'</div>';
    return $html;
  }

  public function get_files_for_participant ( $id ) {
    global $wpdb;
    $tbl_file = $wpdb->prefix . GSD_TABLE_FILE;
    $query = $wpdb->prepare( "SELECT * FROM $tbl_file WHERE teilnehmerin_id = %d", $id );
    return $wpdb->get_results( $query );
  }

  public function get_file_by_type ( $type, $files ) {
    foreach ( $files as $file ) {
      if ( $file->type === $type ) return $file;
    }
    return false;
  }

  public function render_participants ($rows, $filter="unapproved") {
    if ( $filter == "participants" ) {
      ?>
      <h2>Teilnehmer*innen mit noch nicht eingereichten Rückkehrmelde-Daten:</h2>
      <?php
    } else if ( $filter == "returners" ) {
      ?>
      <h2>Teilnehmer*innen mit eingereichten Rückkehrmelde-Daten:</h2>
      <?php
    }
    ?>
    <table class="widefat">
      <thead>
        <tr>
          <th>Matrikelnr</th>
          <th>Vorname</th>
          <th>Nachname</th>
          <th>e-Mail</th>
          <th>Fachhochschule</th>
          <th>FH-Praxislehrende</th>
          <th>Einsatzinfos</th>
          <th>Praktikumsstelle</th>
          <th>Reisedaten</th>
          <th>Zuschuss</th>
          <th>Files</th>
          <th><span title="Zu erweiterter Datenverarbeitung zugestimmt">DV</span></th>
          <th>Zeitstempel</th>
          <th>Aktionen</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ( $rows as $p ) : ?>
        <tr>
          <td><?= $p->matrikelnr; ?></td>
          <td><?= $p->firstname; ?></td>
          <td><?= $p->lastname; ?></td>
          <td>
            <?php if ( ! empty($p->email) ): ?>
              <a href="<?= $p->email; ?>"><?= $p->email; ?></a>
            <?php else: ?>
              <?= $p->email; ?>
            <?php endif; ?>
          </td>
          <td>
            <?= $this->schools[ $p->hochschule_id ]; ?><br>
          </td>
          <td>
            <?= $p->lektorin; ?>
            <?php if ( ! empty( $p->lektorin_email ) ): ?>
              &lt;<a href="mailto:<?= $p->lektorin_email ?>"><?= $p->lektorin_email ?></a>&gt;
            <?php endif; ?>
          </td>
          <td>
            <?php
            if ( $p->einsatzorga_id ) {
              $info = '<a href="' . $this->orgas[ $p->einsatzorga_id ]["website"] . '">';
              $info .= $this->orgas[ $p->einsatzorga_id ]["name"];
              $info .= '</a><br>';
            } else {
              $info = 'Andere Einsatzorganisation:<br>';
              $info .= ! empty($p->einsatzorga_other_website) ? '<a href="'.$p->einsatzorga_other_website.'">' : '';
              $info .= empty($p->einsatzorga_other_name) ? '<i>(n.a.)</i>' : $p->einsatzorga_other_name;
              $info .= ! empty($p->einsatzorga_other_website) ? '</a>' : '';
              $info .= '<br>';
            }
            $info .= "Kontakt: ";
            $info .= ! empty($p->einsatzorga_kontakt_email) ? '<a href="mailto:'.$p->einsatzorga_kontakt_email.'">' : '';
            $info .= empty($p->einsatzorga_kontakt) ? '<i>(n.a.)</i>' : $p->einsatzorga_kontakt;
            $info .= ! empty($p->einsatzorga_kontakt_email) ? '</a>' : '';
            echo $info;
            ?>
          </td>
          <td>
            <?= $p->praktikumsstelle; ?>
            <?php if ( ! empty($p->praktikumsstelle_aenderung) ) : ?>
              <span class="dashicons dashicons-warning" title="Änderung gegenüber Registrierungsdaten"></span>
            <?php else: ?>
              <span class="dashicons dashicons-yes" title="Unverändert"></span>
            <?php endif; ?>
          </td>
          <td>
            Ausreise: <?= empty( $p->ausreise ) ? '<i>(n.a.)</i>' : $p->ausreise; ?><br>
            Rückkehr: <?= empty( $p->rueckkehr ) ? '<i>(n.a.)</i>' : $p->rueckkehr; ?>
          </td>
          <td>
            OEZA:
            <?php
            if ( isset ( $p->oeza_zuschuss ) ) {
              if ( $p->oeza_zuschuss ) echo '<span class="dashicons dashicons-yes"></span>';
              else echo '<span class="dashicons dashicons-no"></span>';
            }
            else echo "n/a";
            ?>
            <br>
            ergänzend:
            <?php
            if ( isset ( $p->oeza_zuschuss_ergaenzend ) ) {
              if ( $p->oeza_zuschuss_ergaenzend ) echo '<span class="dashicons dashicons-yes"></span>';
              else echo '<span class="dashicons dashicons-no"></span>';
            }
            else echo "n/a";
            ?>
          </td>
          <td>
            <?php
            $files = $this->get_files_for_participant( $p->teilnehmerin_id );
            $file = $this->get_file_by_type( "cf_internship", $files );
            if ( $file !== false ) {
              $path = GSD_UPLOAD_URL . '/' . $file->filepath;
              $title = $file->type . ' , last upload: ' . $file->updated;
              echo '
              <a href="'.$path.'" title="Prakikumsbestätigung durch die Praktikumsstelle">
                <span class="dashicons dashicons-awards"></span>
              </a> ';
            }
            $file = $this->get_file_by_type( "cf_university", $files );
            if ( $file !== false ) {
              $path = GSD_UPLOAD_URL . '/' . $file->filepath;
              $title = $file->type . ' , last upload: ' . $file->updated;
              echo '
              <a href="'.$path.'" title="Prakikumsanerkennung durch die FH">
                <span class="dashicons dashicons-welcome-learn-more"></span>
              </a> ';
            }
            $file = $this->get_file_by_type( "cf_subsidy", $files );
            if ( $file !== false ) {
              $path = GSD_UPLOAD_URL . '/' . $file->filepath;
              $title = $file->type . ' , last upload: ' . $file->updated;
              echo '
              <a href="'.$path.'" title="Empfangsbestätigung Zuschuss Praktikumsstelle">
                <span class="dashicons dashicons-bank"></span>
              </a> ';
            }
            ?>
          </td>
          <td>
            <?php if ($p->datenverarbeitung): ?>
              <span class="dashicons dashicons-yes"></span>
            <?php else: ?>
              <span class="dashicons dashicons-no"></span>
            <?php endif; ?>
          </td>
          <td>
            Erstellt: <span class="nowrap"><?= $p->created; ?></span></br>
            Verändert: <span class="nowrap"><?= $p->updated; ?></span>
          </td>
          <td>
            <?php
            $uri_export = wp_nonce_url( '/wp-content/plugins/gsd-db/export-returners.php?set=single&id=' . $p->matrikelnr, 'export' );
            $uri_report = wp_nonce_url( '/wp-content/plugins/gsd-db/src/print-return-report.php?id=' . $p->matrikelnr, 'report' );
            $uri_datasheet = wp_nonce_url( '/wp-content/plugins/gsd-db/src/print-return-datasheet.php?id=' . $p->matrikelnr, 'datasheet' );
            $uri_mail = "mailto:".$p->email;
            ?>
            <a href="<?= $uri_mail ?>" title="E-Mail senden"><span class="dashicons dashicons-email-alt"></span></a>
            <a href="<?= $uri_export ?>" title="Als CSV exportieren"><span class="dashicons dashicons-download"></span></a>
            <a href="<?= $uri_datasheet ?>" title="Datenblatt anzeigen"><span class="dashicons dashicons-media-document"></span></a>
            <?php if ($p->status == 21): ?>
            <a href="<?= $uri_report ?>" title="Bericht generieren"><span class="dashicons dashicons-clipboard"></span></a>
            <?php endif; ?>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php
  }
}
