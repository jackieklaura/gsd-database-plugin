<?php
namespace GSDDB\Admin;

class Overview
{
  public $schools = [];
  public $orgas = [];
  public $cohorts = [];
  public $active_cohort;
  public $lecturer_school;
  public $search;
  public $is_admin = false;
  public $orderby = 'matrikelnr';
  public $order = 'asc';

  public function __construct () {
    global $wpdb;

    // initialise schools
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_HOCHSCHULE);
    foreach ( $r as $row ) {
      $this->schools[ $row->id ] = $row->name;
    }

    // initialise organisations
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_EINSATZORGA);
    foreach ( $r as $row ) {
      $this->orgas[ $row->id ] = [
        "name" => $row->name,
        "website" => $row->website
      ];
    }

    // intitalise cohorts
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_KOHORTE);
    foreach ( $r as $row ) {
      $this->cohorts[ $row->id ] = $row->label;
    }
  }

  /**
   * Prepares a database query based on participant status and search filters.
   *
   * @param array $status Array of status ids to search for
   * @return string|void A prepared query string or void in case of errors
   */
  public function get_participant_query ($status) {
    global $wpdb;

    // validate status parameter
    if (!is_array($status) || count($status) < 1) {
      echo $this->render_error('Invalid status format used to prepare search query!');
      return;
    }
    foreach ($status as $s) {
      if (!preg_match('/^\\d+$/', $s)) {
        echo $this->render_error('Invalid status format used to prepare search query!');
        return;
      }
    }

    // create the base query depending on participant status
    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $tbl_returner = $wpdb->prefix . GSD_TABLE_RETURN_FORM;
    $tbl_cohort = $wpdb->prefix . GSD_TABLE_KOHORTE;
    $tbl_school = $wpdb->prefix . GSD_TABLE_HOCHSCHULE;
    $cohort_label_selector = "";
    if ($this->search["jg"]) $cohort_label_selector = ", ct.label AS cohort_label";
    $q = "SELECT tn.* $cohort_label_selector FROM $tbl_teilnehmerin AS tn";
    // in case we only want to get participants with return forms handed in,
    // we just need to join with the return form table
    if ($this->search["rm"]) $q .= " JOIN $tbl_returner AS rt ON tn.id = rt.teilnehmerin_id";
    // for text search within (filter is added below) the cohorts
    // we have to join the label field of the cohorts table
    if ($this->search["jg"]) $q .= " JOIN $tbl_cohort AS ct ON tn.kohorte_id = ct.id";
    // for ordering by school we also need the school name
    if ($this->orderby == 'hochschule.name') $q .= " JOIN $tbl_school AS hochschule ON tn.hochschule_id = hochschule.id";
    $q .= " WHERE ";
    if (count($status) == 1) {
      $q .= "status = ".$status[0];
    } else {
      foreach ($status as $key => $value) {
        $status[$key] = "status = $value";
      }
      $q .= "(".implode(" OR ", $status).")";
    }

    // now add all additional filters
    if ($this->active_cohort != "all") {
      $q .= " AND tn.kohorte_id = %d";
      $q = $wpdb->prepare( $q, $this->active_cohort );
    }
    if ($this->search["text"]) {
      $s = '%'.$this->search["text"].'%';
      $q .= " AND (tn.firstname LIKE %s OR tn.lastname LIKE %s OR tn.einsatzland LIKE %s OR tn.comment_internal LIKE %s)";
      $q = $wpdb->prepare( $q, $s, $s, $s, $s, $s );
    }
    if (!$this->is_admin) {
      $q = $wpdb->prepare($q." AND hochschule_id = %d", $this->lecturer_school);
    }
    if ($this->search["fh"]) {
      $q .= " AND tn.hochschule_id = ".$this->search["fh"];
    }
    if ($this->search["eo"]) {
      $q .= " AND tn.einsatzorga_id = ".$this->search["eo"];
    }
    if ($this->search["ps"]) {
      $q .= " AND tn.praktikumsstelle LIKE %s";
      $q = $wpdb->prepare($q, '%'.$this->search["ps"].'%');
    }
    if ($this->search["ik"]) {
      $q .= " AND tn.comment_internal != ''";
    }
    // the return form boolean is already handled by the join above
    // the text search in cohort labels needs to be at the end, as it
    // needs to use HAVING instead of WHERE to use the column alias
    if ($this->search["jg"]) {
      $q .= " HAVING cohort_label LIKE %s";
      $q = $wpdb->prepare($q, '%'.$this->search["jg"].'%');
    }
    $q .= ' ORDER BY '.$this->orderby.' '.$this->order;
    return $q;
  }

  public static function render () {

    global $wpdb;
    $v = new Overview();

    // check wheter an admin is accessing this page
    if (current_user_can('manage_options')) {
      $v->is_admin = true;
    } else {
      // if a lecturer access the page, only participants from their school
      // should be shown.
      $user = wp_get_current_user();
      $tbl_lecturer = $wpdb->prefix . GSD_TABLE_LECTURER;
      $query = "SELECT hochschule_id FROM $tbl_lecturer WHERE wp_user = %d";
      $query = $wpdb->prepare($query, $user->data->ID);
      $row = $wpdb->get_row($query);
      if (!$row) {
        echo $v->render_error(
          'No school set for your lecturer account. Contact project lead to configure your school.'
        );
        return;
      }
      // we will use this later to modify the queries
      $v->lecturer_school = $row->hochschule_id;
    }

    // first check if we are in editing mode and if so, let the editor class
    // handle this request instead
    if ( ! empty( $_GET["action"] ) && $_GET["action"] === "edit" ) {
      require_once( "class-participant-editor.php" );
      return Participant_Editor::render();
    }

    // check if a specific cohort is alreay choosen, otherwise take the first available
    if ( ! empty( $_GET["kohorte"] ) ) {
      if ( array_search( $_GET["kohorte"], array_keys( $v->cohorts ) ) !== false  || $_GET["kohorte"] == "all" ) {
        $v->active_cohort = $_GET["kohorte"];
      } else {
        $v->active_cohort = array_key_first( $v->cohorts );
      }
    } else {
      $v->active_cohort = "all";
    }

    // validate ordering parameters
    $valid_order_fields = [
      'firstname',
      'lastname',
      'hochschule.name',
      'lektorin',
      'einsatzorga_id',
      'praktikumsstelle',
      'ausreise',
      'updated',
    ];
    if (isset($_GET['orderby'])) {
      if (!in_array($_GET['orderby'], $valid_order_fields)) {
        echo $v->render_error('Invalid order by parameter!');
        return;
      }
      $v->orderby = $_GET['orderby'];
    }
    if (isset($_GET['order'])) {
      if (!in_array($_GET['order'], ['asc', 'desc'])) {
        echo $v->render_error('Invalid order parameter!');
        return;
      }
      $v->order = $_GET['order'];
    }

    // validate search parameters
    $v->search = [
      "text" => false,
      "fh" => false,
      "eo" => false,
      "jg" => false,
      "ps" => false,
      "rm" => false,
      "ik" => false,
    ];
    foreach($v->search as $key => $value) {
      if( $key == "text" ) {
        if(!empty($_GET["search"])) $v->search["text"] = $_GET["search"];
      } else {
        if(!empty($_GET["search-".$key]) ) $v->search[$key] = $_GET["search-".$key];
      }
    }
    // FHs and Einsatzorgas need to be int IDs
    if($v->search["fh"] && !preg_match('/^\\d+$/', $v->search["fh"])) {
      echo $v->render_error('Invalid FH ID format!');
      return;
    }
    if($v->search["eo"] && !preg_match('/^\\d+$/', $v->search["eo"])) {
      echo $v->render_error('Invalid Einsatzorganisation ID format!');
      return;
    }
    // Rückkehrmeldung and internal comment are checkbox booleans
    if($v->search["rm"] && $v->search["rm"] != 'on') {
      echo $v->render_error('Invalid Rückkehrmeldung format!');
      return;
    }
    if($v->search["ik"] && $v->search["ik"] != 'on') {
      echo $v->render_error('Invalid Rückkehrmeldung format!');
      return;
    }
    // The other fields are text searches
    foreach( ["text", "jg", "ps"] as $item ) {
      $v->search[$item] = esc_attr($v->search[$item]);
    }

    ?>
    <div class="wrap">
      <h1>GSD Datenbank: Übersicht</h1>
    <?php

    $valid_actions = [ "approve", "register", "cancel", "reactivate", "delete" ];
    if ( ! empty( $_GET["action"] ) ) {

      // check if the action is a supported one
      if ( ! empty($_GET["action"]) && ! in_array($_GET["action"], $valid_actions) ) {
        echo $v->render_error(
          'The action you requested is not supported.',
          'Unsupported action for GSD Overview. GET data: ' . json_encode($_GET),
          __FILE__, __LINE__
        );
        return;
      }

      // check if the nonce is valid
      if ( empty($_GET["_wpnonce"]) || ! wp_verify_nonce($_GET["_wpnonce"]) ) {
        $msg = '
        This request was illegal due to a missing or invalid nonce.<br>
        If you clicked a link 24 hours after the page was loaded, please reload first.
        ';
        echo $v->render_error(
          $msg,
          "No valid nonce for GSD overview action. GET data: " . json_encode($_GET),
          __FILE__, __LINE__
        );
        ?>
        <a href="<?= $_SERVER['SCRIPT_NAME'] . '?page=' . $_GET['page']; ?>">Reload page.</a>
        <?php
        return;
      }

      // check if the ID conforms to the format
      if ( empty($_GET["id"]) || ! preg_match('/^[a-z0-9]+$/', $_GET["id"]) ) {
        echo $v->render_error(
          'This request was illegal due to an invalid user ID.',
          "Invalid user ID provided for GSD overview action. GET data: " . json_encode($_GET),
          __FILE__, __LINE__
        );
        return;
      }

      switch( $_GET["action"] ) {
        case "approve":
          $v->approve_participant( $_GET["id"] );
          break;

        case "register":
          $v->register_participant( $_GET["id"] );
          break;

        case "cancel":
          $v->cancel_participant( $_GET["id"] );
          break;

        case "reactivate":
          $v->reactivate_participant( $_GET["id"] );
          break;

        case "delete":
          if( $v->delete_participant( $_GET["id"] ) == "await_confirmation" ) {
            // if the confirmation dialogue is printed, we don't want to
            // have the participants list output afterward, so we'll return immediately
            return;
          }
          // in all other cases we can continue as usual
          break;
      }

    }

    ?>
      <div class="cohort-selector">
        Aktuell ausgewählte Kohorte:
        <span class="currently-active"><?= $v->active_cohort == "all" ? "Alle" : $v->cohorts[$v->active_cohort] ?></span>
        Umschalten auf:
        <?php
        if ( $v->active_cohort != "all" ) {
          $uri = $_SERVER["SCRIPT_NAME"] . '?page=' . $_GET["page"] . '&kohorte=all';
          echo '<a class="button" href="' . $uri . '">Alle</a> ';
        }
        foreach( $v->cohorts as $key => $label ) {
          if ( $key != $v->active_cohort ) {
            $uri = $_SERVER["SCRIPT_NAME"] . '?page=' . $_GET["page"] . '&kohorte=' . $key;
            echo '<a class="button" href="' . $uri . '">' . $label . '</a> ';
          }
        }
        ?>
      </div>
      <div>
        <form method="GET" action="">
          <input type="hidden" name="page" value="gsd_menu">
          <?php if ( ! empty( $_GET["kohorte"] ) ) : ?>
            <input type="hidden" name="kohorte" value="<?= $_GET["kohorte"] ?>">
          <?php else: ?>
            <input type="hidden" name="kohorte" value="<?= $v->active_cohort ?>">
          <?php endif; ?>
          Einfacher Textfilter: <input type="text" name="search" value="<?= $v->search["text"] ? $v->search["text"] : "" ?>"> <small>(Vorname, Nachname, Einsatzland, interner Kommentar)</small><br>
          <div id="search-accordion">
            <h3>Erweiterte Zusatzfilter</h3>
            <div>
              <table>
                <tr>
                  <td>FH:</td>
                  <td>
                    <select name="search-fh" id="search-fh">
                      <option value="" <?= !$v->search["fh"] ? 'selected="selected"' : '' ?>>Alle</option>
                      <?php foreach ($v->schools as $school_id => $school_name) : ?>
                      <option value="<?= $school_id; ?>" <?= $v->search["fh"] == $school_id ? 'selected="selected"' : '' ?>><?= $school_name; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Einsatzorga:</td>
                  <td>
                    <select name="search-eo" id="search-eo">
                      <option value="" <?= !$v->search["eo"] ? 'selected="selected"' : '' ?>>Alle</option>
                      <?php foreach ($v->orgas as $orga_id => $orga) : ?>
                      <option value="<?= $orga_id; ?>" <?= $v->search["eo"] == $orga_id ? 'selected="selected"' : '' ?>><?= $orga["name"]; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>Jahrgang:</td>
                  <td>
                    <input type="text" name="search-jg" <?= $v->search["jg"] ? 'value="'.$v->search["jg"].'"' : ''; ?>><br>
                    <small>(Textsuche im Kohorten-Feld; nur sinnvoll, wenn alle Kohorten ausgewählt)</small>
                  </td>
                </tr>
                <tr>
                  <td>Praktikumsstelle:</td>
                  <td>
                    <input type="text" name="search-ps" <?= $v->search["ps"] ? 'value="'.$v->search["ps"].'"' : ''; ?>>
                  </td>
                </tr>
                <tr>
                  <td>Rückkehrmeldung:</td>
                  <td>
                    <input type="checkbox" name="search-rm" <?= $v->search["rm"] ? 'checked' : ''; ?>>
                  </td>
                </tr>
                <tr>
                  <td>Interner Kommentar:</td>
                  <td>
                    <input type="checkbox" name="search-ik" <?= $v->search["ik"] ? 'checked' : ''; ?>>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <?php
          $additional_filters_opened = "false";
          foreach ($v->search as $key => $value) {
            if ($key != "text" && $value ) $additional_filters_opened = "0";
          }
          ?>
          <script>
              console.log(jQuery( "#search-accordion" ))
              jQuery( function() {
                jQuery( "#search-accordion" ).accordion({
                  collapsible: true,
                  active: <?= $additional_filters_opened ?>,
                });
                console.log('doing the accordion')
              } );
          </script>
          <input type="submit" value="Suchen">
        </form>
      </div>
    <?php

    $q = $v->get_participant_query([0]);
    $unapproved = $wpdb->get_results( $q );

    $q = $v->get_participant_query([1]);
    $approved = $wpdb->get_results( $q );

    # This category was introduced later, and a non-linear status number was
    # chosen to not interfere with existing user data
    $q = $v->get_participant_query([12]);
    $submitted = $wpdb->get_results( $q );

    $q = $v->get_participant_query([2, 21]);
    $registered = $wpdb->get_results( $q );

    $q = $v->get_participant_query([3]);
    $cancelled = $wpdb->get_results( $q );

    require_once('class-overview-table.php');
    if( ! empty($unapproved) ) {
      ?>
      <h2>Noch nicht bestätigte Voranmeldungen:</h2>
      <?php
      $tbl_unapproved = new Overview_Table($unapproved, 'unapproved', $v);
      $tbl_unapproved->prepare_items();
      $tbl_unapproved->display();
    }
    if( ! empty($approved) ) {
      ?>
      <h2>Vorangemeldete Teilnehmer*innen (noch nicht eingereicht):</h2>
      <?php
      $tbl_approved = new Overview_Table($approved, 'approved', $v);
      $tbl_approved->prepare_items();
      $tbl_approved->display();
    }
    if( ! empty($submitted) ) {
      ?>
      <h2>Vorangemeldete Teilnehmer*innen mit eingereichter Fixanmeldung:</h2>
      <?php
      $tbl_submitted = new Overview_Table($submitted, 'submitted', $v);
      $tbl_submitted->prepare_items();
      $tbl_submitted->display();
    }
    if( ! empty($registered) ) {
      ?>
      <h2>Fixangemeldete Teilnehmer*innen:</h2>
      <?php
      $tbl_registered = new Overview_Table($registered, 'registered', $v);
      $tbl_registered->prepare_items();
      $tbl_registered->display();
    }
    if( ! empty($cancelled) ) {
      ?>
      <h2>Abgebrochen:</h2>
      <?php
      $tbl_cancelled = new Overview_Table($cancelled, 'cancelled', $v);
      $tbl_cancelled->prepare_items();
      $tbl_cancelled->display();
    }

    if ($v->is_admin) {
      if ( empty($approved) && empty($unapproved) && empty($submitted) && empty($registered) && empty($cancelled) ) {
        ?>
        <div class="notice">Es sind keine Teilnehmer*innendaten für diese Auswahl vorhanden!</div>
        <?php
      } elseif ( $v->active_cohort != "all" ) {
        ?>
        <h2>Mail-Adressat*innen-Kreise:</h2>
        <ul>
          <?php
          $addresses = '';
          foreach ( $unapproved as $person ) {
            $addresses .= $person->email . ';';
          }
          foreach ( $approved as $person ) {
            $addresses .= $person->email . ';';
          }
          foreach ( $submitted as $person ) {
            $addresses .= $person->email . ';';
          }
          foreach ( $registered as $person ) {
            $addresses .= $person->email . ';';
          }
          foreach ( $cancelled as $person ) {
            $addresses .= $person->email . ';';
          }
          // remove trailing ','
          $addresses = substr( $addresses, 0, -1 );
          ?>
          <li>
            <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">Alle</a> &nbsp;&nbsp;&nbsp;
            [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in CC</a> ] &nbsp;&nbsp;&nbsp;
            [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in BCC</a> ]
          </li>
          <?php
          if ( ! empty($unapproved) ) {
            $addresses = '';
            foreach ( $unapproved as $person ) {
              $addresses .= $person->email . ';';
            }
            // remove trailing ','
            $addresses = substr( $addresses, 0, -1 );
            ?>
            <li>
              <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle noch nicht bestätigten Voranmeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">Alle noch nicht bestätigten Voranmeldungen</a> &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle noch nicht bestätigten Voranmeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in CC</a> ] &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle noch nicht bestätigten Voranmeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in BCC</a> ]
            </li>
            <?php
          }
          if ( ! empty($approved) ) {
            $addresses = '';
            foreach ( $approved as $person ) {
              $addresses .= $person->email . ';';
            }
            // remove trailing ','
            $addresses = substr( $addresses, 0, -1 );
            ?>
            <li>
              <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle noch nicht eingereichten Vorangemeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">Alle noch nicht eingereichten Vorangemeldungen</a> &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle noch nicht eingereichten Vorangemeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in CC</a> ] &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle noch nicht eingereichten Vorangemeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in BCC</a> ]
            </li>
            <?php
          }
          if ( ! empty($submitted) ) {
            $addresses = '';
            foreach ( $submitted as $person ) {
              $addresses .= $person->email . ';';
            }
            // remove trailing ','
            $addresses = substr( $addresses, 0, -1 );
            ?>
            <li>
              <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle eingereichten Voranmeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">Alle eingereichten Voranmeldungen</a> &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle eingereichten Voranmeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in CC</a> ] &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle eingereichten Voranmeldungen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in BCC</a> ]
            </li>
            <?php
          }
          if ( ! empty($registered) ) {
            $addresses = '';
            foreach ( $registered as $person ) {
              $addresses .= $person->email . ';';
            }
            // remove trailing ','
            $addresses = substr( $addresses, 0, -1 );
            ?>
            <li>
              <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle Fixangemeldeten in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">Alle Fixangemeldeten</a> &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle Fixangemeldeten in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in CC</a> ] &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle Fixangemeldeten in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in BCC</a> ]
            </li>
            <?php
          }
          if ( ! empty($cancelled) ) {
            $addresses = '';
            foreach ( $cancelled as $person ) {
              $addresses .= $person->email . ';';
            }
            // remove trailing ','
            $addresses = substr( $addresses, 0, -1 );
            ?>
            <li>
              <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle Abbrecher*innen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">Alle Abbrecher*innen</a> &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle Abbrecher*innen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in CC</a> ] &nbsp;&nbsp;&nbsp;
              [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle Abbrecher*innen in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in BCC</a> ]
            </li>
            <?php
          }
          foreach ( $v->schools as $school_id => $school_name ) {
            // TODO: check if this list makes sense in combination with search filters
            $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
            $q = "SELECT email FROM $tbl_teilnehmerin WHERE hochschule_id = %d AND kohorte_id = %d";
            $q = $wpdb->prepare( $q, $school_id, $v->active_cohort );
            $result = $wpdb->get_results( $q );
            if ( $result ) {
              $addresses = '';
              foreach ( $result as $person ) {
                $addresses .= $person->email . ';';
              }
              // remove trailing ','
              $addresses = substr( $addresses, 0, -1 );
              ?>
              <li>
                <a href="mailto:<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle an der <?= esc_attr($school_name) ?> in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>"><?= esc_html($school_name) ?></a> &nbsp;&nbsp;&nbsp;
                [ <a href="mailto:?cc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle an der <?= esc_attr($school_name) ?> in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in CC</a> ] &nbsp;&nbsp;&nbsp;
                [ <a href="mailto:?bcc=<?= esc_attr($addresses) ?>?subject=GSD Benachrichtigung an: Alle an der <?= esc_attr($school_name) ?> in Kohorte <?= esc_attr($v->cohorts[$v->active_cohort]) ?>">in BCC</a> ]
              </li>
              <?php
            }
          }
          ?>
        </ul>
        <h2>Exportieren in CSV Files:</h2>
        <ul>
          <li><a href="<?= wp_nonce_url( '/wp-content/plugins/gsd-db/export.php?set=all&kohorte=' . $v->active_cohort, 'export' ) ?>">Alle Datensätze in Kohorte</a></li>
          <?php if (!empty($unapproved)): ?>
            <li><a href="<?= wp_nonce_url( '/wp-content/plugins/gsd-db/export.php?set=unapproved&kohorte=' . $v->active_cohort, 'export' ) ?>">Nur noch nicht bestätigte Voranmeldungen in Kohorte</a></li>
          <?php endif; ?>
          <?php if (!empty($approved)): ?>
            <li><a href="<?= wp_nonce_url( '/wp-content/plugins/gsd-db/export.php?set=approved&kohorte=' . $v->active_cohort, 'export' ) ?>">Nur noch nicht eingereichte Vorangemeldungen in Kohorte</a></li>
          <?php endif; ?>
          <?php if (!empty($submitted)): ?>
            <li><a href="<?= wp_nonce_url( '/wp-content/plugins/gsd-db/export.php?set=submitted&kohorte=' . $v->active_cohort, 'export' ) ?>">Nur eingereichte Vorangemeldungen in Kohorte</a></li>
          <?php endif; ?>
          <?php if (!empty($registered)): ?>
            <li><a href="<?= wp_nonce_url( '/wp-content/plugins/gsd-db/export.php?set=registered&kohorte=' . $v->active_cohort, 'export' ) ?>">Nur Fixangemeldete in Kohorte</a></li>
          <?php endif; ?>
          <?php if (!empty($cancelled)): ?>
            <li><a href="<?= wp_nonce_url( '/wp-content/plugins/gsd-db/export.php?set=cancelled&kohorte=' . $v->active_cohort, 'export' ) ?>">Nur Abgebrochene in Kohorte</a></li>
          <?php endif; ?>
        </ul>
        <?php
      } else {
        ?>
        <h2>Mail-Adressat*innen-Kreise:</h2>
        <div>... sind bislang nur für Kohorten implementiert!</div>
        <?php
      }
    }
    ?>
    </div>
    <?php
  }

  public function render_error($msg, $log_msg=false, $file=false, $line=false) {
    if ($log_msg) {
      $m = '[GSD error]';
      if ( $file ) {
        $m .= ' '.$file;
      }
      if ( $line ) {
        $m .= $line;
      }
      $m .= ': ' . $log_msg;
      error_log($m);
    }
    $html = '<div class="error">'.$msg.'</div>';
    return $html;
  }

  public function approve_participant ( $id ) {
    global $wpdb;

    // first, get the relevant participant data from the db
    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $query = "SELECT firstname, lastname, email FROM $tbl_teilnehmerin WHERE matrikelnr = %s";
    $result = $wpdb->get_row( $wpdb->prepare( $query, $id ) );
    if( empty( $result ) ) {
      echo $this->render_error(
        'Teilnehmer*innendaten zu dieser Matrikelnummer konnten nicht gefunden werden!',
        'The submitted GET data was: ' . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }
    $participant = [
      "matrikelnr" => $id,
      "firstname" => $result->firstname,
      "lastname" => $result->lastname,
      "email" => $result->email,
    ];

    // now create wordpress user
    $user = $this->create_user($participant);
    if ( is_wp_error($user["id"]) ) {
      echo $this->render_error(
        'Neuer Benutzer*innen-Account konnte nicht angelegt werden:
        ' . $user["id"]->get_error_message(),
        $user["id"]->get_error_message(), __FILE__, __LINE__
      );
      return;
    }

    // finally update database entry for participant
    $set = [
      "status" => 1,
      "wp_user" => $user["id"],
    ];
    $where = [ "matrikelnr" => $id ];
    $updated = $wpdb->update( $tbl_teilnehmerin, $set, $where );
    if ( ! $updated ) {
      echo $this->render_error(
        'The provided ID for this action does not exist or was already approved!',
        "A new WordPress user was created, but the corresponding GSD table entry
        could not be updated. This should not happen. Please remove the newly
        created WordPress user account again and contact the plugin author.
        The submitted GET data was: " . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }

    // send notification e-mail with credentials to the participant
    $msg = "Liebe*r {$participant["firstname"]} {$participant["lastname"]},\n" .
      "\n" .
      "deine Voranmeldung für ein GSD Praktikum wurde gerade überprüft und\n" .
      "freigeschalten. Damit erhältst du nun auch einen Account mit folgenden\n" .
      "Zugangsdaten:\n" .
      "\n" .
      "  Login:     {$participant["matrikelnr"]}\n" .
      "  Passwort:  {$user["pass"]}\n" .
      "\n" .
      "Mit diesen Daten kannst du dich nun auf der GSD Website einloggen. Nutze\n" .
      "dazu den Login-Button auf der Website im Menü links unten.\n" .
      "\n" .
      "Sobald du eingelogged bist, kannst du das Formular für die\n" .
      "Fixanmeldung ansehen bzw. bis zur Fixanmeldefrist ausfüllen.\n" .
      "\n" .
      "Liebe Grüße,\n" .
      "die GSD Projektleitung";
    wp_mail($participant["email"], "Account für das GSD Praktikum freigeschalten", $msg);
  }

  public function register_participant ( $id ) {
    global $wpdb;

    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $set = [
      "status" => 2,
    ];
    $where = [ "matrikelnr" => $id ];
    $updated = $wpdb->update( $tbl_teilnehmerin, $set, $where );
    if ( ! $updated ) {
      echo $this->render_error(
        'The provided ID for this action does not exist or was already cancelled!',
        "The submitted GET data was: " . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }

    // get participant details and send notification e-mail
    $query = "SELECT firstname, lastname, email FROM $tbl_teilnehmerin WHERE matrikelnr = %s";
    $result = $wpdb->get_row( $wpdb->prepare( $query, $id ) );
    if( empty( $result ) ) {
      echo $this->render_error(
        'Participant could be updated but data for the notification not found!',
        'The submitted GET data was: ' . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }
    $msg = "Liebe*r {$result->firstname} {$result->lastnam},\n" .
      "\n" .
      "deine Fixanmeldung für ein GSD Praktikum wurde gerade überprüft und\n" .
      "bestätigt! 😊\n" .
      "\n" .
      "Falls du noch Daten nachreichen musst, tue das bitte spätestens bis zum Termin\n" .
      "des Vorbereitungsseminares. \n" .
      "\n" .
      "Gewisse fehlende Daten müssen schon früher nachgereicht werden – die\n" .
      "Projektleitung hat dir gegebenenfalls ein Kommentar dazu in deinem Datenblatt\n" .
      "hinterlegt. \n" .
      "\n" .
      "Logge dich bitte in dein Datenblatt ein (über den Button „Fixanmeldung“) und\n" .
      "überprüfe, ob dir ein Kommentar hinterlegt wurde.\n" .
      "\n" .
      "Du kannst fehlende Daten nachreichen, indem du den jeweiligen\n" .
      "Formularabschnitt „zwischenspeicherst“, dann sieht auch das GSD-Team deine\n" .
      "Änderungen. \n" .
      "\n" .
      "Nun wünschen wir dir alles Gute für die weiteren Schritte und melden uns bald\n" .
      "mit einem persönlichen Willkommensmail bei dir!\n" .
      "\n" .
      "Liebe Grüße,\n" .
      "das GSD-Team";
    wp_mail($result->email, "Fixanmeldung für das GSD Praktikum bestätigt", $msg);
  }

  public function cancel_participant ( $id ) {
    global $wpdb;

    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $set = [
      "status" => 3,
    ];
    $where = [ "matrikelnr" => $id ];
    $updated = $wpdb->update( $tbl_teilnehmerin, $set, $where );
    if ( ! $updated ) {
      echo $this->render_error(
        'The provided ID for this action does not exist or was already cancelled!',
        "The submitted GET data was: " . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }
  }

  public function reactivate_participant ( $id ) {
    global $wpdb;

    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $set = [
      "status" => 1,
    ];
    $where = [ "matrikelnr" => $id ];
    $updated = $wpdb->update( $tbl_teilnehmerin, $set, $where );
    if ( ! $updated ) {
      echo $this->render_error(
        'The provided ID for this action does not exist or was already reactivated!',
        "The submitted GET data was: " . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }
  }

  public function delete_participant ( $id ) {
    // before finally deleting anything, we ask for confirmation
    if( empty( $_GET["confirm"] ) || $_GET["confirm"] !== "true" ) {
      $confirm = $_SERVER["QUERY_STRING"] . '&confirm=true';
      $confirm_without_user = $confirm . '&delete_user=false';
      ?>
      <div class="notice notice-warning">
        <br>
        <span class="dashicons dashicons-warning"></span>
        Löschvorgang für Teilnehmer*in mit Matrikelnr <?= $id ?> initialisiert!
        <br>&nbsp;
      </div>

      Möchtest du d* Teilnehmer*in wirklich unwiederruflich löschen?<br><br>

      <a class="button-primary" href="?<?= $confirm ?>">Ja!</a>
      <a class="button" href="?<?= $confirm_without_user ?>">Ja, aber WordPress Account nicht löschen.</a>
      &nbsp;&nbsp;&nbsp;
      <a class="button-primary" onclick="history.back()">Nein, bitte bring mich wieder retour.</a>
      <?php

      return "await_confirmation";
    }

    // get database id and wp user id of participant
    global $wpdb;
    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $query = $wpdb->prepare( "SELECT id, wp_user, status FROM $tbl_teilnehmerin WHERE matrikelnr = %s", $id );
    $row = $wpdb->get_row( $query );
    if( empty( $row ) ) {
      echo $this->render_error(
        "No entry for a participant with matrikelnr $id could be found in $tbl_teilnehmerin.",
        "Provided GET data: " . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }
    $teilnehmerin_id = $row->id;
    $user_id = $row->wp_user;
    $status = $row->status;

    // delete all emergency contacts, if any
    $tbl_notfallkontakt = $wpdb->prefix . GSD_TABLE_NOTFALLKONTAKT;
    $num = $wpdb->delete( $tbl_notfallkontakt, [ "teilnehmerin_id" => $teilnehmerin_id ] );
    if ( $num === false ) {        // 0 is fine as return, but false means error
      echo $this->render_error(
        "Something went wrong when trying to delete from table $tbl_notfallkontakt. Please contact
        the plugin author with this messange and the current time and date as debug info.",
        "Provided GET data: " . json_encode($_GET),
        __FILE__, __LINE__
      );
    }

    // delete all files, if any
    $tbl_file = $wpdb->prefix . GSD_TABLE_FILE;
    // TODO: as soon as file upload is implemented: fetch file paths first and delete them
    $num = $wpdb->delete( $tbl_file, [ "teilnehmerin_id" => $teilnehmerin_id ] );
    if ( $num === false ) {        // 0 is fine as return, but false means error
      echo $this->render_error(
        "Something went wrong when trying to delete from table $tbl_file. Please contact
        the plugin author with this messange and the current time and date as debug info.",
        "Provided GET data: " . json_encode($_GET),
        __FILE__, __LINE__
      );
    }

    // delete participant
    $num = $wpdb->delete( $tbl_teilnehmerin, [ "matrikelnr" => $id ] );
    if ( $num != 1 ) {        // everything else than 1 should really not happen
      echo $this->render_error(
        "Something went wrong when trying to delete from table $tbl_teilnehmerin. Please contact
        the plugin author with this messange and the current time and date as debug info.",
        "WPDB::delete returned: '$num', provided GET data: " . json_encode($_GET),
        __FILE__, __LINE__
      );
    }

    // delete wp user (if this was not exempted or if we handle a pre registrant)
    if( $status !== "0" ) {            // wpdb returns an int id as a string !!!
      if ( empty( $_GET["delete_user"] ) || $_GET["delete_user"] !== "false" ) {
        // wp_delete_user is only available by default in the admin
        // therefore we have to require wp-admin/includes/user.php here
        require_once( ABSPATH.'wp-admin/includes/user.php' );
        wp_delete_user( $user_id );
      }
    }

    ?>
    <div class="notice notice-success">
      <span class="dashicons dashicons-yes"></span>
      Teilnehmer*in mit Matrikelnummer <b><?= $id ?></b> wurde erfolgreich gelöscht.
    </div>
    <?php
  }

  private function create_user( $data ) {
    $keyspace = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
                '0123456789'; // WP seems to not like (some?) special characters
    $password = "";
    for ($i = 0; $i < 24; $i++) {
      $password .= $keyspace[ random_int( 0, strlen($keyspace)-1 ) ];
    }
    $id = wp_insert_user([
      "user_login" => $data["matrikelnr"],
      "user_pass" => $password,
      "first_name" => $data["firstname"],
      "last_name" => $data["lastname"],
      "user_email" => $data["email"],
      "show_admin_bar_front" => false,
    ]);
    return [
      "id" => $id,
      "pass" => $password,
    ];
  }

  public function get_files_for_participant ( $id ) {
    global $wpdb;
    $tbl_file = $wpdb->prefix . GSD_TABLE_FILE;
    $query = $wpdb->prepare( "SELECT * FROM $tbl_file WHERE teilnehmerin_id = %d", $id );
    return $wpdb->get_results( $query );
  }

  public function get_file_by_type ( $type, $files ) {
    foreach ( $files as $file ) {
      if ( $file->type === $type ) return $file;
    }
    return false;
  }

}
