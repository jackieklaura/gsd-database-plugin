<?php
namespace GSDDB\Admin;

require_once plugin_dir_path( __FILE__ ) . '../class-form.php';
require_once plugin_dir_path( __FILE__ ) . '../class-plugin.php';
require_once plugin_dir_path( __FILE__ ) . '../class-database.php';
require_once plugin_dir_path( __FILE__ ) . '../class-notification.php';
use GSDDB\Form;
use GSDDB\Plugin;
use GSDDB\Database;
use GSDDB\Notification;

class Participant_Editor
{
  private $id;
  private $participant;
  private $schools;
  private $orgas;
  private $contacts_local;
  private $contacts_remote;
  private $files;
  private $form_config;
  private $form;

  public function __construct ( $participant_data ) {
    global $wpdb;
    $this->participant = $participant_data;

    // initialise schools
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_HOCHSCHULE);
    foreach ( $r as $row ) {
      $this->schools[ $row->id ] = $row->name;
    }

    // initialise organisations
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_EINSATZORGA);
    foreach ( $r as $row ) {
      $this->orgas[ $row->id ] = [
        "name" => $row->name,
        "website" => $row->website
      ];
    }

    // initialise contacts
    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_NOTFALLKONTAKT .
      " WHERE teilnehmerin_id = %d AND ist_im_einsatzland = 0";
    $q = $wpdb->prepare( $q, $participant_data["id"] );
    $this->contacts_local = $wpdb->get_row( $q, ARRAY_A );

    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_NOTFALLKONTAKT .
      " WHERE teilnehmerin_id = %d AND ist_im_einsatzland = 1";
    $q = $wpdb->prepare( $q, $participant_data["id"] );
    $this->contacts_remote = $wpdb->get_row( $q, ARRAY_A );

    // initialise files
    $q = "SELECT * FROM {$wpdb->prefix}".GSD_TABLE_FILE .
      " WHERE teilnehmerin_id = %d";
    $q = $wpdb->prepare( $q, $participant_data["id"] );
    $this->files = $wpdb->get_results( $q, ARRAY_A );

    $this->form_config = [
      "title" => "Fixanmeldungsdaten",
      "slug" => "gsd_admin_edit_participant",
      "submit_label" => "Speichern",
      "fields" => [
        [
          "type" => "section",
          "header" => "Kommentare der Projektleitung",
          "fields" => [
            [
              "name" => "comment_internal",
              "label" => "Interne Anmerkung (nicht für Teilnehmerin sichtbar)",
              "type" => "textarea",
              "sanitize" => "textarea_field",
            ],
            [
              "name" => "comment_for_participant",
              "label" => "Anmerkung für d* Teilnehmer*in",
              "type" => "textarea",
              "sanitize" => "textarea_field",
            ],
          ]
        ],
        [ // Persönliche Daten
          "type" => "section",
          "header" => "Persönliche Daten",
          "fields" => [
            [
              "name" => "firstname",
              "label" => "Vorname", "type" => "text", "sanitize" => "text_field",
            ],
            [
              "name" => "lastname",
              "label" => "Nachname", "type" => "text", "sanitize" => "text_field",
            ],
            [
              "name" => "email",
              "label" => "E-Mailadresse", "type" => "email", "sanitize" => "email",
            ],
            [
              "name" => "adresse", "label" => "Adressdaten", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "telefon", "label" => "Telefonnummer", "type" => "tel",
              "length" => 20,
              "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
              "format_info" => "only numbers and spaces and an optional '+' in the beginning",
            ],
            [
              "name" => "skype", "label" => "Skypekontakt", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "birthdate",
              "label" => "Geburtsdatum", "type" => "date",
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
            [
              "name" => "staatsangehoerigkeit", "label" => "Staatsangehoerigkeit",
              "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "passnummer", "label" => "Reisepassnummer", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "ausstellungsdatum", "label" => "Reisepass Ausstellungsdatum",
              "type" => "date",
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
            [
              "name" => "ausstellungsort", "label" => "Reisepass Ausstellungsort",
              "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "studienbeihilfe",
              "label" => "Ich beziehe Studienbeihilfe/Selbsterhalterstipendium",
              "type" => "checkbox", "validate" => "checkbox",
            ],
            [
              "name" => "anmerkungen",
              "label" => "Evtl. Erkrankungen, Allergien, spezielle Bedürfnisse",
              "type" => "textarea",
              "sanitize" => "textarea_field",
            ],
          ]
        ],
        [ // Fachhochschule.
          "type" => "section",
          "header" => "FH-bezogene Daten",
          "fields" => [
            [
              "type" => "dropdown",
              "name" => "hochschule_id", "label" => "Fachhochschule", "mandatory" => true,
              "options" => Database::get_dropdown_array_hochschule(),
              "validate" => "options"
            ],
            [
              "name" => "matrikelnr",
              "label" => "Matrikelnummer", "type" => "text",
            ],
            [
              "type" => "radio",
              "name" => "studium", "label" => "Studiengang",
              "options" => Database::get_radio_options_studien(),
              "validate" => "options",
            ],
            [
              "name" => "vollzeit",
              "label" => "Vollzeitstudium (nicht ankreuzen für berufsbegleitende Studiengänge):",
              "type" => "checkbox",
              "validate" => "checkbox"
            ],
            [
              "name" => "lektorin",
              "label" => "Name FH-Praxislektor*in", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "lektorin_email",
              "label" => "E-Mailadresse FH-Praxislektor*in", "type" => "email",
              "sanitize" => "email",
            ],
            [
              "name" => "lektorin_telefon",
              "label" => "Telefon FH-Praxislektor*in",
              "type" => "tel",
              "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
              "format_info" => "only numbers and spaces and an optional '+' in the beginning",
            ],
          ]
        ],
        [ // Einsatzorganisation
          "type" => "section",
          "header" => "Einsatzorganisation",
          "fields" => [
            [
              "name" => "einsatzorga_id", "label" => "Einsatzorganisation",
              "type" => "einsatzorga",
              "options" => Database::get_dropdown_array_einsatzorga(),
              "options_extended" => Database::get_einsatzorgas(),
              "validate" => "options",
              "alternative_fields" => [
                "einsatzorga_other_name" => [ "label" => "Name der Einsatzorganisation" ],
                "einsatzorga_other_website" => [ "label" => "Website der Einsatzorganisation" ],
              ]
            ],
            [
              "name" => "einsatzorga_kontakt",
              "label" => "Name Kontaktperson der Einsatzorganisation",
              "type" => "text", "sanitize" => "text_field",
            ],
            [
              "name" => "einsatzorga_kontakt_email",
              "label" => "E-Mailadresse Kontaktperson der Einsatzorganisation",
              "type" => "text", "sanitize" => "email",
            ],
            [
              "name" => "einsatzorga_kontakt_telefon",
              "label" => "Telefon Kontaktperson der Einsatzorganisation",
              "type" => "tel",
              "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
              "format_info" => "only numbers and spaces and an optional '+' in the beginning",
            ],
          ]
        ],
        [ // Praktikumsstelle
          "type" => "section",
          "header" => "Praktikumsstelle",
          "fields" => [
            [
              "name" => "einsatzland",
              "label" => "Einsatzland", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "praktikumsstelle",
              "label" => "Name (und ggf. Abkürzung) Praktikumsstelle", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "praktikumsstelle_adresse",
              "label" => "Adresse Praktikumsstelle", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "praktikumsstelle_telefon",
              "label" => "Telefon Praktikumsstelle",
              "type" => "tel",
              "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
              "format_info" => "only numbers and spaces and an optional '+' in the beginning",
            ],
            [
              "name" => "praktikumsstelle_website",
              "label" => "Website Praktikumsstelle", "type" => "text",
              "sanitize" => "url",
            ],
            [
              "name" => "arbeitsbereich",
              "label" => "Arbeitsbereich", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "praktikumsstelle_kontakt",
              "label" => "Name Praxisanleiter*in vor Ort", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "praktikumsstelle_kontakt_email",
              "label" => "E-Mailadresse Praxisanleiter*in vor Ort", "type" => "email",
              "sanitize" => "email",
            ],
            [
              "name" => "praktikumsstelle_kontakt_telefon",
              "label" => "Telefon Praxisanleiter*in vor Ort", "type" => "tel",
              "validate" => "regex", "regex" => '/^\\+?[0-9 ]+$/',
              "format_info" => "only numbers and spaces and an optional '+' in the beginning",
            ],            [
              "name" => "ausreise",
              "label" => "Ausreise", "type" => "date",
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
            [
              "name" => "rueckkehr",
              "label" => "Rückreise", "type" => "date",
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
            [
              "name" => "praktikum_beginn",
              "label" => "Praktikumsbeginn", "type" => "date",
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
            [
              "name" => "praktikum_ende",
              "label" => "Praktikumsende", "type" => "date",
              "validate" => "regex", "regex" => '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/',
              "format_info" => "YYYY-MM-DD",
            ],
            [
              "name" => "praktikum_dauer",
              "label" => "Praktikumsdauer (in Wochen)", "type" => "number",
              "validate" => "regex", "regex" => '/^[0-9]+$/',
            ],
            [
              "name" => "praktikumsstelle_beschreibung",
              "type" => "textarea", "sanitize" => "textarea_field",
              "label" => "Beschreibung Praktikumsstelle (mind. 5 – höchstens 10 Zeilen)",
              "explanation" => "Kurzbeschreibung, Zielsetzungen,
                Zielgruppen, Angebote etc.
                Bitte Quelle angeben (Einsatzorganisation, Schreiben der
                Praktikumsstelle vom …, Internetadresse, Bericht … etc.)",
            ],
            [
              "name" => "arbeitsbereich_sektor",
              "type" => "textarea", "sanitize" => "textarea_field",
              "label" => "Arbeitsbereiche/Sektoren, in denen die/der Praktikant*in
                tätig sein wird (max. 2 Zeilen)",
              "explanation" => "Beispiel: Arbeit mit Straßenkindern /Kinderarbeiter*innen,
                Gemeinwesenarbeit, Gesundheit, Arbeit mit Mädchen und
                Frauen in der Sexarbeit, ….
                Bitte Quelle angeben (Einsatzorganisation, Schreiben der
                Praktikumsstelle vom …, Internetadresse, Bericht … etc.)",
            ],
            [
              "name" => "arbeitsbereich_anforderung",
              "type" => "textarea", "sanitize" => "textarea_field",
              "label" => "Tätigkeitsbereich / Anforderungsprofil Praktikant*in (mind. 5 – höchstens 10 Zeilen)",
              "explanation" => "
                Beispiel: Mitgestaltung des Alltags der Kinder (Spiele, Essen etc.),
                Übernehmen von kleineren administrativen Aufgaben (Ansuchen um
                finanzielle Unterstützung etc.), Kontaktaufnahme mit
                Sozialarbeiter*innen der Organisation, ….
                Bitte Quelle angeben (Einsatzorganisation, Schreiben der
                Praktikumsstelle vom …, Internetadresse, Bericht … etc.)",
            ],
            [
              "name" => "erfahrung",
              "type" => "textarea", "sanitize" => "textarea_field",
              "label" => "Bisherige Erfahrung",
              "explanation" => "(Tätigkeit der/des Studierenden im
                Sozial- und Gesundheitsbereich / EZA-Bereich; auch einschlägige
                Auslandsaufenthalte, Praktika)",
            ],
          ]
        ],
        [ // Sprachkenntnisse
          "type" => "section",
          "header" => "Sprachkenntnisse",
          "fields" => [
            [
              "name" => "sprache",
              "label" => "Sprache Einsatzland", "type" => "text",
              "sanitize" => "text_field",
            ],
            [
              "name" => "sprachkenntnis",
              "label" => "Aktuelle Sprachkenntnisse laut europ. Referenzrahmen (geschätzt)",
              "type" => "radio",
              "options" => Database::get_radio_options_sprachkenntnisse(),
              "validate" => "options"
            ],
            [
              "name" => "sprachkenntnis_testergebnis",
              "type" => "dropdown",
              "label" => "Aktuelle Sprachkenntnisse (nach europ. Referenzrahmen)
                laut Sprachtest",
              "explanation" => "Bitte geben Sie hier das Testergebnis Ihres
                Sprachtests im europäischen Sprachenportfolio Referenzrahmen
                A1 bis C2 an und laden Sie Ihr Testergebnis weiter unten im
                Bereich „Hochladen der Anmeldeunterlagen“ als Datei hoch",
              "options" => Database::get_dropdown_array_sprechkenntnisse(),
            ],
            [
              "name" => "sprachkenntnis_planung",
              "type" => "textarea", "sanitize" => "textarea_field",
              "label" => "Sollte dieses Ergebnis unter B2 liegen, führen Sie hier
                bitte genau an, welche Maßnahmen Sie unternehmen werden, um dieses
                Niveau spätestens bis zum Termin des Vorbereitungsseminars zu
                erreichen und wenn nötig nachträglich nachweisen zu können",
            ],
            [
              "name" => "sprachkurs",
              "label" => "Sprachkurs vor Ort", "type" => "language_course",
              "validate" => "checkbox",
              "fields_if_yes" => [
                "sprachkurs_institut" => [ "label" => "Sprachkurs Institut" ],
                "sprachkurs_institut_website" => [ "label" => "Sprachkurs Instituts-Website" ],
                "sprachkurs_beginn" => [ "label" => "Sprachkurs Beginn", "type" => "date" ],
                "sprachkurs_ende" => [ "label" => "Sprachkurs Ende", "type" => "date" ],
              ]
            ],
          ]
        ],
        [ // Zuschüsse
          "type" => "section",
          "header" => "Zuschussansuchen",
          "fields" => [
            [
              "name" => "oeza_zuschuss",
              "type" => "checkbox", "validate" => "checkbox",
              "label" => "ÖZA Zuschuss:",
            ],
            [
              "name" => "oeza_zuschuss_ergaenzend",
              "type" => "checkbox", "validate" => "checkbox",
              "label" => "Ergänzender OEZA-Zuschuss:",
            ],
          ]
        ],
        [ // Verpflichtungserklaerung
          "type" => "section",
          "header" => "Verpflichtungserklaerung",
          "fields" => [
            [
              "name" => "datenverarbeitung",
              "type" => "checkbox", "validate" => "checkbox",
              "label" => "Stimmt erweiterter Datenverarbeitung zu:",
            ],
            [
              "name" => "verpflichtungserklaerung",
              "type" => "checkbox", "validate" => "checkbox",
              "label" => "Stimmt den genannten Bedingungen zu:",
            ],
          ]
        ],
        [ // Kohorte
          "type" => "section",
          "header" => "Kohorte",
          "fields" => [
            [
              "type" => "dropdown",
              "name" => "kohorte_id", "label" => "Kohorte", "mandatory" => true,
              "options" => Database::get_dropdown_array_kohorte(),
              "validate" => "options"
            ],
          ]
        ],
      ]
    ];
  }

  public static function render () {
    ?>
    <h1>GSD Datenbank: Teilnehmer*in bearbeiten</h1>
    <?php
    // check whether ID is valid
    if ( empty( $_GET["id"] ) ) {
      echo Plugin::render_error('The ID parameter is missing!');
      return;
    }
    if ( ! preg_match( '/^[a-z0-9]+$/', $_GET["id"] ) ) {
      echo Plugin::render_error('Invalid participant ID!');
      return;
    }
    // set validated id
    $id = $_GET["id"];

    // now check if data for this participant ID exists and generate object
    global $wpdb;
    $tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
    $q = "SELECT * FROM $tbl_teilnehmerin WHERE matrikelnr = %s";
    $q = $wpdb->prepare( $q, $id );
    $row = $wpdb->get_row( $q, ARRAY_A );
    if ( empty( $row ) ) {
      echo Plugin::render_error(
        'No data exists for this participant!',
        'Participant data empty for requested ID. GET data: ' . json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }

    $editor = new Participant_Editor( $row );
    $editor->init_form();
    $editor->process_input();
    $editor->output_form();
  }

  public function init_form () {
    $this->form = new Form( $this->form_config );
    $this->form->load_fixed_data( $this->participant );
  }

  public function process_input () {
    // if post data was already submitted, we'll process it
    if ( isset( $_POST[ $this->form_config["slug"] ] ) ) {
      if ( ! $this->form->validate_nonce( $_POST ) ) {
        echo Notification::nonce();
        return;
      }
      $this->form->load_data( $_POST );

      // check if there have been any errors with the submitted data
      if ( ! empty( $this->form->errors ) ) {
        if ( ! empty( $this->form->errors["missing"] ) ) {
          echo Notification::field_issues(
            "Folgende Pflichtfelder sind nicht (oder falsch) ausgefüllt worden:",
            $form->errors["missing"],
          );
        }
        if ( ! empty( $this->form->errors["validation"] ) ) {
          echo Notification::field_issues(
            "Folgende Felder enthalten noch ungültige Zeichen:",
            $this->form->errors["validation"],
          );
        }
        return;
      }

      // no validation errors happened so far, let's attempt to store the data now
      // if nothing was updated we'll receive 0, otherwise 1 or more.
      // but in case of an error we'll receive false
      $retval = $this->store( $this->form_config, $this->form->get_submitted_data() );
      if ( $retval === false ) {
        echo Notification::error([
          "message" => "Fehler beim Schreiben der Teilnehmer*innendaten in die Datenbank!<br>
          Bitte notiere die aktuelle Zeit und kontaktiere damit die Plugin-Entwicklerin."
        ]);
      } else if ( $retval === 0 ) {
        echo Notification::warning([
          "message" => "Daten wurden nicht verändert. (Leere Felder werden nicht aktualisiert!)"
        ]);
      } else {
        // When the update was successfull, we render a corresponding message
        echo Notification::success([
          "message" => "Daten aktualisiert."
        ]);
        // We also have to check, whether the comment for the participant has changed
        // and notify them in case there is new information
        $comment_old = $this->participant["comment_for_participant"];
        $comment_new = $this->form->get_submitted_data()["comment_for_participant"];
        if ( $comment_new !== $comment_old ) {
          if ( ! empty( $comment_new ) ) {
            $msg = "Liebe*r {$this->participant["firstname"]} {$this->participant["lastname"]},\n";
            if ( empty( $comment_old ) ) {
              $msg .= "\nDas GSD-Team hat dir einen Kommentar zu deinem GSD-Fixanmeldedatenblatt\n".
                "hinterlegt. ";
            } else {
              $msg .= "\nder Kommentar zu deinem GSD-Datensatz wurde von der Projektleitung\n".
                "aktualisiert. ";
            }
            $msg .= "Du kannst den Kommentar in deinem Datensatz sehen\n".
              "in dem du die GSD-Website besuchst, zum Formular für die\n".
              "Fixanmeldung gehst und dich mit deinem GSD-Account Passwort\n".
              "einloggst.\n".
              "\n".
              "Bitte ändere/ergänze dein Fixanmeldedatenblatt entsprechend\n".
              "und gib per Mail Bescheid, dass du die Änderung/Ergänzung\n".
              "durchgeführt hast, an: gsd.praktika@gmail.com\n".
              "\n".
              "Link zur Fixanmeldung: \n".
              home_url()."/anmeldung/fixanmeldung/fixanmelde-datenblatt-inkl-verpflichtungserklarung/\n" .  // TODO: make this configurable
              "\n" .
              "Liebe Grüße,\n" .
              "das GSD Team";
            wp_mail($this->participant["email"], "Neuer Kommentar zu deinem GSD Praktikum", $msg);
          }
        }
      }
    }

    // if nothing was yet submitted, we have to still load data from the db
    else {
      $this->form->load_data( $this->participant );
    }
  }

  public function output_form () {
    echo $this->form->render();

    // uploaded files and emergency contacts are only displayed but need not
    // be edited here
    ?>
    <hr>
    <h2>Zusatzinfos: Hochgeladene Files & Notfallkonakte</h2>

    Files:

    <ul>
    <?php
    if ( !$this->files ) {
      echo "<li>Es wurden noch keine Files hochgeladen.</li>";
    } else {
      $types = [
        "picture" => [
          "label" => "Photo",
          "dashicon" => "dashicons-format-image",
        ],
        "motivation" => [
          "label" => "Motivationsschreiben",
          "dashicon" => "dashicons-text-page",
        ],
        "identity" => [
          "label" => "Identitätsnachweis",
          "dashicon" => "dashicons-id",
        ],
        "languagetest" => [
          "label" => "Sprachtestergebnis",
          "dashicon" => "dashicons-editor-spellcheck",
        ],
      ];
      foreach ( $types as $type => $type_meta ) {
        foreach ( $this->files as $file ) {
          if ( $file["type"] === $type ) {
            $path = GSD_UPLOAD_URL . '/' . $file["filepath"];
            echo '
            <li>
              <a href="'.$path.'" title="'.$type_meta["label"].'">
                <span class="dashicons '.$type_meta["dashicon"].'"></span>
                '.$type_meta["label"].'
              </a>
              (zuletzt hochgeladen: ' . $file["updated"] . ')
            </li>';
          }
        }
      }
    }

    ?>
    </ul>

    Notfallkontakte:

    <ul>
      <?php
      if ( $this->contacts_local ) {
        $c = $this->contacts_local;
        $line = "<i>Daheim:</i> ";
        if ( $c["fullname"] ) $line .= $c["fullname"];
        else $line .= '<i>(no name set)</i>';
        if ( $c["adresse"] ) $line .= ', '. $c["adresse"];
        if ( $c["email"] ) $line .= ', <b>email</b>: <a href="mailto:'.$c["email"].'">'.$c["email"].'</a>';
        if ( $c["telefon"] ) $line .= ', <b>tel</b>: '.$c["telefon"];
        if ( $c["skype"] ) $line .= ', <b>skype</b>: '.$c["skype"];
        if ( $c["beziehungsgrad"] ) $line .= ', <b>Beziehungsgrad</b>: '.$c["beziehungsgrad"];
        if ( $c["anmerkungen"] ) $line .= ', <b>Anmerkungen</b>: '.$c["anmerkungen"];
        echo "
        <li>$line</li>";
      }
      if ( $this->contacts_remote ) {
        $c = $this->contacts_remote;
        $line = "<i>Im Einsatzland:</i> ";
        if ( $c["fullname"] ) $line .= $c["fullname"];
        else $line .= '<i>(no name set)</i>';
        if ( $c["adresse"] ) $line .= ', '. $c["adresse"];
        if ( $c["email"] ) $line .= ', <b>email</b>: <a href="mailto:'.$c["email"].'">'.$c["email"].'</a>';
        if ( $c["telefon"] ) $line .= ', <b>tel</b>: '.$c["telefon"];
        if ( $c["skype"] ) $line .= ', <b>skype</b>: '.$c["skype"];
        if ( $c["beziehungsgrad"] ) $line .= ', <b>Beziehungsgrad</b>: '.$c["beziehungsgrad"];
        if ( $c["anmerkungen"] ) $line .= ', <b>Anmerkungen</b>: '.$c["anmerkungen"];
        echo "
        <li>$line</li>";
      }
      if ( !$this->contacts_local && !$this->contacts_remote ) {
        echo "<li>Es wurden noch keine Angaben zu Notfallkontakten gemacht.</li>";
      }
      ?>
    </ul>
    <?php
  }

  private function store ( $section, $data ) {
    global $wpdb;

    // empty array of column names and values to be updated
    $update_data = [];

    // those fields can be directly taken from the validated
    // form data, if they are not empty
    $take_if_not_empty = [];
    foreach ( $section["fields"] as $field ) {
      if ( $field["type"] == "section" ) {
        foreach ( $field["fields"] as $f ) {
          // only named fields should be stored
          if ( isset( $f["name"] ) ) {
            $take_if_not_empty[] = $f["name"];
          }
        }
      } else {
        // only named fields should be stored
        if ( isset( $field["name"] ) ) {
          $take_if_not_empty[] = $field["name"];
        }
      }
    }
    foreach ( $take_if_not_empty as $key ) {
      if ( isset( $data[$key] ) && $data[$key] !== '' ) {
        $update_data[$key] = $data[$key];
      }
    }

    // if all fields are empty, nothing gets updated
    if ( empty( $update_data ) ) {
      return 0;
    }

    // TODO: check this  when adding the forms in $this->output_forms()
    // in case we are handling an emergency contact entry
    $contact = false;
    if ( isset($_POST["notfallkontakte_local"]) ) $contact = 'local';
    if ( isset($_POST["notfallkontakte_remote"]) ) $contact = 'remote';
    if ( $contact ) {
      $table = $wpdb->prefix . GSD_TABLE_NOTFALLKONTAKT;
      $where = [ "teilnehmerin_id" => $this->participant["id"] ];
      // if there is only the one hidden field set, we'll treat it as unchanged
      if ( count($update_data) == 1 ) {
        return 0;
      }
      // now we have to chek if there already is an entry to update,
      // otherwise we have to insert
      if ( $contact == 'local' ) {
        if ( empty( $this->contacts_local ) ) {
          $update_data["teilnehmerin_id"] = $this->participant["id"];
          return $wpdb->insert( $table, $update_data );
        } else {
          return $wpdb->update( $table, $update_data, $where );
        }
      } else {
        if ( empty( $this->contacts_remote ) ) {
          $update_data["teilnehmerin_id"] = $this->participant["id"];
          return $wpdb->insert( $table, $update_data );
        } else {
          return $wpdb->update( $table, $update_data, $where );
        }
      }
    }

    // TODO: check this  when adding the forms in $this->output_forms()
    // in case we are handling file uploads
    else if ( isset($_POST["files"]) ) {
      $update_count = 0;
      foreach ( $update_data as $key => $file ) {
        // create the target filename and move the uploaded file
        $target_base = GSD_UPLOAD_DIR . '/';
        $target_file = $this->participant["matrikelnr"] . '_' .
          $key . '_' . date('Y-m-d_H-i-s') . '_' . $file["name"];
        move_uploaded_file( $file["tmp_name"], $target_base.$target_file );

        // check if there is already an existing entry
        $updating = false;
        foreach ( $this->files as $f ) {
          if ( $f["type"] == $key ) {
            $updating = true;
          }
        }

        // now inser/update the file entry in the databse
        $table = $wpdb->prefix . GSD_TABLE_FILE;
        $where = [
          "teilnehmerin_id" => $this->participant["id"],
          "type" => $key,
        ];
        $values = [
          "teilnehmerin_id" => $this->participant["id"],
          "type" => $key,
          "filepath" => $target_file,
          "updated" => date( "c" ),
        ];
        if ( $updating ) {
          $num = $wpdb->update( $table, $values, $where );
        } else {
          $num = $wpdb->insert( $table, $values );
        }
        if ( $num === false ) {
          return false;
        }
        $update_count += $num;
      }
      return $update_count;
    }

    // changing plain participant data is much more easy, because we know that
    // there is already an entry
    else {
      // we only have to check einsatzorga_id if it is "OTHER"
      if ( isset( $data["einsatzorga_id"] ) && $data["einsatzorga_id"] == "OTHER" ) {
        $update_data["einsatzorga_id"] = null;
        // TODO: this is hardcoded now due to time availability; should be automated in improved version
        if ( ! empty( $data["einsatzorga_other_name"] ) ) $update_data["einsatzorga_other_name"] = $data["einsatzorga_other_name"];
        if ( ! empty( $data["einsatzorga_other_website"] ) ) $update_data["einsatzorga_other_website"] = $data["einsatzorga_other_website"];
      }
      // and for language courses we also have to pull in the alternative data
      if ( isset( $data["sprachkurs"] ) ) {
        $update_data["sprachkurs"] = $data["sprachkurs"];
        // TODO: this is hardcoded now due to time availability; should be automated in improved version
        if ( ! empty( $data["sprachkurs_institut"] ) ) $update_data["sprachkurs_institut"] = $data["sprachkurs_institut"];
        if ( ! empty( $data["sprachkurs_institut_website"] ) ) $update_data["sprachkurs_institut_website"] = $data["sprachkurs_institut_website"];
        if ( ! empty( $data["sprachkurs_beginn"] ) ) $update_data["sprachkurs_beginn"] = $data["sprachkurs_beginn"];
        if ( ! empty( $data["sprachkurs_ende"] ) ) $update_data["sprachkurs_ende"] = $data["sprachkurs_ende"];
      }
      // now we can update the row and return the number of rows (should be 1 or
      // 0 in case nothing changed)
      $table = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
      $where = [ "id" => $this->participant["id"] ];
      $update_data["updated"] = date( "c" );
      return $wpdb->update($table, $update_data, $where );
    }
  }

}
