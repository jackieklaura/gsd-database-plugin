<?php
namespace GSDDB\Admin;

class Settings
{
  private $lecturers = [];
  private $schools = [];
  private $cohorts = [];
  private $uri_form = '';

  public function __construct () {
    global $wpdb;

    // initialise schools
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_HOCHSCHULE);
    foreach ( $r as $row ) {
      $this->schools[ $row->id ] = $row->name;
    }

    // initialise cohorts
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_KOHORTE);
    foreach ( $r as $row ) {
      $this->cohorts[ $row->id ] = [
        'label' => $row->label,
        'active' => $row->active,
        'schools' => [],
      ];
    }
    $cohorts_schools = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . GSD_TABLE_KOHORTE_HOCHSCHULE);
    foreach ( $cohorts_schools as $row ) {
      $this->cohorts[$row->kohorte_id]['schools'][] = $row->hochschule_id;
    }

    // initialise lecturers
    $lecturers = get_users(['role' => 'lecturer']);
    foreach ($lecturers as $lecturer) {
      $new_lecturer = [
        'id' => $lecturer->data->ID,
        'login' => $lecturer->data->user_login,
        'name' => $lecturer->data->display_name,
        'email' => $lecturer->data->user_email,
        'school' => false,
      ];
      $tbl_lecturer = $wpdb->prefix . GSD_TABLE_LECTURER;
      $query = "SELECT * FROM $tbl_lecturer WHERE wp_user = %d";
      $query = $wpdb->prepare($query, $lecturer->data->ID);
      $result = $wpdb->get_row($query);
      if ($result) {
        $new_lecturer['school'] = $result->hochschule_id;
      }
      $this->lecturers[] = $new_lecturer;
    }

    // initialise uri used for form actions
    $this->uri_form = $_SERVER["REQUEST_URI"];
  }

  public static function render () {
    $v = new Settings();
    $v->process_edit();
    ?>
    <div class="wrap">
      <h1>GSD Einstellungen</h1>
      <h2>Lektor*innenverwaltung</h2>
      <div>
        <?= $v->render_lecturers() ?>
      </div>
      <h2>Hochschulen</h2>
      <div>
        <?= $v->render_schools() ?>
      </div>
      <h2>Kohorten</h2>
      <div>
        <?= $v->render_cohorts() ?>
      </div>
    </div>
    <?php
    // TODO: add a check for entries in the lecturers table that have no
    //       corresponding user any more (and clean them up)
  }

  public function process_edit () {
    global $wpdb;

    // in case no action was provided, we don't process any submitted data
    if (empty($_GET['action'])) return;

    // updating lecturers
    if ($_GET['action'] === 'update-lecturer') {
      if (! $this->validate_id() ) return;
      if (! $this->validate_nonce() ) return;
      if (! $this->validate_school_id() ) return;
      $id = $_GET['id'];
      $school = $_GET['school'];

      // check whether there is an actual change in the data
      foreach ($this->lecturers as $idx => $lecturer) {
        if ($lecturer['id'] == $id) {
          if ($this->lecturers[$idx]['school'] == $school) {
            // school id is same as before so, we don't have to update anything
            return;
          }
          break;
        }
      }

      $set = [
        'wp_user' => $id,
        'hochschule_id' => $school,
      ];
      $where = ['wp_user' => $id];
      $updated = $wpdb->update($wpdb->prefix.GSD_TABLE_LECTURER, $set, $where);
      if (!$updated) {
        $updated = $wpdb->insert($wpdb->prefix.GSD_TABLE_LECTURER, $set);
      }
      if (!$updated) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. id: '.$id.' school: '.$school,
          __FILE__, __LINE__
        );
      } else {
        $this->lecturers[$idx]['school'] = $school;
      }
    }

    // updating school
    elseif ($_GET['action'] === 'update-school') {
      if (! $this->validate_id() ) return;
      if (! $this->validate_nonce() ) return;
      if (! $this->validate_school_name() ) return;

      $id = $_GET['id'];
      $name = $_GET['name'];

      // if the schools name hasn't changed or is empty, we can return
      if ( $name === $this->schools[$id] || $name === '' ) {
        return;
      }

      $set = ['name' => $name];
      $where = ['id' => $id];
      $updated = $wpdb->update($wpdb->prefix.GSD_TABLE_HOCHSCHULE, $set, $where);
      if (!$updated) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. id: '.$id.' school: '.$name,
          __FILE__, __LINE__
        );
      } else {
        $this->schools[$id] = $name;
      }

    }

    // adding school
    elseif ($_GET['action'] === 'add-school') {
      if (! $this->validate_nonce() ) return;
      if (! $this->validate_school_name() ) return;

      $name = $_GET['name'];
      if ($name === '') return;

      $i = $wpdb->insert($wpdb->prefix.GSD_TABLE_HOCHSCHULE, ['name' => $name]);
      if(!$i) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. school: '.$name,
          __FILE__, __LINE__
        );
      } else {
        $this->schools[$wpdb->insert_id] = $name;
      }
    }

    // deleting school
    elseif ($_GET['action'] === 'delete-school') {
      if (! $this->validate_id() ) return;
      if (! $this->validate_nonce() ) return;

      $id = $_GET['id'];

      // check if school is used in any cohorts before deleting it
      $table = $wpdb->prefix . GSD_TABLE_KOHORTE_HOCHSCHULE;
      $query = "SELECT * FROM $table WHERE hochschule_id = %d";
      $query = $wpdb->prepare($query, $id);
      $r = $wpdb->get_results($query);
      if ($r) {
        $name = $this->schools[$id];
        echo $this->render_error("'$name' kann nicht gelöscht werden, da sie noch in mindestens einer Kohorte verwendet wird.");
        return;
      }

      $i = $wpdb->delete($wpdb->prefix.GSD_TABLE_HOCHSCHULE, ['id' => $id]);
      if(!$i) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. id: '.$id,
          __FILE__, __LINE__
        );
      } else {
        unset($this->schools[$id]);
      }
    }

    // rename cohort
    elseif ($_GET['action'] === 'rename-cohort') {
      if (! $this->validate_id() ) return;
      if (! $this->validate_nonce() ) return;
      if (! $this->validate_cohort_name() ) return;

      $id = $_GET['id'];
      $name = $_GET['name'];

      // if the cohort name hasn't changed or is empty, we can return
      if ( $name === $this->cohorts[$id]['label'] || $name === '' ) {
        return;
      }

      $set = ['label' => $name];
      $where = ['id' => $id];
      $updated = $wpdb->update($wpdb->prefix.GSD_TABLE_KOHORTE, $set, $where);
      if (!$updated) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. id: '.$id.' name: '.$name,
          __FILE__, __LINE__
        );
      } else {
        $this->cohorts[$id]['label'] = $name;
      }
    }

    // adding cohort
    elseif ($_GET['action'] === 'add-cohort') {
      if (! $this->validate_nonce() ) return;
      if (! $this->validate_cohort_name() ) return;

      $name = $_GET['name'];
      if ($name === '') return;

      $i = $wpdb->insert($wpdb->prefix.GSD_TABLE_KOHORTE, ['label' => $name, 'active' => 0]);
      if(!$i) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. cohort name: '.$name,
          __FILE__, __LINE__
        );
      } else {
        $this->cohorts[$wpdb->insert_id] = ['label' => $name, 'active' => 0, 'schools' => []];
      }
    }

    // switching a cohorts active flag
    elseif ($_GET['action'] === 'switch-cohort-active-flag') {
      if (! $this->validate_id() ) return;
      if (! $this->validate_nonce() ) return;

      $id = $_GET['id'];
      $active_flag = $this->cohorts[$id]['active'] ? 0 : 1;

      $set = ['active' => $active_flag];
      $where = ['id' => $id];
      $updated = $wpdb->update($wpdb->prefix.GSD_TABLE_KOHORTE, $set, $where);
      if (!$updated) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. id: '.$id,
          __FILE__, __LINE__
        );
      } else {
        $this->cohorts[$id]['active'] = $active_flag;
      }
    }

    // removing a school from a cohort
    elseif ($_GET['action'] === 'remove-school-from-cohort') {
      if (! $this->validate_id() ) return;
      if (! $this->validate_nonce() ) return;
      if (! $this->validate_school_id() ) return;

      $id = $_GET['id'];
      $school = $_GET['school'];

      $where = [
        'kohorte_id' => $id,
        'hochschule_id' => $school,
      ];
      $i = $wpdb->delete($wpdb->prefix.GSD_TABLE_KOHORTE_HOCHSCHULE, $where);
      if(!$i) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. id: '.$id.' school: '.$school,
          __FILE__, __LINE__
        );
      } else {
        $i = array_search($school, $this->cohorts[$id]['schools']);
        unset($this->cohorts[$id]['schools'][$i]);
      }
    }

    // add a school to a cohort
    elseif ($_GET['action'] === 'add-school-to-cohort') {
      if (! $this->validate_id() ) return;
      if (! $this->validate_nonce() ) return;
      if (! $this->validate_school_id() ) return;

      $id = $_GET['id'];
      $school = $_GET['school'];

      // check if the school already is in the cohort, in which case we can return
      if (in_array($school, $this->cohorts[$id]['schools'])) return;

      $data = [
        'kohorte_id' => $id,
        'hochschule_id' => $school,
      ];
      $i = $wpdb->insert($wpdb->prefix.GSD_TABLE_KOHORTE_HOCHSCHULE, $data);
      if(!$i) {
        echo $this->render_error(
          'Error writing to databases. Contact system admin with date and time.',
          'Error writing to databases. id: '.$id.' school: '.$school,
          __FILE__, __LINE__
        );
      } else {
        $this->cohorts[$id]['schools'][] = $school;
      }
    }
  }

  public function render_lecturers () {
    $uri_form = $_SERVER["REQUEST_URI"] . '?page=' . $_GET["page"];
    ?>
    <table class="widefat importers striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Login</th>
          <th>Name</th>
          <th>e-Mail</th>
          <th>Fachhochschule</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($this->lecturers as $lecturer) : ?>
        <tr>
          <td><?= $lecturer['id'] ?></td>
          <td><?= $lecturer['login'] ?></td>
          <td><?= $lecturer['name'] ?></td>
          <td><?= $lecturer['email'] ?></td>
          <td>
            <form action="<?= $uri_form ?>" method="GET">
              <input type="hidden" name="page" value="gsd_menu_settings">
              <input type="hidden" name="action" value="update-lecturer">
              <input type="hidden" name="id" value="<?= $lecturer['id'] ?>">
              <select name="school">
                <?php
                foreach ($this->schools as $value => $school) {
                  $selected = $lecturer['school'] == $value ? ' selected="selected"': '';
                  echo '<option value="'.$value.'"'.$selected.'>'.$school.'</option>';
                }
                ?>
              </select>
              <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
              <input type="submit" value="Update" />
              <?php
              if ($lecturer['school'] === false) {
                echo ' <small><i>(noch nicht eingetragen)</i></small>';
              }
              ?>
            </form>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php
  }

  public function render_schools () {
    ?>
    <table class="widefat importers striped">
      <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Aktionen</th>
      </thead>
      <tbody>
        <?php foreach ($this->schools as $id => $school) : ?>
        <tr>
          <td><?= $id ?></td>
          <td><?= $school ?></td>
          <td>
            <form action="<?= $this->uri_form ?>" method="GET" style="display: inline-block;">
              <input type="hidden" name="page" value="gsd_menu_settings">
              <input type="hidden" name="action" value="update-school">
              <input type="hidden" name="id" value="<?= $id ?>">
              <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
              <input type="text" name="name" value="<?= $school ?>">
              <input type="submit" value="Update" />
            </form>
            <form action="<?= $this->uri_form ?>" method="GET" style="display: inline-block;">
              <input type="hidden" name="page" value="gsd_menu_settings">
              <input type="hidden" name="action" value="delete-school">
              <input type="hidden" name="id" value="<?= $id ?>">
              <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
              <input type="submit" value="Löschen" />
            </form>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <p>
      Neue Schule hinzufügen:
      <form action="<?= $this->uri_form ?>" method="GET">
        <input type="hidden" name="page" value="gsd_menu_settings">
        <input type="hidden" name="action" value="add-school">
        <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
        <input type="text" name="name">
        <input type="submit" value="Hinzufügen" />
      </form>
    </p>
    <?php
  }

  public function render_cohorts () {
    ?>
    <table class="widefat importers striped">
      <thead>
        <th>ID</th>
        <th>Label</th>
        <th>Aktiv?</th>
        <th>Aktionen</th>
        <th>Hochschulen</th>
      </thead>
      <tbody>
        <?php foreach ($this->cohorts as $id => $cohort) : ?>
        <?php
          $active_label = $cohort['active'] ? '<span class="dashicons dashicons-yes-alt"></span>' : '';
          $button_label = $cohort['active'] ? 'Deaktivieren' : 'Aktivieren';
        ?>
        <tr>
          <td><?= $id ?></td>
          <td><?= $cohort['label'] ?></td>
          <td style="text-align: center;"><?= $active_label ?></td>
          <td>
            <form action="<?= $this->uri_form ?>" method="GET" style="display: inline-block;">
              <input type="hidden" name="page" value="gsd_menu_settings">
              <input type="hidden" name="action" value="switch-cohort-active-flag">
              <input type="hidden" name="id" value="<?= $id ?>">
              <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
              <input type="submit" value="<?= $button_label ?>" />
            </form>
            <form action="<?= $this->uri_form ?>" method="GET" style="display: inline-block;">
              <input type="hidden" name="page" value="gsd_menu_settings">
              <input type="hidden" name="action" value="rename-cohort">
              <input type="hidden" name="id" value="<?= $id ?>">
              <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
              <input type="text" name="name" value="<?= $cohort['label'] ?>" maxlength="8">
              <input type="submit" value="Umbenennen" />
            </form>
          </td>
          <td>
            <?php foreach ($cohort['schools'] as $school_id) : ?>
              <?php
              $uri = $this->uri_form;
              $uri .= "&action=remove-school-from-cohort&id=$id&school=$school_id";
              $uri .= "&_wpnonce=" . wp_create_nonce('gsd-settings');
              echo $this->schools[$school_id];
              ?>
              <a href="<?= $uri ?>" title="Hochschule aus Kohorte entfernen">
                <small><span class="dashicons dashicons-dismiss"></span></small>
              </a>
              <br>
            <?php endforeach; ?>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <p>
      <form action="<?= $this->uri_form ?>" method="GET">
        <input type="hidden" name="page" value="gsd_menu_settings">
        <input type="hidden" name="action" value="add-school-to-cohort">
        <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
        Hochschule
        <select name="school">
          <?php
          foreach ($this->schools as $school_id => $school) {
            echo '<option value="'.$school_id.'">'.$school.'</option>';
          }
          ?>
        </select>
        zu Kohorte
        <select name="id">
          <?php
          foreach ($this->cohorts as $id => $cohort) {
            $selected = !empty($_GET['id']) && $_GET['id'] == $id ? 'selected' : '';
            echo '<option value="'.$id.'" '.$selected.'>'.$cohort['label'].'</option>';
          }
          ?>
        </select>
        <input type="submit" value="hinzufügen" />
      </form>
    </p>
    <p>
      <form action="<?= $this->uri_form ?>" method="GET">
        <b>Neue Kohorte</b> (max. 8 Zeichen):
        <input type="hidden" name="page" value="gsd_menu_settings">
        <input type="hidden" name="action" value="add-cohort">
        <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('gsd-settings') ?>">
        <input type="text" name="name" maxlength="8" size="10">
        <input type="submit" value="hinzufügen" />
      </form>
      <small>
        <b>Disclaimer:</b> Kohorten können nicht gelöscht werden, solange die Archiv-Funktion noch nicht implementiert ist.
        Sollte eine Kohorte dennoch versehentlich angelegt worden, kann sie direkt aus der Datenbank gelöscht werden.
        Bitte dazu d* Website-Administrator*in oder Entwickler*in kontaktieren.
      </small>
    </p>
    <?php
  }

  public function render_error($msg, $log_msg=false, $file=false, $line=false) {
    if ($log_msg) {
      $m = '[GSD error]';
      if ( $file ) {
        $m .= ' '.$file;
      }
      if ( $line ) {
        $m .= $line;
      }
      $m .= ': ' . $log_msg;
      error_log($m);
    }
    $html = '<div class="error">'.$msg.'</div>';
    return $html;
  }

  public function validate_id () {
    if (empty($_GET['id']) || !preg_match('/^[0-9]+$/', $_GET["id"])) {
      echo $this->render_error(
        'Invalid request due to invalid or missing id parameter.',
        'Invalid request due to invalid or missing id parameter. GET data: '.json_encode($_GET),
        __FILE__, __LINE__
      );
      return false;
    }
    return true;
  }

  public function validate_nonce () {
    if (empty($_GET['_wpnonce']) || !wp_verify_nonce($_GET['_wpnonce'], 'gsd-settings')) {
      echo $this->render_error(
        'Invalid request due to invalid or missing nonce.',
        'Invalid request due to invalid or missing nonce. GET data: '.json_encode($_GET),
        __FILE__, __LINE__
      );
      return false;
    }
    return true;
  }

  public function validate_school_id () {
    if (empty($_GET['school']) || !preg_match('/^[0-9]+$/', $_GET['school'])) {
      echo $this->render_error(
        'Invalid or missing school parameter.',
        'Invalid or missing school parameters. GET data: '.json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }
    return true;
  }

  public function validate_school_name () {
    if (!isset($_GET['name'])) {
      echo $this->render_error(
        'Missing name parameter.',
        'Missing name parameters. GET data: '.json_encode($_GET),
        __FILE__, __LINE__
      );
      return;
    }
    $_GET['name'] = sanitize_text_field($_GET['name']);
    return true;
  }

  public function validate_cohort_name () {
    return $this->validate_school_name();
  }

}
