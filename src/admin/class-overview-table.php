<?php
namespace GSDDB\Admin;

class Overview_Table extends \WP_List_Table {
  private $participants;
  private $view;
  private $type;

  public function __construct($participants, $type, $view) {
    parent::__construct();
    $this->participants = $participants;
    $this->view = $view;
    $this->type = $type;

    $this->_column_headers = [
      $this->get_columns(),
      array(),  // hidden fields
      $this->get_sortable_columns(),
      'actions'  // primary column with actions
    ];
  }

  // overriding the parent method just to get rid of the fixed class
  protected function get_table_classes() {
    $mode = get_user_setting( 'posts_list_mode', 'list' );
    $mode_class = esc_attr( 'table-view-' . $mode );
    return array( 'widefat', 'striped', $mode_class, $this->_args['plural'] );
  }

  public function get_columns() {
    return [
      'matrikelnr' => 'Matrikelnr',
      'firstname' => 'Vorname',
      'lastname' => 'Nachname',
      'email' => 'e-Mail',
      'school' => 'Fachhochschule',
      'teacher' => 'FH-Praxislehrende',
      'einsatzorga' => 'Einsatzinfos',
      'praktikumsstelle' => 'Praktikumsstelle',
      'language' => 'Sprache',
      'travel' => 'Reisedaten',
      'funding' => 'Zuschuss',
      'files' => 'Files',
      'timestamp' => 'Zeitstempel',
      'actions' => 'Aktionen',
    ];
  }

  public function get_sortable_columns() {
    return [
      'firstname' => ['firstname', false],
      'lastname' => ['lastname', false],
      'school' => ['hochschule.name', false],
      'teacher' => ['lektorin', false],
      'einsatzorga' => ['einsatzorga_id', false],
      'praktikumsstelle' => ['praktikumsstelle', false],
      'travel' => ['ausreise', false],
      'timestamp' => ['updated', false],
    ];
  }

  public function column_default($item, $column_name) {
    return $item[$column_name];
  }

  public function prepare_items() {
    $this->items = [];
    foreach ($this->participants as $p) {
      // set the basic column contents
      $item = [
        'id' => $p->id,
        'matrikelnr' => $p->matrikelnr,
        'firstname' => $p->firstname,
        'lastname' => $p->lastname,
        'email' => $p->email ? '<a href="mailto:'.$p->email.'">'.$p->email.'</a>' : '',
        'email_plain' => $p->email,
        'school' => $this->view->schools[$p->hochschule_id].'<br>'.$p->studium,
        'teacher' => $p->lektorin,
        'einsatzorga' => $p->einsatzorga_id ? '' : 'Andere Einsatzorganisation:<br>',
        'praktikumsstelle' => $p->praktikumsstelle,
        'language' => $p->sprache.'<br>Level: '.$p->sprachkenntnis,
        'travel' => '',
        'funding' => 'OEZA: ',
        'files' => '',
        'timestamp' => 'Erstellt: <span class="nowrap">'.$p->created.'</span><br>Verändert: <span class="nowrap">'.$p->updated.'</span>',
      ];
      // add lecturer e-mail
      if (!empty($p->lektorin_email)) {
        $item['teacher'] .= ' &lt;<a href="mailto:'.$p->lektorin_email.'">'.$p->lektorin_email.'</a>&gt;';
      }
      // add organisation infos
      if ( $p->einsatzorga_id ) {
        $item['einsatzorga'] .= '<a href="' . $this->view->orgas[ $p->einsatzorga_id ]["website"] . '">';
        $item['einsatzorga'] .= $this->view->orgas[ $p->einsatzorga_id ]["name"];
        $item['einsatzorga'] .= '</a><br>';
      } else {
        $item['einsatzorga'] .= !empty($p->einsatzorga_other_website) ? '<a href="'.$p->einsatzorga_other_website.'">' : '';
        $item['einsatzorga'] .= empty($p->einsatzorga_other_name) ? '<i>(n.a.)</i>' : $p->einsatzorga_other_name;
        $item['einsatzorga'] .= empty($p->einsatzorga_other_website) ? '</a>' : '';
        $item['einsatzorga'] .= '<br>';
      }
      $item['einsatzorga'] .= 'Kontakt: ';
      $item['einsatzorga'] .= ! empty($p->einsatzorga_kontakt_email) ? '<a href="mailto:'.$p->einsatzorga_kontakt_email.'">' : '';
      $item['einsatzorga'] .= empty($p->einsatzorga_kontakt) ? '<i>(n.a.)</i>' : $p->einsatzorga_kontakt;
      $item['einsatzorga'] .= ! empty($p->einsatzorga_kontakt_email) ? '</a>' : '';
      // add country if set
      if (!empty($p->einsatzland)) {
        $item['praktikumsstelle'] .= '<br>Einsatzland: '.$p->einsatzland;
      }
      // add travel dates
      $item['travel'] .= 'Ausreise: ';
      $item['travel'] .= empty($p->ausreise) ? '<i>(n.a.)</i>' : $p->ausreise;
      $item['travel'] .= '<br>';
      $item['travel'] .= 'Rückkehr: ';
      $item['travel'] .= empty($p->rueckkehr) ? '<i>(n.a.)</i>' : $p->rueckkehr;
      // add funding info
      if (isset($p->oeza_zuschuss)) {
        if ($p->oeza_zuschuss) $item['funding'] .= '<span class="dashicons dashicons-yes"></span>';
        else $item['funding'] .= '<span class="dashicons dashicons-no"></span>';
      } else {
        $item['funding'] .= "n/a";
      }
      $item['funding'] .= '<br>ergänzend: ';
      if (isset($p->oeza_zuschuss_ergaenzend)) {
        if ($p->oeza_zuschuss_ergaenzend) $item['funding'] .= '<span class="dashicons dashicons-yes"></span>';
        else $item['funding'] .= '<span class="dashicons dashicons-no"></span>';
      }
      else {
        $item['funding'] .= "n/a";
      }
      // add icons for files column
      $files = $this->view->get_files_for_participant( $p->id );
      $file = $this->view->get_file_by_type( "picture", $files );
      if ( $file !== false ) {
        $path = GSD_UPLOAD_URL . '/' . $file->filepath;
        $title = $file->type . ' , last upload: ' . $file->updated;
        $item['files'] .= '
        <a href="'.$path.'" title="'.$title.'">
          <span class="dashicons dashicons-format-image"></span>
        </a> ';
      }
      $file = $this->view->get_file_by_type( "motivation", $files );
      if ( $file !== false ) {
        $path = GSD_UPLOAD_URL . '/' . $file->filepath;
        $title = $file->type . ' , last upload: ' . $file->updated;
        $item['files'] .= '
        <a href="'.$path.'" title="'.$title.'">
          <span class="dashicons dashicons-text-page"></span>
        </a> ';
      }
      $file = $this->view->get_file_by_type( "identity", $files );
      if ( $file !== false ) {
        $path = GSD_UPLOAD_URL . '/' . $file->filepath;
        $title = $file->type . ' , last upload: ' . $file->updated;
        $item['files'] .= '
        <a href="'.$path.'" title="'.$title.'">
          <span class="dashicons dashicons-id"></span>
        </a> ';
      }
      $file = $this->view->get_file_by_type( "languagetest", $files );
      if ( $file !== false ) {
        $path = GSD_UPLOAD_URL . '/' . $file->filepath;
        $title = $file->type . ' , last upload: ' . $file->updated;
        $item['files'] .= '
        <a href="'.$path.'" title="'.$title.'">
          <span class="dashicons dashicons-editor-spellcheck"></span>
        </a> ';
      }
      // now add this prepared item to the list
      $this->items[] = $item;
    }
  }

  public function _column_actions($item) {
    $actions = '';
    $uri_base = $_SERVER["SCRIPT_NAME"] . '?page=' . $_GET["page"] . '&kohorte=' . $this->view->active_cohort;
    $uri_export = wp_nonce_url( '/wp-content/plugins/gsd-db/export.php?set=single&id=' . $item['matrikelnr'], 'export' );
    $uri_datasheet = wp_nonce_url( '/wp-content/plugins/gsd-db/src/print-datasheet.php?id=' . $item['matrikelnr'], 'datasheet' );
    if ($this->view->is_admin) {
      $uri_edit = wp_nonce_url( $uri_base . '&action=edit&id=' . $item['matrikelnr'] );
      $uri_delete = wp_nonce_url( $uri_base . '&action=delete&id=' . $item['matrikelnr'] );
      $uri_mail = "mailto:".$item['email_plain'];
      $uri_approve = wp_nonce_url( $uri_base . '&action=approve&id=' . $item['matrikelnr'] );
      $uri_register = wp_nonce_url( $uri_base . '&action=register&id=' . $item['matrikelnr'] );
      $uri_cancel = wp_nonce_url( $uri_base . '&action=cancel&id=' . $item['matrikelnr'] );
      $actions .= '<a href="'.$uri_edit.'" title="Bearbeiten"><span class="dashicons dashicons-edit"></span></a>';
      if ($this->type == 'unapproved') {
        $actions .= ' <a href="'.$uri_approve.'" title="Voranmeldung bestätigen"><span class="dashicons dashicons-yes-alt"></span></a>';
      } else if ($this->type == 'approved') {
        $actions .= ' <a href="'.$uri_cancel.'" title="Voranmeldung abbrechen"><span class="dashicons dashicons-dismiss"></span></a>';
      } else if ($this->type == 'submitted') {
        $actions .= ' <a href="'.$uri_register.'" title="Als fixe*n Teilnehmer*in registrieren"><span class="dashicons dashicons-yes-alt"></span></a>';
        $actions .= ' <a href="'.$uri_cancel.'" title="Voranmeldung abbrechen"><span class="dashicons dashicons-dismiss"></span></a>';
      } else if ($this->type == 'registered') {
        $actions .= ' <a href="'.$uri_cancel.'" title="Praktikum abbrechen"><span class="dashicons dashicons-dismiss"></span></a>';
      } else if ($this->type == 'cancelled') {
        $actions .= ' <a href="'.$uri_reactivate.'" title="Reaktivieren"><span class="dashicons dashicons-yes-alt"></span></a>';
      }
      $actions .= ' <a href="'.$uri_delete.'" title="Löschen"><span class="dashicons dashicons-trash"></span></a>';
      $actions .= ' <a href="'.$uri_mail.'" title="E-Mail senden"><span class="dashicons dashicons-email-alt"></span></a>';
    }
    $actions .= ' <a href="'.$uri_export.'" title="Als CSV exportieren"><span class="dashicons dashicons-download"></span></a>';
    $actions .= ' <a href="'.$uri_datasheet.'" title="Datenblatt anzeigen"><span class="dashicons dashicons-media-document"></span></a>';
    return "<td>$actions</td>";
  }
}
