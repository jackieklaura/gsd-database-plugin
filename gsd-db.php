<?php
/*
  Plugin Name: Global Social Dialogue Database
  Plugin URI: http://gitlab.com/jackieklaura
  Description: A database and collection of forms for the GSD project to enable students to provide their data to the project in a consistent way.
  Author: Andrea Ida Malkah Klaura <jackie@tantemalkah.at>
  Version: 1.0.0
  Author URI: https://tantemalkah.at
  License: AGPLv3
*/

namespace GSDDB;

define ("GSD_TABLE_TEILNEHMERIN", "gsd_teilnehmerin");
define ("GSD_TABLE_KOHORTE", "gsd_kohorte");
define ("GSD_TABLE_HOCHSCHULE", "gsd_hochschule");
define ("GSD_TABLE_KOHORTE_HOCHSCHULE", "gsd_kohorte_hochschule");
define ("GSD_TABLE_EINSATZORGA", "gsd_einsatzorga");
define ("GSD_TABLE_FILE", "gsd_file");
define ("GSD_TABLE_NOTFALLKONTAKT", "gsd_notfallkontakt");
define ("GSD_TABLE_RETURN_FORM", "gsd_return_form");
define ("GSD_TABLE_LECTURER", "gsd_lektorin");

define ("GSD_UPLOAD_DIR", wp_get_upload_dir()["basedir"] . '/gsd_uploads' );
define ("GSD_UPLOAD_URL", wp_get_upload_dir()["baseurl"] . '/gsd_uploads' );

register_activation_hook( __FILE__, function() {
  require_once plugin_dir_path( __FILE__ ) . 'src/class-activation.php';
  Activation::activate();
} );

require_once plugin_dir_path( __FILE__ ) . 'src/class-plugin.php';

function enqueue_jquery_ui_accordion()
{
  wp_enqueue_script( 'jquery-ui-accordion' );
  wp_enqueue_style('gsd-db-admin-jquery-ui-css', plugins_url().'/gsd-db/assets/css/jquery-ui/accordion.min.css');
}
add_action( 'admin_enqueue_scripts', 'GSDDB\\enqueue_jquery_ui_accordion' );

$gsddb = new Plugin();
$gsddb->init();
