<?php
require_once($_SERVER["DOCUMENT_ROOT"].'/wp-load.php');

if ( ! is_user_logged_in() ) {
  $msg = '[GSD error]: export.php: an unauthenticated user accessed the export script!';
  $msg .= ' IP: ' . $_SERVER["REMOTE_ADDR"];
  error_log($msg);
  exit;
}

if ( ! current_user_can("manage_options") ) {
  $msg = '[GSD error]: export.php: a user without permission tried to export data.';
  $msg .= ' $_COOKIES: ' . json_encode($_COOKIES);
  $msg .= ' $_SERVER: ' . json_encode($_SERVER);
  $msg .= ' $_GET:' . json_encode($_GET);
  error_log($msg);
  exit( "Your are not allowed to export data!" );
}

function respond_with_error ($data) {
  header('Content-Type: application/json');
  http_response_code(400);
  echo json_encode($data);
  exit;
}

// validate GET parameters and nonce
if ( empty( $_GET["_wpnonce"] ) ) {
  respond_with_error([
    "error" => "missing nonce",
  ]);
}
if ( ! wp_verify_nonce($_GET["_wpnonce"], 'export') ) {
  respond_with_error([
    "error" => "invalid nonce",
  ]);
}

$allowed_sets = ["single", "all"];

if ( empty( $_GET["set"] ) ) {
    respond_with_error([
      "error" => "missing parameter: set",
      "allowed" => $allowed_sets,
    ]);
}
if ( ! in_array( $_GET["set"], $allowed_sets ) ) {
  respond_with_error([
    "error" => "invalid set",
    "allowed" => $allowed_sets,
  ]);
}

$id = "";
if ( $_GET["set"] == "single" ) {
  if ( empty( $_GET["id"] ) ) {
    respond_with_error([
      "error" => "missing parameter: id",
    ]);
  }

  if ( ! preg_match('/^[a-z0-9]+$/', $_GET["id"]) ) {
    respond_with_error([
      "error" => "malformed id parameter",
    ]);
  }
  $id = $_GET["id"];
}
$set = $_GET["set"];

$kohorte = false;
if ( ! empty( $_GET["kohorte"] ) ) {
  if ( preg_match('/^[0-9]+$/', $_GET["kohorte"]) ) {
    $kohorte = $_GET["kohorte"];
  } else {
    respond_with_error([
      "error" => "malformed kohorte parameter",
    ]);
  }
}

/**
 * This function applies a filter to an SQL query for status and cohort of participants
 *
 * Given an unfiltered (without any WHERE clauses) SQL query on a table
 * this function applies filters for the $set
 * and the $kohorte by appending an according WHERE clause.
 *
 * @param string $q The unfiltered query for the filter to be applied on
 * @param string $set The set of participants that should be used based on their status
 * @param string $id The participant id (needed if $set == "single"), can otherwise be empty
 * @param string $kohorte The cohort ID of the participants to be exported
 * @return string Filtered SQL query
 */
function gsd_filter_export_query($q, $set, $id, $kohorte) {
  global $wpdb;
  switch ($set) {
    case "single":
      $q .= " WHERE tn.matrikelnr = %s";
      $q = $wpdb->prepare($q, $id);
      break;

    default:
      // the "all" case does not need a specific filter
      break;
  }

  if ( $kohorte ) {
    if ( $set == "all" ) {
      $q .= " WHERE tn.kohorte_id = %d";
    } else {
      $q .= " AND tn.kohorte_id = %d";
    }
    $q = $wpdb->prepare($q, $kohorte);
  }
  return $q;
}

// now we can formulate our query to fetch the participant data
$tbl_hochschule = $wpdb->prefix . GSD_TABLE_HOCHSCHULE;
$tbl_teilnehmerin = $wpdb->prefix . GSD_TABLE_TEILNEHMERIN;
$tbl_returners = $wpdb->prefix . GSD_TABLE_RETURN_FORM;
$q = "SELECT fh.name AS hochschule, tn.kohorte_id, ret.* FROM
        $tbl_hochschule AS fh RIGHT JOIN $tbl_returners AS ret
        ON fh.id = ret.hochschule_id
        JOIN $tbl_teilnehmerin AS tn ON tn.id = ret.teilnehmerin_id";
$q = gsd_filter_export_query( $q, $set, $id, $kohorte );
$r = $wpdb->get_results( $q , ARRAY_A );

if ( empty($r) ) {

  if ( $set == "single" ) {
    $error = [
      "error" => "invalid ID",
      "detail" => "the ID you requested data for is not registered"
    ];
  } else {
    $error = [
      "error" => "no data available",
      "details" => "exporting only works if there is at least one valid " .
        "registrant/participant in the requested data set"
    ];
  }
  respond_with_error($error);
}

// before we are ready to output our data we also fetch the labels used for
// the einsatzorga_id column
$tbl_einsatzorga = $wpdb->prefix . GSD_TABLE_EINSATZORGA;
$q = "SELECT * FROM $tbl_einsatzorga";
$rows = $wpdb->get_results( $q , ARRAY_A );
// and create a mapping array that can be used to retrieve the label
$einsatzorgas = [];
foreach( $rows as $row ) {
  $einsatzorgas[ $row["id"] ] = $row["name"];
}

// we also have to fetch all files for all the
// participants we are exporting
$tbl_file = $wpdb->prefix . GSD_TABLE_FILE;
$q = "SELECT f.* FROM $tbl_file AS f JOIN $tbl_returners AS tn ON f.teilnehmerin_id = tn.teilnehmerin_id";
$q = gsd_filter_export_query( $q, $set, $id, $kohorte );
$rows = $wpdb->get_results( $q , ARRAY_A );
$files = [];
foreach( $rows as $row ) {
  if ( empty($files[$row["teilnehmerin_id"]]) ) $files[$row["teilnehmerin_id"]] = [];
  $files[$row["teilnehmerin_id"]][$row["type"]] = [];
  $files[$row["teilnehmerin_id"]][$row["type"]]["path"] = $row["filepath"];
  $files[$row["teilnehmerin_id"]][$row["type"]]["updated"] = $row["updated"];
  $files[$row["teilnehmerin_id"]][$row["type"]]["created"] = $row["created"];
}

/*
 * $column_config is used to configure the CSV output of the SQL query result
 *
 * The items represent the order in which the columns should be put into the
 * CSV file. Only those columns will be inserted, which exist in this config.
 * Other columns of the result set will be ignored. Each table column name
 * listed in this config maps to a lable, which will be inserted in the header
 * row of the CSV file.
 */
$column_config = [
  // Primärdaten wie in der Backend-Übersicht
  "matrikelnr" => "Matrikelnr",
  "firstname" => "Vorname",
  "lastname" => "Nachname",
  "email" => "e-Mail",
  "hochschule" => "Fachhochschule",
  "lektorin" => "Name FH-Praxislehrende",
  // Zusätzliche persönliche Daten
  "adresse" => "Adresse d. Teilnehmerin",
  "telefon" => "Telefonnr",
  "skype" => "Skype-Kontakt",
  // Einsatzorganisation
  "einsatzorga_id" => "Einsatzorganisation (vordefiniert)",
  "einsatzorga_other_name" => "Sonstige Einsatorganisation Name",
  "einsatzorga_other_website" => "Sonstige Einsatorganisation Website",
  "einsatzorga_kontakt" => "Kontaktperson Einsatzorganisation",
  "einsatzorga_kontakt_email" => "e-Mail Kontaktperson Einsatzorganisation",
  "einsatzorga_kontakt_telefon" => "Telefon Kontaktperson Einsatzorganisation",
  // Praktikumsstelle
  "praktikumsstelle" => "Praktikumsstelle",
  "praktikumsstelle_aenderung" => "geändert?",
  "praktikumsstelle_land" => "Land Praktikumsstelle",
  "praktikumsstelle_adresse" => "Adresse Praktikumsstelle",
  "praktikumsstelle_website" => "Website Praktikumsstelle",
  "praktikumsstelle_kontakt" => "Kontaktperson Praktikumsstelle",
  "praktikumsstelle_kontakt_email" => "e-Mail Kontaktperson Praktikumsstelle",
  "praktikumsstelle_kontakt_telefon" => "Telefon Kontaktperson Praktikumsstelle",
  "praktikumsstelle_beschreibung" => "Beschreibung Praktikumsstelle",
  "praktikum_beginn" => "Praktikumsbeginn",
  "praktikum_ende" => "Praktikumsende",
  "praktikum_dauer" => "Dauer (in Wochen)",
  "praktikumsstelle_sektoren" => "Arbeitsbereich Sektor",
  "anforderungsprofil" => "Anforerungsprofil",
  // Sprache
  "sprachmindestniveau" => "Mindestsprachniveau",
  "sprachniveau_amtssprache" => "Sprachniveau Amtssprache",
  "sprachniveau_lokalsprache" => "Sprachniveau Lokalsprache",
  // Reisedaten
  "ausreise" => "Ausreise",
  "rueckkehr" => "Rückkehr",
  // Erfahrungsberichte
  "erfahrungen_allgemein" => "Erfahrung Allgemein",
  "erfahrungen_kommunikation" => "Erfahrung Kommunikation",
  "erfahrungen_wohnsituation" => "Erfahrung Wohnsituation",
  "erfahrungen_weiterbildung" => "Erfahrung Weiterbildung",
  "erfahrungen_sicherheit" => "Erfahrung Sicherheit",
  "erfahrungen_highlight" => "Erfahrung Highlight",
  "erfahrungen_lowlight" => "Erfahrung Lowlight",
  "erfahrungen_rueckmeldung" => "Erfahrung Rückmeldung",
  // Kosten und Nachhaltigkeit
  "kosten_gesamt" => "Kosten Gesamt",
  "kosten_kommentar" => "Kosten Kommentar",
  "kosten_sprachtraining" => "Kosten Sprachtraining",
  "nachhaltigkeit" => "Nachhaltigkeit",
  // Zuschuss & Verpflichtungserklärung
  "foerdermittel" => "Fördermittel",
  "foerderstellen" => "Förderstellen",
  "zuschuss" => "Zuschuss",
  "ueberweisungsdaten" => "Überweisungsdaten",
  "kenntnisnahme" => "Kenntnisnahme?",
  // Zeitstempel
  "created" => "Erstellt",
  "updated" => "Zuletzt aktualisiert",
];


$filename = "gsd-export-returners-";
if ( $_GET["set"] == "single" ) {
  $filename .= $_GET["id"];
} else {
  $filename .= $_GET["set"];
}
$filename .= "_" . date("Y-m-d_Hi") . ".csv";

header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="' . $filename . '"');

// for the first line we want to have the column names
$line = '';
foreach ( $column_config as $label ) {
  $line .= $label . ";";
}
$line .= "Prakikumsbestätigung;";
$line .= "Prakikumsanerkennung durch FH;";
$line .= "Zuschuss-Empfangsbestätigung;";
echo $line . "\n";

// now the contents of all rows follow
foreach ( $r as $row ) {
  $line = '';
  foreach ( $column_config as $column => $value ) {
    if ( $column == "einsatzorga_id" && !empty($row[$column]) ) {
      $line .= '"' . str_replace('"', '""', $einsatzorgas[$row[$column]] ) . '";';
    } else {
      $line .= '"' . str_replace('"', '""', $row[ $column ] ) . '";';
    }
  }
  // now add the file uploads
  $id = $row["teilnehmerin_id"];
  foreach ( ["cf_internship", "cf_university", "cf_subsidy"] as $type ) {
    if ( empty($files[$id]) || empty($files[$id][$type]) ) $line .= ";";
    else $line .= '"'.GSD_UPLOAD_URL."/".$files[$id][$type]["path"]."' (".$files[$id][$type]["updated"].')";';
  }
  echo $line . "\n";
}
